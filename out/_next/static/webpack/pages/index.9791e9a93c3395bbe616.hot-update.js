webpackHotUpdate_N_E("pages/index",{

/***/ "./components/elements/detail/modules/ModuleDetailShoppingActions.jsx":
/*!****************************************************************************!*\
  !*** ./components/elements/detail/modules/ModuleDetailShoppingActions.jsx ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _store_cart_action__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ~/store/cart/action */ "./store/cart/action.js");
/* harmony import */ var _store_compare_action__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ~/store/compare/action */ "./store/compare/action.js");
/* harmony import */ var _store_wishlist_action__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ~/store/wishlist/action */ "./store/wishlist/action.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_6__);



var _jsxFileName = "D:\\ReactJS\\projects\\nextjs\\web3-shopping\\components\\elements\\detail\\modules\\ModuleDetailShoppingActions.jsx",
    _this = undefined,
    _s = $RefreshSig$();








var ModuleDetailShoppingActions = function ModuleDetailShoppingActions(_ref) {
  _s();

  var product = _ref.product,
      _ref$extended = _ref.extended,
      extended = _ref$extended === void 0 ? false : _ref$extended;
  var dispatch = Object(react_redux__WEBPACK_IMPORTED_MODULE_5__["useDispatch"])();
  var cartItems = Object(react_redux__WEBPACK_IMPORTED_MODULE_5__["useSelector"])(function (state) {
    return state.cart.cartItems;
  });

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(1),
      quantity = _useState[0],
      setQuantity = _useState[1];

  var Router = Object(next_router__WEBPACK_IMPORTED_MODULE_6__["useRouter"])();

  var handleAddItemToCart = function handleAddItemToCart(e) {
    e.preventDefault();
    var tmp = product;
    tmp.quantity = quantity;
    dispatch(Object(_store_cart_action__WEBPACK_IMPORTED_MODULE_2__["addItem"])(tmp));
  };

  var handleBuynow = function handleBuynow(e) {
    e.preventDefault();
    var tmp = product;
    tmp.quantity = quantity;
    dispatch(Object(_store_cart_action__WEBPACK_IMPORTED_MODULE_2__["addItem"])(tmp));
    setTimeout(function () {
      Router.push("/account/checkout");
    }, 1000);
  };

  var handleAddItemToCompare = function handleAddItemToCompare(e) {
    e.preventDefault();
    dispatch(Object(_store_compare_action__WEBPACK_IMPORTED_MODULE_3__["addItemToCompare"])(product));
  };

  var handleAddItemToWishlist = function handleAddItemToWishlist(e) {
    e.preventDefault(); // const { product } = this.props;

    dispatch(Object(_store_wishlist_action__WEBPACK_IMPORTED_MODULE_4__["addItemToWishlist"])(product));
  };

  var handleIncreaseItemQty = function handleIncreaseItemQty(e) {
    e.preventDefault();
    setQuantity(quantity + 1);
  };

  var handleDecreaseItemQty = function handleDecreaseItemQty(e) {
    e.preventDefault();

    if (quantity > 1) {
      setQuantity(quantity - 1);
    }
  };

  if (!extended) {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        className: "ps-product__shopping",
        style: {
          marginBottom: "0px",
          paddingBottom: "0px",
          borderBottom: "0px",
          display: "flex",
          justifyContent: "center"
        },
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
          className: "ps-btn",
          style: {
            fontWeight: "normal"
          },
          href: "#",
          onClick: function onClick(e) {
            return handleBuynow(e);
          },
          children: "Buy Now"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 73,
          columnNumber: 11
        }, _this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 56,
        columnNumber: 9
      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 82,
        columnNumber: 9
      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        className: "ps-product__shopping",
        style: {
          display: "flex",
          justifyContent: "center",
          borderBottom: "0px solid #fff"
        },
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          className: "ps-product__actions",
          style: {
            width: "100%",
            textAlign: "center",
            border: "1px solid #000",
            maxWidth: "100px"
          },
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
            href: "#",
            onClick: function onClick(e) {
              return handleAddItemToWishlist(e);
            },
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
              className: "icon-heart"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 101,
              columnNumber: 15
            }, _this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 100,
            columnNumber: 13
          }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
            href: "#",
            onClick: function onClick(e) {
              return handleAddItemToCompare(e);
            },
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
              className: "icon-chart-bars"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 104,
              columnNumber: 15
            }, _this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 103,
            columnNumber: 13
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 91,
          columnNumber: 11
        }, _this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 83,
        columnNumber: 9
      }, _this)]
    }, void 0, true);
  } else {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      className: "ps-product__shopping extend",
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        className: "ps-product__btn-group",
        style: {
          display: "flex",
          justifyContent: "center"
        },
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          className: "ps-product__actions",
          style: {
            border: "1px solid #000",
            textAlign: "center",
            width: "100px"
          },
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
            href: "#",
            onClick: function onClick(e) {
              return handleAddItemToWishlist(e);
            },
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
              className: "icon-heart"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 155,
              columnNumber: 15
            }, _this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 154,
            columnNumber: 13
          }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
            href: "#",
            onClick: function onClick(e) {
              return handleAddItemToCompare(e);
            },
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
              className: "icon-chart-bars"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 158,
              columnNumber: 15
            }, _this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 157,
            columnNumber: 13
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 146,
          columnNumber: 11
        }, _this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 113,
        columnNumber: 9
      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
        className: "ps-btn",
        href: "#",
        onClick: function onClick(e) {
          return handleBuynow(e);
        },
        children: "Buy Now"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 162,
        columnNumber: 9
      }, _this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 112,
      columnNumber: 7
    }, _this);
  }
};

_s(ModuleDetailShoppingActions, "uB62DFDl7e65ktayiBPtCxuxiJI=", false, function () {
  return [react_redux__WEBPACK_IMPORTED_MODULE_5__["useDispatch"], react_redux__WEBPACK_IMPORTED_MODULE_5__["useSelector"], next_router__WEBPACK_IMPORTED_MODULE_6__["useRouter"]];
});

_c = ModuleDetailShoppingActions;
/* harmony default export */ __webpack_exports__["default"] = (ModuleDetailShoppingActions);

var _c;

$RefreshReg$(_c, "ModuleDetailShoppingActions");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9lbGVtZW50cy9kZXRhaWwvbW9kdWxlcy9Nb2R1bGVEZXRhaWxTaG9wcGluZ0FjdGlvbnMuanN4Il0sIm5hbWVzIjpbIk1vZHVsZURldGFpbFNob3BwaW5nQWN0aW9ucyIsInByb2R1Y3QiLCJleHRlbmRlZCIsImRpc3BhdGNoIiwidXNlRGlzcGF0Y2giLCJjYXJ0SXRlbXMiLCJ1c2VTZWxlY3RvciIsInN0YXRlIiwiY2FydCIsInVzZVN0YXRlIiwicXVhbnRpdHkiLCJzZXRRdWFudGl0eSIsIlJvdXRlciIsInVzZVJvdXRlciIsImhhbmRsZUFkZEl0ZW1Ub0NhcnQiLCJlIiwicHJldmVudERlZmF1bHQiLCJ0bXAiLCJhZGRJdGVtIiwiaGFuZGxlQnV5bm93Iiwic2V0VGltZW91dCIsInB1c2giLCJoYW5kbGVBZGRJdGVtVG9Db21wYXJlIiwiYWRkSXRlbVRvQ29tcGFyZSIsImhhbmRsZUFkZEl0ZW1Ub1dpc2hsaXN0IiwiYWRkSXRlbVRvV2lzaGxpc3QiLCJoYW5kbGVJbmNyZWFzZUl0ZW1RdHkiLCJoYW5kbGVEZWNyZWFzZUl0ZW1RdHkiLCJtYXJnaW5Cb3R0b20iLCJwYWRkaW5nQm90dG9tIiwiYm9yZGVyQm90dG9tIiwiZGlzcGxheSIsImp1c3RpZnlDb250ZW50IiwiZm9udFdlaWdodCIsIndpZHRoIiwidGV4dEFsaWduIiwiYm9yZGVyIiwibWF4V2lkdGgiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsSUFBTUEsMkJBQTJCLEdBQUcsU0FBOUJBLDJCQUE4QixPQUFtQztBQUFBOztBQUFBLE1BQWhDQyxPQUFnQyxRQUFoQ0EsT0FBZ0M7QUFBQSwyQkFBdkJDLFFBQXVCO0FBQUEsTUFBdkJBLFFBQXVCLDhCQUFaLEtBQVk7QUFDckUsTUFBTUMsUUFBUSxHQUFHQywrREFBVyxFQUE1QjtBQUNBLE1BQU1DLFNBQVMsR0FBR0MsK0RBQVcsQ0FBQyxVQUFDQyxLQUFEO0FBQUEsV0FBV0EsS0FBSyxDQUFDQyxJQUFOLENBQVdILFNBQXRCO0FBQUEsR0FBRCxDQUE3Qjs7QUFGcUUsa0JBR3JDSSxzREFBUSxDQUFDLENBQUQsQ0FINkI7QUFBQSxNQUc5REMsUUFIOEQ7QUFBQSxNQUdwREMsV0FIb0Q7O0FBSXJFLE1BQU1DLE1BQU0sR0FBR0MsNkRBQVMsRUFBeEI7O0FBRUEsTUFBTUMsbUJBQW1CLEdBQUcsU0FBdEJBLG1CQUFzQixDQUFDQyxDQUFELEVBQU87QUFDakNBLEtBQUMsQ0FBQ0MsY0FBRjtBQUNBLFFBQUlDLEdBQUcsR0FBR2hCLE9BQVY7QUFDQWdCLE9BQUcsQ0FBQ1AsUUFBSixHQUFlQSxRQUFmO0FBQ0FQLFlBQVEsQ0FBQ2Usa0VBQU8sQ0FBQ0QsR0FBRCxDQUFSLENBQVI7QUFDRCxHQUxEOztBQU9BLE1BQU1FLFlBQVksR0FBRyxTQUFmQSxZQUFlLENBQUNKLENBQUQsRUFBTztBQUMxQkEsS0FBQyxDQUFDQyxjQUFGO0FBQ0EsUUFBSUMsR0FBRyxHQUFHaEIsT0FBVjtBQUNBZ0IsT0FBRyxDQUFDUCxRQUFKLEdBQWVBLFFBQWY7QUFDQVAsWUFBUSxDQUFDZSxrRUFBTyxDQUFDRCxHQUFELENBQVIsQ0FBUjtBQUNBRyxjQUFVLENBQUMsWUFBWTtBQUNyQlIsWUFBTSxDQUFDUyxJQUFQLENBQVksbUJBQVo7QUFDRCxLQUZTLEVBRVAsSUFGTyxDQUFWO0FBR0QsR0FSRDs7QUFVQSxNQUFNQyxzQkFBc0IsR0FBRyxTQUF6QkEsc0JBQXlCLENBQUNQLENBQUQsRUFBTztBQUNwQ0EsS0FBQyxDQUFDQyxjQUFGO0FBQ0FiLFlBQVEsQ0FBQ29CLDhFQUFnQixDQUFDdEIsT0FBRCxDQUFqQixDQUFSO0FBQ0QsR0FIRDs7QUFLQSxNQUFNdUIsdUJBQXVCLEdBQUcsU0FBMUJBLHVCQUEwQixDQUFDVCxDQUFELEVBQU87QUFDckNBLEtBQUMsQ0FBQ0MsY0FBRixHQURxQyxDQUVyQzs7QUFDQWIsWUFBUSxDQUFDc0IsZ0ZBQWlCLENBQUN4QixPQUFELENBQWxCLENBQVI7QUFDRCxHQUpEOztBQU1BLE1BQU15QixxQkFBcUIsR0FBRyxTQUF4QkEscUJBQXdCLENBQUNYLENBQUQsRUFBTztBQUNuQ0EsS0FBQyxDQUFDQyxjQUFGO0FBQ0FMLGVBQVcsQ0FBQ0QsUUFBUSxHQUFHLENBQVosQ0FBWDtBQUNELEdBSEQ7O0FBS0EsTUFBTWlCLHFCQUFxQixHQUFHLFNBQXhCQSxxQkFBd0IsQ0FBQ1osQ0FBRCxFQUFPO0FBQ25DQSxLQUFDLENBQUNDLGNBQUY7O0FBQ0EsUUFBSU4sUUFBUSxHQUFHLENBQWYsRUFBa0I7QUFDaEJDLGlCQUFXLENBQUNELFFBQVEsR0FBRyxDQUFaLENBQVg7QUFDRDtBQUNGLEdBTEQ7O0FBTUEsTUFBSSxDQUFDUixRQUFMLEVBQWU7QUFDYix3QkFDRTtBQUFBLDhCQUNFO0FBQ0UsaUJBQVMsRUFBQyxzQkFEWjtBQUVFLGFBQUssRUFBRTtBQUNMMEIsc0JBQVksRUFBRSxLQURUO0FBRUxDLHVCQUFhLEVBQUUsS0FGVjtBQUdMQyxzQkFBWSxFQUFFLEtBSFQ7QUFJTEMsaUJBQU8sRUFBRSxNQUpKO0FBS0xDLHdCQUFjLEVBQUU7QUFMWCxTQUZUO0FBQUEsK0JBaUJFO0FBQ0UsbUJBQVMsRUFBQyxRQURaO0FBRUUsZUFBSyxFQUFFO0FBQUVDLHNCQUFVLEVBQUU7QUFBZCxXQUZUO0FBR0UsY0FBSSxFQUFDLEdBSFA7QUFJRSxpQkFBTyxFQUFFLGlCQUFDbEIsQ0FBRDtBQUFBLG1CQUFPSSxZQUFZLENBQUNKLENBQUQsQ0FBbkI7QUFBQSxXQUpYO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBakJGO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFERixlQTJCRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBM0JGLGVBNEJFO0FBQ0UsaUJBQVMsRUFBQyxzQkFEWjtBQUVFLGFBQUssRUFBRTtBQUNMZ0IsaUJBQU8sRUFBRSxNQURKO0FBRUxDLHdCQUFjLEVBQUUsUUFGWDtBQUdMRixzQkFBWSxFQUFFO0FBSFQsU0FGVDtBQUFBLCtCQVFFO0FBQ0UsbUJBQVMsRUFBQyxxQkFEWjtBQUVFLGVBQUssRUFBRTtBQUNMSSxpQkFBSyxFQUFFLE1BREY7QUFFTEMscUJBQVMsRUFBRSxRQUZOO0FBR0xDLGtCQUFNLEVBQUUsZ0JBSEg7QUFJTEMsb0JBQVEsRUFBRTtBQUpMLFdBRlQ7QUFBQSxrQ0FTRTtBQUFHLGdCQUFJLEVBQUMsR0FBUjtBQUFZLG1CQUFPLEVBQUUsaUJBQUN0QixDQUFEO0FBQUEscUJBQU9TLHVCQUF1QixDQUFDVCxDQUFELENBQTlCO0FBQUEsYUFBckI7QUFBQSxtQ0FDRTtBQUFHLHVCQUFTLEVBQUM7QUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFURixlQVlFO0FBQUcsZ0JBQUksRUFBQyxHQUFSO0FBQVksbUJBQU8sRUFBRSxpQkFBQ0EsQ0FBRDtBQUFBLHFCQUFPTyxzQkFBc0IsQ0FBQ1AsQ0FBRCxDQUE3QjtBQUFBLGFBQXJCO0FBQUEsbUNBQ0U7QUFBRyx1QkFBUyxFQUFDO0FBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBWkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUkY7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQTVCRjtBQUFBLG9CQURGO0FBd0RELEdBekRELE1BeURPO0FBQ0wsd0JBQ0U7QUFBSyxlQUFTLEVBQUMsNkJBQWY7QUFBQSw4QkFDRTtBQUNFLGlCQUFTLEVBQUMsdUJBRFo7QUFFRSxhQUFLLEVBQUU7QUFBRWdCLGlCQUFPLEVBQUUsTUFBWDtBQUFtQkMsd0JBQWMsRUFBRTtBQUFuQyxTQUZUO0FBQUEsK0JBaUNFO0FBQ0UsbUJBQVMsRUFBQyxxQkFEWjtBQUVFLGVBQUssRUFBRTtBQUNMSSxrQkFBTSxFQUFFLGdCQURIO0FBRUxELHFCQUFTLEVBQUUsUUFGTjtBQUdMRCxpQkFBSyxFQUFFO0FBSEYsV0FGVDtBQUFBLGtDQVFFO0FBQUcsZ0JBQUksRUFBQyxHQUFSO0FBQVksbUJBQU8sRUFBRSxpQkFBQ25CLENBQUQ7QUFBQSxxQkFBT1MsdUJBQXVCLENBQUNULENBQUQsQ0FBOUI7QUFBQSxhQUFyQjtBQUFBLG1DQUNFO0FBQUcsdUJBQVMsRUFBQztBQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQVJGLGVBV0U7QUFBRyxnQkFBSSxFQUFDLEdBQVI7QUFBWSxtQkFBTyxFQUFFLGlCQUFDQSxDQUFEO0FBQUEscUJBQU9PLHNCQUFzQixDQUFDUCxDQUFELENBQTdCO0FBQUEsYUFBckI7QUFBQSxtQ0FDRTtBQUFHLHVCQUFTLEVBQUM7QUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFYRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFqQ0Y7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQURGLGVBa0RFO0FBQUcsaUJBQVMsRUFBQyxRQUFiO0FBQXNCLFlBQUksRUFBQyxHQUEzQjtBQUErQixlQUFPLEVBQUUsaUJBQUNBLENBQUQ7QUFBQSxpQkFBT0ksWUFBWSxDQUFDSixDQUFELENBQW5CO0FBQUEsU0FBeEM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFsREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGFBREY7QUF3REQ7QUFDRixDQWhLRDs7R0FBTWYsMkI7VUFDYUksdUQsRUFDQ0UsdUQsRUFFSE8scUQ7OztLQUpYYiwyQjtBQWtLU0EsMEZBQWYiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvaW5kZXguOTc5MWU5YTkzYzMzOTViYmU2MTYuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyB1c2VTdGF0ZSB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgeyBhZGRJdGVtIH0gZnJvbSBcIn4vc3RvcmUvY2FydC9hY3Rpb25cIjtcclxuaW1wb3J0IHsgYWRkSXRlbVRvQ29tcGFyZSB9IGZyb20gXCJ+L3N0b3JlL2NvbXBhcmUvYWN0aW9uXCI7XHJcbmltcG9ydCB7IGFkZEl0ZW1Ub1dpc2hsaXN0IH0gZnJvbSBcIn4vc3RvcmUvd2lzaGxpc3QvYWN0aW9uXCI7XHJcbmltcG9ydCB7IHVzZURpc3BhdGNoLCB1c2VTZWxlY3RvciB9IGZyb20gXCJyZWFjdC1yZWR1eFwiO1xyXG5pbXBvcnQgeyB1c2VSb3V0ZXIgfSBmcm9tIFwibmV4dC9yb3V0ZXJcIjtcclxuXHJcbmNvbnN0IE1vZHVsZURldGFpbFNob3BwaW5nQWN0aW9ucyA9ICh7IHByb2R1Y3QsIGV4dGVuZGVkID0gZmFsc2UgfSkgPT4ge1xyXG4gIGNvbnN0IGRpc3BhdGNoID0gdXNlRGlzcGF0Y2goKTtcclxuICBjb25zdCBjYXJ0SXRlbXMgPSB1c2VTZWxlY3Rvcigoc3RhdGUpID0+IHN0YXRlLmNhcnQuY2FydEl0ZW1zKTtcclxuICBjb25zdCBbcXVhbnRpdHksIHNldFF1YW50aXR5XSA9IHVzZVN0YXRlKDEpO1xyXG4gIGNvbnN0IFJvdXRlciA9IHVzZVJvdXRlcigpO1xyXG5cclxuICBjb25zdCBoYW5kbGVBZGRJdGVtVG9DYXJ0ID0gKGUpID0+IHtcclxuICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgIGxldCB0bXAgPSBwcm9kdWN0O1xyXG4gICAgdG1wLnF1YW50aXR5ID0gcXVhbnRpdHk7XHJcbiAgICBkaXNwYXRjaChhZGRJdGVtKHRtcCkpO1xyXG4gIH07XHJcblxyXG4gIGNvbnN0IGhhbmRsZUJ1eW5vdyA9IChlKSA9PiB7XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICBsZXQgdG1wID0gcHJvZHVjdDtcclxuICAgIHRtcC5xdWFudGl0eSA9IHF1YW50aXR5O1xyXG4gICAgZGlzcGF0Y2goYWRkSXRlbSh0bXApKTtcclxuICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICBSb3V0ZXIucHVzaChcIi9hY2NvdW50L2NoZWNrb3V0XCIpO1xyXG4gICAgfSwgMTAwMCk7XHJcbiAgfTtcclxuXHJcbiAgY29uc3QgaGFuZGxlQWRkSXRlbVRvQ29tcGFyZSA9IChlKSA9PiB7XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICBkaXNwYXRjaChhZGRJdGVtVG9Db21wYXJlKHByb2R1Y3QpKTtcclxuICB9O1xyXG5cclxuICBjb25zdCBoYW5kbGVBZGRJdGVtVG9XaXNobGlzdCA9IChlKSA9PiB7XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAvLyBjb25zdCB7IHByb2R1Y3QgfSA9IHRoaXMucHJvcHM7XHJcbiAgICBkaXNwYXRjaChhZGRJdGVtVG9XaXNobGlzdChwcm9kdWN0KSk7XHJcbiAgfTtcclxuXHJcbiAgY29uc3QgaGFuZGxlSW5jcmVhc2VJdGVtUXR5ID0gKGUpID0+IHtcclxuICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgIHNldFF1YW50aXR5KHF1YW50aXR5ICsgMSk7XHJcbiAgfTtcclxuXHJcbiAgY29uc3QgaGFuZGxlRGVjcmVhc2VJdGVtUXR5ID0gKGUpID0+IHtcclxuICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgIGlmIChxdWFudGl0eSA+IDEpIHtcclxuICAgICAgc2V0UXVhbnRpdHkocXVhbnRpdHkgLSAxKTtcclxuICAgIH1cclxuICB9O1xyXG4gIGlmICghZXh0ZW5kZWQpIHtcclxuICAgIHJldHVybiAoXHJcbiAgICAgIDw+XHJcbiAgICAgICAgPGRpdlxyXG4gICAgICAgICAgY2xhc3NOYW1lPVwicHMtcHJvZHVjdF9fc2hvcHBpbmdcIlxyXG4gICAgICAgICAgc3R5bGU9e3tcclxuICAgICAgICAgICAgbWFyZ2luQm90dG9tOiBcIjBweFwiLFxyXG4gICAgICAgICAgICBwYWRkaW5nQm90dG9tOiBcIjBweFwiLFxyXG4gICAgICAgICAgICBib3JkZXJCb3R0b206IFwiMHB4XCIsXHJcbiAgICAgICAgICAgIGRpc3BsYXk6IFwiZmxleFwiLFxyXG4gICAgICAgICAgICBqdXN0aWZ5Q29udGVudDogXCJjZW50ZXJcIixcclxuICAgICAgICAgIH19XHJcbiAgICAgICAgPlxyXG4gICAgICAgICAgey8qIHtjYXJ0SXRlbXM/Lmxlbmd0aCA8IDEgJiYgPGFcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwicHMtYnRuIHBzLWJ0bi0tYmxhY2tcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBocmVmPVwiI1wiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlPXt7Zm9udFdlaWdodDpcIm5vcm1hbFwifX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KGUpID0+IGhhbmRsZUFkZEl0ZW1Ub0NhcnQoZSl9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBBZGQgdG8gY2FydFxyXG4gICAgICAgICAgICAgICAgICAgIDwvYT59ICovfVxyXG4gICAgICAgICAgPGFcclxuICAgICAgICAgICAgY2xhc3NOYW1lPVwicHMtYnRuXCJcclxuICAgICAgICAgICAgc3R5bGU9e3sgZm9udFdlaWdodDogXCJub3JtYWxcIiB9fVxyXG4gICAgICAgICAgICBocmVmPVwiI1wiXHJcbiAgICAgICAgICAgIG9uQ2xpY2s9eyhlKSA9PiBoYW5kbGVCdXlub3coZSl9XHJcbiAgICAgICAgICA+XHJcbiAgICAgICAgICAgIEJ1eSBOb3dcclxuICAgICAgICAgIDwvYT5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8YnI+PC9icj5cclxuICAgICAgICA8ZGl2XHJcbiAgICAgICAgICBjbGFzc05hbWU9XCJwcy1wcm9kdWN0X19zaG9wcGluZ1wiXHJcbiAgICAgICAgICBzdHlsZT17e1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBcImZsZXhcIixcclxuICAgICAgICAgICAganVzdGlmeUNvbnRlbnQ6IFwiY2VudGVyXCIsXHJcbiAgICAgICAgICAgIGJvcmRlckJvdHRvbTogXCIwcHggc29saWQgI2ZmZlwiLFxyXG4gICAgICAgICAgfX1cclxuICAgICAgICA+XHJcbiAgICAgICAgICA8ZGl2XHJcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cInBzLXByb2R1Y3RfX2FjdGlvbnNcIlxyXG4gICAgICAgICAgICBzdHlsZT17e1xyXG4gICAgICAgICAgICAgIHdpZHRoOiBcIjEwMCVcIixcclxuICAgICAgICAgICAgICB0ZXh0QWxpZ246IFwiY2VudGVyXCIsXHJcbiAgICAgICAgICAgICAgYm9yZGVyOiBcIjFweCBzb2xpZCAjMDAwXCIsXHJcbiAgICAgICAgICAgICAgbWF4V2lkdGg6IFwiMTAwcHhcIixcclxuICAgICAgICAgICAgfX1cclxuICAgICAgICAgID5cclxuICAgICAgICAgICAgPGEgaHJlZj1cIiNcIiBvbkNsaWNrPXsoZSkgPT4gaGFuZGxlQWRkSXRlbVRvV2lzaGxpc3QoZSl9PlxyXG4gICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImljb24taGVhcnRcIj48L2k+XHJcbiAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgPGEgaHJlZj1cIiNcIiBvbkNsaWNrPXsoZSkgPT4gaGFuZGxlQWRkSXRlbVRvQ29tcGFyZShlKX0+XHJcbiAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiaWNvbi1jaGFydC1iYXJzXCI+PC9pPlxyXG4gICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgPC8+XHJcbiAgICApO1xyXG4gIH0gZWxzZSB7XHJcbiAgICByZXR1cm4gKFxyXG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cInBzLXByb2R1Y3RfX3Nob3BwaW5nIGV4dGVuZFwiPlxyXG4gICAgICAgIDxkaXZcclxuICAgICAgICAgIGNsYXNzTmFtZT1cInBzLXByb2R1Y3RfX2J0bi1ncm91cFwiXHJcbiAgICAgICAgICBzdHlsZT17eyBkaXNwbGF5OiBcImZsZXhcIiwganVzdGlmeUNvbnRlbnQ6IFwiY2VudGVyXCIgfX1cclxuICAgICAgICA+XHJcbiAgICAgICAgICB7LyogPGZpZ3VyZT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGZpZ2NhcHRpb24+UXVhbnRpdHk8L2ZpZ2NhcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZm9ybS1ncm91cC0tbnVtYmVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwidXBcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eyhlKSA9PiBoYW5kbGVJbmNyZWFzZUl0ZW1RdHkoZSl9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhIGZhLXBsdXNcIj48L2k+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b25cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJkb3duXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoZSkgPT4gaGFuZGxlRGVjcmVhc2VJdGVtUXR5KGUpfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYSBmYS1taW51c1wiPjwvaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZm9ybS1jb250cm9sXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwidGV4dFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9e3F1YW50aXR5fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2ZpZ3VyZT4gKi99XHJcbiAgICAgICAgICB7LyogPGFcclxuICAgICAgICAgICAgY2xhc3NOYW1lPVwicHMtYnRuIHBzLWJ0bi0tYmxhY2tcIlxyXG4gICAgICAgICAgICBzdHlsZT17eyBtaW5XaWR0aDogXCIxMDAlXCIsIG1hcmdpblJpZ2h0OiBcIjIwJVwiIH19XHJcbiAgICAgICAgICAgIGhyZWY9XCIjXCJcclxuICAgICAgICAgICAgb25DbGljaz17KGUpID0+IGhhbmRsZUFkZEl0ZW1Ub0NhcnQoZSl9XHJcbiAgICAgICAgICA+XHJcbiAgICAgICAgICAgIEFkZCB0byBjYXJ0XHJcbiAgICAgICAgICA8L2E+ICovfVxyXG4gICAgICAgICAgPGRpdlxyXG4gICAgICAgICAgICBjbGFzc05hbWU9XCJwcy1wcm9kdWN0X19hY3Rpb25zXCJcclxuICAgICAgICAgICAgc3R5bGU9e3tcclxuICAgICAgICAgICAgICBib3JkZXI6IFwiMXB4IHNvbGlkICMwMDBcIixcclxuICAgICAgICAgICAgICB0ZXh0QWxpZ246IFwiY2VudGVyXCIsXHJcbiAgICAgICAgICAgICAgd2lkdGg6IFwiMTAwcHhcIixcclxuICAgICAgICAgICAgfX1cclxuICAgICAgICAgID5cclxuICAgICAgICAgICAgPGEgaHJlZj1cIiNcIiBvbkNsaWNrPXsoZSkgPT4gaGFuZGxlQWRkSXRlbVRvV2lzaGxpc3QoZSl9PlxyXG4gICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImljb24taGVhcnRcIj48L2k+XHJcbiAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgPGEgaHJlZj1cIiNcIiBvbkNsaWNrPXsoZSkgPT4gaGFuZGxlQWRkSXRlbVRvQ29tcGFyZShlKX0+XHJcbiAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiaWNvbi1jaGFydC1iYXJzXCI+PC9pPlxyXG4gICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8YSBjbGFzc05hbWU9XCJwcy1idG5cIiBocmVmPVwiI1wiIG9uQ2xpY2s9eyhlKSA9PiBoYW5kbGVCdXlub3coZSl9PlxyXG4gICAgICAgICAgQnV5IE5vd1xyXG4gICAgICAgIDwvYT5cclxuICAgICAgPC9kaXY+XHJcbiAgICApO1xyXG4gIH1cclxufTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IE1vZHVsZURldGFpbFNob3BwaW5nQWN0aW9ucztcclxuIl0sInNvdXJjZVJvb3QiOiIifQ==