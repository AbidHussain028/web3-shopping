webpackHotUpdate_N_E("pages/product/[pid]",{

/***/ "./components/elements/products/modules/ModuleProductActions.js":
/*!**********************************************************************!*\
  !*** ./components/elements/products/modules/ModuleProductActions.js ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _store_cart_action__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ~/store/cart/action */ "./store/cart/action.js");
/* harmony import */ var _store_compare_action__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ~/store/compare/action */ "./store/compare/action.js");
/* harmony import */ var _store_wishlist_action__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ~/store/wishlist/action */ "./store/wishlist/action.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _components_elements_detail_ProductDetailQuickView__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ~/components/elements/detail/ProductDetailQuickView */ "./components/elements/detail/ProductDetailQuickView.jsx");


var _jsxFileName = "D:\\ReactJS\\projects\\nextjs\\web3-shopping\\components\\elements\\products\\modules\\ModuleProductActions.js",
    _this = undefined,
    _s = $RefreshSig$();









var ModuleProductActions = function ModuleProductActions(_ref) {
  _s();

  var product = _ref.product;
  var dispatch = Object(react_redux__WEBPACK_IMPORTED_MODULE_6__["useDispatch"])();
  var cartItems = Object(react_redux__WEBPACK_IMPORTED_MODULE_6__["useSelector"])(function (state) {
    return state.cart.cartItems;
  });

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      isQuickView = _useState[0],
      setIsQuickView = _useState[1];

  var handleAddItemToCart = function handleAddItemToCart(e) {
    e.preventDefault();
    dispatch(Object(_store_cart_action__WEBPACK_IMPORTED_MODULE_3__["addItem"])(product));
  };

  var handleAddItemToCompare = function handleAddItemToCompare(e) {
    e.preventDefault();
    dispatch(Object(_store_compare_action__WEBPACK_IMPORTED_MODULE_4__["addItemToCompare"])(product));
  };

  var handleAddItemToWishlist = function handleAddItemToWishlist(e) {
    dispatch(Object(_store_wishlist_action__WEBPACK_IMPORTED_MODULE_5__["addItemToWishlist"])(product));
  };

  var handleShowQuickView = function handleShowQuickView(e) {
    e.preventDefault();
    setIsQuickView(true);
  };

  var handleHideQuickView = function handleHideQuickView(e) {
    e.preventDefault();
    setIsQuickView(false);
  };

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
    className: "ps-product__actions",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
        href: "#",
        "data-toggle": "tooltip",
        "data-placement": "top",
        title: "Quick View",
        onClick: handleShowQuickView,
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
          className: "icon-eye"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 57,
          columnNumber: 11
        }, _this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 50,
        columnNumber: 9
      }, _this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 49,
      columnNumber: 7
    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
        href: "#",
        "data-toggle": "tooltip",
        "data-placement": "top",
        title: "Add to wishlist",
        onClick: handleAddItemToWishlist,
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
          className: "icon-heart"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 68,
          columnNumber: 11
        }, _this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 61,
        columnNumber: 9
      }, _this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 60,
      columnNumber: 7
    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
        href: "#",
        "data-toggle": "tooltip",
        "data-placement": "top",
        title: "Compare",
        onClick: handleAddItemToCompare,
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
          className: "icon-chart-bars"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 79,
          columnNumber: 11
        }, _this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 72,
        columnNumber: 9
      }, _this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 71,
      columnNumber: 7
    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Modal"], {
      centered: true,
      footer: null,
      width: 1024,
      onCancel: function onCancel(e) {
        return handleHideQuickView(e);
      },
      visible: isQuickView,
      closeIcon: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
        className: "icon icon-cross2"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 88,
        columnNumber: 20
      }, _this),
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h3", {
        children: "Quickview"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 90,
        columnNumber: 9
      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_elements_detail_ProductDetailQuickView__WEBPACK_IMPORTED_MODULE_7__["default"], {
        product: product
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 91,
        columnNumber: 9
      }, _this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 82,
      columnNumber: 7
    }, _this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 38,
    columnNumber: 5
  }, _this);
};

_s(ModuleProductActions, "l+QyFN/TrP2EwcXvhtJqzL38ilw=", false, function () {
  return [react_redux__WEBPACK_IMPORTED_MODULE_6__["useDispatch"], react_redux__WEBPACK_IMPORTED_MODULE_6__["useSelector"]];
});

_c = ModuleProductActions;
/* harmony default export */ __webpack_exports__["default"] = (ModuleProductActions);

var _c;

$RefreshReg$(_c, "ModuleProductActions");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9lbGVtZW50cy9wcm9kdWN0cy9tb2R1bGVzL01vZHVsZVByb2R1Y3RBY3Rpb25zLmpzIl0sIm5hbWVzIjpbIk1vZHVsZVByb2R1Y3RBY3Rpb25zIiwicHJvZHVjdCIsImRpc3BhdGNoIiwidXNlRGlzcGF0Y2giLCJjYXJ0SXRlbXMiLCJ1c2VTZWxlY3RvciIsInN0YXRlIiwiY2FydCIsInVzZVN0YXRlIiwiaXNRdWlja1ZpZXciLCJzZXRJc1F1aWNrVmlldyIsImhhbmRsZUFkZEl0ZW1Ub0NhcnQiLCJlIiwicHJldmVudERlZmF1bHQiLCJhZGRJdGVtIiwiaGFuZGxlQWRkSXRlbVRvQ29tcGFyZSIsImFkZEl0ZW1Ub0NvbXBhcmUiLCJoYW5kbGVBZGRJdGVtVG9XaXNobGlzdCIsImFkZEl0ZW1Ub1dpc2hsaXN0IiwiaGFuZGxlU2hvd1F1aWNrVmlldyIsImhhbmRsZUhpZGVRdWlja1ZpZXciXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLElBQU1BLG9CQUFvQixHQUFHLFNBQXZCQSxvQkFBdUIsT0FBaUI7QUFBQTs7QUFBQSxNQUFkQyxPQUFjLFFBQWRBLE9BQWM7QUFDNUMsTUFBTUMsUUFBUSxHQUFHQywrREFBVyxFQUE1QjtBQUNBLE1BQU1DLFNBQVMsR0FBR0MsK0RBQVcsQ0FBQyxVQUFDQyxLQUFEO0FBQUEsV0FBV0EsS0FBSyxDQUFDQyxJQUFOLENBQVdILFNBQXRCO0FBQUEsR0FBRCxDQUE3Qjs7QUFGNEMsa0JBR05JLHNEQUFRLENBQUMsS0FBRCxDQUhGO0FBQUEsTUFHckNDLFdBSHFDO0FBQUEsTUFHeEJDLGNBSHdCOztBQUs1QyxNQUFNQyxtQkFBbUIsR0FBRyxTQUF0QkEsbUJBQXNCLENBQUNDLENBQUQsRUFBTztBQUNqQ0EsS0FBQyxDQUFDQyxjQUFGO0FBQ0FYLFlBQVEsQ0FBQ1ksa0VBQU8sQ0FBQ2IsT0FBRCxDQUFSLENBQVI7QUFDRCxHQUhEOztBQUtBLE1BQU1jLHNCQUFzQixHQUFHLFNBQXpCQSxzQkFBeUIsQ0FBQ0gsQ0FBRCxFQUFPO0FBQ3BDQSxLQUFDLENBQUNDLGNBQUY7QUFDQVgsWUFBUSxDQUFDYyw4RUFBZ0IsQ0FBQ2YsT0FBRCxDQUFqQixDQUFSO0FBQ0QsR0FIRDs7QUFLQSxNQUFNZ0IsdUJBQXVCLEdBQUcsU0FBMUJBLHVCQUEwQixDQUFDTCxDQUFELEVBQU87QUFDckNWLFlBQVEsQ0FBQ2dCLGdGQUFpQixDQUFDakIsT0FBRCxDQUFsQixDQUFSO0FBQ0QsR0FGRDs7QUFJQSxNQUFNa0IsbUJBQW1CLEdBQUcsU0FBdEJBLG1CQUFzQixDQUFDUCxDQUFELEVBQU87QUFDakNBLEtBQUMsQ0FBQ0MsY0FBRjtBQUNBSCxrQkFBYyxDQUFDLElBQUQsQ0FBZDtBQUNELEdBSEQ7O0FBS0EsTUFBTVUsbUJBQW1CLEdBQUcsU0FBdEJBLG1CQUFzQixDQUFDUixDQUFELEVBQU87QUFDakNBLEtBQUMsQ0FBQ0MsY0FBRjtBQUNBSCxrQkFBYyxDQUFDLEtBQUQsQ0FBZDtBQUNELEdBSEQ7O0FBSUEsc0JBQ0U7QUFBSSxhQUFTLEVBQUMscUJBQWQ7QUFBQSw0QkFXRTtBQUFBLDZCQUNFO0FBQ0UsWUFBSSxFQUFDLEdBRFA7QUFFRSx1QkFBWSxTQUZkO0FBR0UsMEJBQWUsS0FIakI7QUFJRSxhQUFLLEVBQUMsWUFKUjtBQUtFLGVBQU8sRUFBRVMsbUJBTFg7QUFBQSwrQkFPRTtBQUFHLG1CQUFTLEVBQUM7QUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFYRixlQXNCRTtBQUFBLDZCQUNFO0FBQ0UsWUFBSSxFQUFDLEdBRFA7QUFFRSx1QkFBWSxTQUZkO0FBR0UsMEJBQWUsS0FIakI7QUFJRSxhQUFLLEVBQUMsaUJBSlI7QUFLRSxlQUFPLEVBQUVGLHVCQUxYO0FBQUEsK0JBT0U7QUFBRyxtQkFBUyxFQUFDO0FBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVBGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGFBdEJGLGVBaUNFO0FBQUEsNkJBQ0U7QUFDRSxZQUFJLEVBQUMsR0FEUDtBQUVFLHVCQUFZLFNBRmQ7QUFHRSwwQkFBZSxLQUhqQjtBQUlFLGFBQUssRUFBQyxTQUpSO0FBS0UsZUFBTyxFQUFFRixzQkFMWDtBQUFBLCtCQU9FO0FBQUcsbUJBQVMsRUFBQztBQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFQRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxhQWpDRixlQTRDRSxxRUFBQywwQ0FBRDtBQUNFLGNBQVEsTUFEVjtBQUVFLFlBQU0sRUFBRSxJQUZWO0FBR0UsV0FBSyxFQUFFLElBSFQ7QUFJRSxjQUFRLEVBQUUsa0JBQUNILENBQUQ7QUFBQSxlQUFPUSxtQkFBbUIsQ0FBQ1IsQ0FBRCxDQUExQjtBQUFBLE9BSlo7QUFLRSxhQUFPLEVBQUVILFdBTFg7QUFNRSxlQUFTLGVBQUU7QUFBRyxpQkFBUyxFQUFDO0FBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQU5iO0FBQUEsOEJBUUU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFSRixlQVNFLHFFQUFDLDBGQUFEO0FBQXdCLGVBQU8sRUFBRVI7QUFBakM7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQVRGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxhQTVDRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsV0FERjtBQTBERCxDQXRGRDs7R0FBTUQsb0I7VUFDYUcsdUQsRUFDQ0UsdUQ7OztLQUZkTCxvQjtBQXdGU0EsbUZBQWYiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvcHJvZHVjdC9bcGlkXS40ZDcxOTM4NDRkMGFjYmE4NGMyOS5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IHVzZVN0YXRlIH0gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCB7IE1vZGFsIH0gZnJvbSBcImFudGRcIjtcclxuaW1wb3J0IHsgYWRkSXRlbSB9IGZyb20gXCJ+L3N0b3JlL2NhcnQvYWN0aW9uXCI7XHJcbmltcG9ydCB7IGFkZEl0ZW1Ub0NvbXBhcmUgfSBmcm9tIFwifi9zdG9yZS9jb21wYXJlL2FjdGlvblwiO1xyXG5pbXBvcnQgeyBhZGRJdGVtVG9XaXNobGlzdCB9IGZyb20gXCJ+L3N0b3JlL3dpc2hsaXN0L2FjdGlvblwiO1xyXG5pbXBvcnQgeyB1c2VEaXNwYXRjaCwgdXNlU2VsZWN0b3IgfSBmcm9tIFwicmVhY3QtcmVkdXhcIjtcclxuaW1wb3J0IFByb2R1Y3REZXRhaWxRdWlja1ZpZXcgZnJvbSBcIn4vY29tcG9uZW50cy9lbGVtZW50cy9kZXRhaWwvUHJvZHVjdERldGFpbFF1aWNrVmlld1wiO1xyXG5cclxuY29uc3QgTW9kdWxlUHJvZHVjdEFjdGlvbnMgPSAoeyBwcm9kdWN0IH0pID0+IHtcclxuICBjb25zdCBkaXNwYXRjaCA9IHVzZURpc3BhdGNoKCk7XHJcbiAgY29uc3QgY2FydEl0ZW1zID0gdXNlU2VsZWN0b3IoKHN0YXRlKSA9PiBzdGF0ZS5jYXJ0LmNhcnRJdGVtcyk7XHJcbiAgY29uc3QgW2lzUXVpY2tWaWV3LCBzZXRJc1F1aWNrVmlld10gPSB1c2VTdGF0ZShmYWxzZSk7XHJcblxyXG4gIGNvbnN0IGhhbmRsZUFkZEl0ZW1Ub0NhcnQgPSAoZSkgPT4ge1xyXG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgZGlzcGF0Y2goYWRkSXRlbShwcm9kdWN0KSk7XHJcbiAgfTtcclxuXHJcbiAgY29uc3QgaGFuZGxlQWRkSXRlbVRvQ29tcGFyZSA9IChlKSA9PiB7XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICBkaXNwYXRjaChhZGRJdGVtVG9Db21wYXJlKHByb2R1Y3QpKTtcclxuICB9O1xyXG5cclxuICBjb25zdCBoYW5kbGVBZGRJdGVtVG9XaXNobGlzdCA9IChlKSA9PiB7XHJcbiAgICBkaXNwYXRjaChhZGRJdGVtVG9XaXNobGlzdChwcm9kdWN0KSk7XHJcbiAgfTtcclxuXHJcbiAgY29uc3QgaGFuZGxlU2hvd1F1aWNrVmlldyA9IChlKSA9PiB7XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICBzZXRJc1F1aWNrVmlldyh0cnVlKTtcclxuICB9O1xyXG5cclxuICBjb25zdCBoYW5kbGVIaWRlUXVpY2tWaWV3ID0gKGUpID0+IHtcclxuICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgIHNldElzUXVpY2tWaWV3KGZhbHNlKTtcclxuICB9O1xyXG4gIHJldHVybiAoXHJcbiAgICA8dWwgY2xhc3NOYW1lPVwicHMtcHJvZHVjdF9fYWN0aW9uc1wiPlxyXG4gICAgICB7Lyoge2NhcnRJdGVtcz8ubGVuZ3RoIDwgMSAmJjxsaT5cclxuICAgICAgICAgICAgICAgIDxhXHJcbiAgICAgICAgICAgICAgICAgICAgaHJlZj1cIiNcIlxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGEtdG9nZ2xlPVwidG9vbHRpcFwiXHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YS1wbGFjZW1lbnQ9XCJ0b3BcIlxyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlPVwiQWRkIFRvIENhcnRcIlxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e2hhbmRsZUFkZEl0ZW1Ub0NhcnR9PlxyXG4gICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImljb24tYmFnMlwiPjwvaT5cclxuICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgPC9saT59ICovfVxyXG4gICAgICA8bGk+XHJcbiAgICAgICAgPGFcclxuICAgICAgICAgIGhyZWY9XCIjXCJcclxuICAgICAgICAgIGRhdGEtdG9nZ2xlPVwidG9vbHRpcFwiXHJcbiAgICAgICAgICBkYXRhLXBsYWNlbWVudD1cInRvcFwiXHJcbiAgICAgICAgICB0aXRsZT1cIlF1aWNrIFZpZXdcIlxyXG4gICAgICAgICAgb25DbGljaz17aGFuZGxlU2hvd1F1aWNrVmlld31cclxuICAgICAgICA+XHJcbiAgICAgICAgICA8aSBjbGFzc05hbWU9XCJpY29uLWV5ZVwiPjwvaT5cclxuICAgICAgICA8L2E+XHJcbiAgICAgIDwvbGk+XHJcbiAgICAgIDxsaT5cclxuICAgICAgICA8YVxyXG4gICAgICAgICAgaHJlZj1cIiNcIlxyXG4gICAgICAgICAgZGF0YS10b2dnbGU9XCJ0b29sdGlwXCJcclxuICAgICAgICAgIGRhdGEtcGxhY2VtZW50PVwidG9wXCJcclxuICAgICAgICAgIHRpdGxlPVwiQWRkIHRvIHdpc2hsaXN0XCJcclxuICAgICAgICAgIG9uQ2xpY2s9e2hhbmRsZUFkZEl0ZW1Ub1dpc2hsaXN0fVxyXG4gICAgICAgID5cclxuICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImljb24taGVhcnRcIj48L2k+XHJcbiAgICAgICAgPC9hPlxyXG4gICAgICA8L2xpPlxyXG4gICAgICA8bGk+XHJcbiAgICAgICAgPGFcclxuICAgICAgICAgIGhyZWY9XCIjXCJcclxuICAgICAgICAgIGRhdGEtdG9nZ2xlPVwidG9vbHRpcFwiXHJcbiAgICAgICAgICBkYXRhLXBsYWNlbWVudD1cInRvcFwiXHJcbiAgICAgICAgICB0aXRsZT1cIkNvbXBhcmVcIlxyXG4gICAgICAgICAgb25DbGljaz17aGFuZGxlQWRkSXRlbVRvQ29tcGFyZX1cclxuICAgICAgICA+XHJcbiAgICAgICAgICA8aSBjbGFzc05hbWU9XCJpY29uLWNoYXJ0LWJhcnNcIj48L2k+XHJcbiAgICAgICAgPC9hPlxyXG4gICAgICA8L2xpPlxyXG4gICAgICA8TW9kYWxcclxuICAgICAgICBjZW50ZXJlZFxyXG4gICAgICAgIGZvb3Rlcj17bnVsbH1cclxuICAgICAgICB3aWR0aD17MTAyNH1cclxuICAgICAgICBvbkNhbmNlbD17KGUpID0+IGhhbmRsZUhpZGVRdWlja1ZpZXcoZSl9XHJcbiAgICAgICAgdmlzaWJsZT17aXNRdWlja1ZpZXd9XHJcbiAgICAgICAgY2xvc2VJY29uPXs8aSBjbGFzc05hbWU9XCJpY29uIGljb24tY3Jvc3MyXCI+PC9pPn1cclxuICAgICAgPlxyXG4gICAgICAgIDxoMz5RdWlja3ZpZXc8L2gzPlxyXG4gICAgICAgIDxQcm9kdWN0RGV0YWlsUXVpY2tWaWV3IHByb2R1Y3Q9e3Byb2R1Y3R9IC8+XHJcbiAgICAgIDwvTW9kYWw+XHJcbiAgICA8L3VsPlxyXG4gICk7XHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBNb2R1bGVQcm9kdWN0QWN0aW9ucztcclxuIl0sInNvdXJjZVJvb3QiOiIifQ==