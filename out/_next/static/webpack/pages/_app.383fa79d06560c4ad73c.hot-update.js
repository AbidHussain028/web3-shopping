webpackHotUpdate_N_E("pages/_app",{

/***/ "./store/cart/saga.js":
/*!****************************!*\
  !*** ./store/cart/saga.js ***!
  \****************************/
/*! exports provided: calculateAmount, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "calculateAmount", function() { return calculateAmount; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return rootSaga; });
/* harmony import */ var D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/toConsumableArray */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/toConsumableArray.js");
/* harmony import */ var D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/regenerator */ "./node_modules/next/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! redux-saga/effects */ "./node_modules/redux-saga/dist/redux-saga-effects-npm-proxy.esm.js");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _action__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./action */ "./store/cart/action.js");



var _marked = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(getCartSaga),
    _marked2 = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(addItemSaga),
    _marked3 = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(removeItemSaga),
    _marked4 = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(increaseQtySaga),
    _marked5 = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(decreaseItemQtySaga),
    _marked6 = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(clearCartSaga),
    _marked7 = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(rootSaga);





var modalSuccess = function modalSuccess(type) {
  antd__WEBPACK_IMPORTED_MODULE_3__["notification"][type]({
    message: "Success",
    description: "This product has been added to your cart!",
    duration: 1
  });
};

var modalWarning = function modalWarning(type) {
  antd__WEBPACK_IMPORTED_MODULE_3__["notification"][type]({
    message: "Remove A Item",
    description: "This product has been removed from your cart!",
    duration: 1
  });
};

var calculateAmount = function calculateAmount(obj) {
  return Object.values(obj).reduce(function (acc, _ref) {
    var quantity = _ref.quantity,
        price = _ref.price;
    return acc + quantity * price;
  }, 0).toFixed(2);
};

function getCartSaga() {
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function getCartSaga$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["getCartSuccess"])());

        case 3:
          _context.next = 9;
          break;

        case 5:
          _context.prev = 5;
          _context.t0 = _context["catch"](0);
          _context.next = 9;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["getCartError"])(_context.t0));

        case 9:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, null, [[0, 5]]);
}

function addItemSaga(payload) {
  var product, localCart, currentCart, existItem;
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function addItemSaga$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          product = payload.product;
          localCart = JSON.parse(localStorage.getItem("persist:martfury")).cart;
          currentCart = JSON.parse(localCart);
          existItem = currentCart.cartItems.find(function (item) {
            return item.id === product.id;
          });

          if (existItem) {
            existItem.quantity += product.quantity;
          } else {
            if (!product.quantity) {
              product.quantity = 1;
            }

            currentCart.cartItems = Object(D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__["default"])(product);
          }

          currentCart.amount = calculateAmount(currentCart.cartItems);
          currentCart.cartTotal++;
          _context2.next = 10;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["updateCartSuccess"])(currentCart));

        case 10:
          modalSuccess("success");
          _context2.next = 17;
          break;

        case 13:
          _context2.prev = 13;
          _context2.t0 = _context2["catch"](0);
          _context2.next = 17;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["getCartError"])(_context2.t0));

        case 17:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2, null, [[0, 13]]);
}

function removeItemSaga(payload) {
  var product, localCart, index;
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function removeItemSaga$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          product = payload.product;
          localCart = JSON.parse(JSON.parse(localStorage.getItem("persist:martfury")).cart);
          index = localCart.cartItems.findIndex(function (item) {
            return item.id === product.id;
          });
          localCart.cartTotal = localCart.cartTotal - product.quantity;
          localCart.cartItems.splice(index, 1);
          localCart.amount = calculateAmount(localCart.cartItems);

          if (localCart.cartItems.length === 0) {
            localCart.cartItems = [];
            localCart.amount = 0;
            localCart.cartTotal = 0;
          }

          _context3.next = 10;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["updateCartSuccess"])(localCart));

        case 10:
          modalWarning("warning");
          _context3.next = 17;
          break;

        case 13:
          _context3.prev = 13;
          _context3.t0 = _context3["catch"](0);
          _context3.next = 17;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["getCartError"])(_context3.t0));

        case 17:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3, null, [[0, 13]]);
}

function increaseQtySaga(payload) {
  var product, localCart, selectedItem;
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function increaseQtySaga$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.prev = 0;
          product = payload.product;
          localCart = JSON.parse(JSON.parse(localStorage.getItem("persist:martfury")).cart);
          selectedItem = localCart.cartItems.find(function (item) {
            return item.id === product.id;
          });

          if (selectedItem) {
            selectedItem.quantity++;
            localCart.cartTotal++;
            localCart.amount = calculateAmount(localCart.cartItems);
          }

          _context4.next = 7;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["updateCartSuccess"])(localCart));

        case 7:
          _context4.next = 13;
          break;

        case 9:
          _context4.prev = 9;
          _context4.t0 = _context4["catch"](0);
          _context4.next = 13;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["getCartError"])(_context4.t0));

        case 13:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4, null, [[0, 9]]);
}

function decreaseItemQtySaga(payload) {
  var product, localCart, selectedItem;
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function decreaseItemQtySaga$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.prev = 0;
          product = payload.product;
          localCart = JSON.parse(JSON.parse(localStorage.getItem("persist:martfury")).cart);
          selectedItem = localCart.cartItems.find(function (item) {
            return item.id === product.id;
          });

          if (selectedItem) {
            selectedItem.quantity--;
            localCart.cartTotal--;
            localCart.amount = calculateAmount(localCart.cartItems);
          }

          _context5.next = 7;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["updateCartSuccess"])(localCart));

        case 7:
          _context5.next = 13;
          break;

        case 9:
          _context5.prev = 9;
          _context5.t0 = _context5["catch"](0);
          _context5.next = 13;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["getCartError"])(_context5.t0));

        case 13:
        case "end":
          return _context5.stop();
      }
    }
  }, _marked5, null, [[0, 9]]);
}

function clearCartSaga() {
  var emptyCart;
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function clearCartSaga$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.prev = 0;
          emptyCart = {
            cartItems: [],
            amount: 0,
            cartTotal: 0
          };
          _context6.next = 4;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["updateCartSuccess"])(emptyCart));

        case 4:
          _context6.next = 10;
          break;

        case 6:
          _context6.prev = 6;
          _context6.t0 = _context6["catch"](0);
          _context6.next = 10;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["updateCartError"])(_context6.t0));

        case 10:
        case "end":
          return _context6.stop();
      }
    }
  }, _marked6, null, [[0, 6]]);
}

function rootSaga() {
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function rootSaga$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          _context7.next = 2;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["all"])([Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["takeEvery"])(_action__WEBPACK_IMPORTED_MODULE_4__["actionTypes"].GET_CART, getCartSaga)]);

        case 2:
          _context7.next = 4;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["all"])([Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["takeEvery"])(_action__WEBPACK_IMPORTED_MODULE_4__["actionTypes"].ADD_ITEM, addItemSaga)]);

        case 4:
          _context7.next = 6;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["all"])([Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["takeEvery"])(_action__WEBPACK_IMPORTED_MODULE_4__["actionTypes"].REMOVE_ITEM, removeItemSaga)]);

        case 6:
          _context7.next = 8;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["all"])([Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["takeEvery"])(_action__WEBPACK_IMPORTED_MODULE_4__["actionTypes"].INCREASE_QTY, increaseQtySaga)]);

        case 8:
          _context7.next = 10;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["all"])([Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["takeEvery"])(_action__WEBPACK_IMPORTED_MODULE_4__["actionTypes"].DECREASE_QTY, decreaseItemQtySaga)]);

        case 10:
        case "end":
          return _context7.stop();
      }
    }
  }, _marked7);
}

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vc3RvcmUvY2FydC9zYWdhLmpzIl0sIm5hbWVzIjpbImdldENhcnRTYWdhIiwiYWRkSXRlbVNhZ2EiLCJyZW1vdmVJdGVtU2FnYSIsImluY3JlYXNlUXR5U2FnYSIsImRlY3JlYXNlSXRlbVF0eVNhZ2EiLCJjbGVhckNhcnRTYWdhIiwicm9vdFNhZ2EiLCJtb2RhbFN1Y2Nlc3MiLCJ0eXBlIiwibm90aWZpY2F0aW9uIiwibWVzc2FnZSIsImRlc2NyaXB0aW9uIiwiZHVyYXRpb24iLCJtb2RhbFdhcm5pbmciLCJjYWxjdWxhdGVBbW91bnQiLCJvYmoiLCJPYmplY3QiLCJ2YWx1ZXMiLCJyZWR1Y2UiLCJhY2MiLCJxdWFudGl0eSIsInByaWNlIiwidG9GaXhlZCIsInB1dCIsImdldENhcnRTdWNjZXNzIiwiZ2V0Q2FydEVycm9yIiwicGF5bG9hZCIsInByb2R1Y3QiLCJsb2NhbENhcnQiLCJKU09OIiwicGFyc2UiLCJsb2NhbFN0b3JhZ2UiLCJnZXRJdGVtIiwiY2FydCIsImN1cnJlbnRDYXJ0IiwiZXhpc3RJdGVtIiwiY2FydEl0ZW1zIiwiZmluZCIsIml0ZW0iLCJpZCIsImFtb3VudCIsImNhcnRUb3RhbCIsInVwZGF0ZUNhcnRTdWNjZXNzIiwiaW5kZXgiLCJmaW5kSW5kZXgiLCJzcGxpY2UiLCJsZW5ndGgiLCJzZWxlY3RlZEl0ZW0iLCJlbXB0eUNhcnQiLCJ1cGRhdGVDYXJ0RXJyb3IiLCJhbGwiLCJ0YWtlRXZlcnkiLCJhY3Rpb25UeXBlcyIsIkdFVF9DQVJUIiwiQUREX0lURU0iLCJSRU1PVkVfSVRFTSIsIklOQ1JFQVNFX1FUWSIsIkRFQ1JFQVNFX1FUWSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozt3S0ErQlVBLFc7eUtBUUFDLFc7eUtBeUJBQyxjO3lLQXNCQUMsZTt5S0FvQkFDLG1CO3lLQXFCQUMsYTt5S0FhZUMsUTs7QUE1SXpCO0FBQ0E7QUFFQTs7QUFRQSxJQUFNQyxZQUFZLEdBQUcsU0FBZkEsWUFBZSxDQUFDQyxJQUFELEVBQVU7QUFDN0JDLG1EQUFZLENBQUNELElBQUQsQ0FBWixDQUFtQjtBQUNqQkUsV0FBTyxFQUFFLFNBRFE7QUFFakJDLGVBQVcsRUFBRSwyQ0FGSTtBQUdqQkMsWUFBUSxFQUFFO0FBSE8sR0FBbkI7QUFLRCxDQU5EOztBQU9BLElBQU1DLFlBQVksR0FBRyxTQUFmQSxZQUFlLENBQUNMLElBQUQsRUFBVTtBQUM3QkMsbURBQVksQ0FBQ0QsSUFBRCxDQUFaLENBQW1CO0FBQ2pCRSxXQUFPLEVBQUUsZUFEUTtBQUVqQkMsZUFBVyxFQUFFLCtDQUZJO0FBR2pCQyxZQUFRLEVBQUU7QUFITyxHQUFuQjtBQUtELENBTkQ7O0FBUU8sSUFBTUUsZUFBZSxHQUFHLFNBQWxCQSxlQUFrQixDQUFDQyxHQUFEO0FBQUEsU0FDN0JDLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjRixHQUFkLEVBQ0dHLE1BREgsQ0FDVSxVQUFDQyxHQUFEO0FBQUEsUUFBUUMsUUFBUixRQUFRQSxRQUFSO0FBQUEsUUFBa0JDLEtBQWxCLFFBQWtCQSxLQUFsQjtBQUFBLFdBQThCRixHQUFHLEdBQUdDLFFBQVEsR0FBR0MsS0FBL0M7QUFBQSxHQURWLEVBQ2dFLENBRGhFLEVBRUdDLE9BRkgsQ0FFVyxDQUZYLENBRDZCO0FBQUEsQ0FBeEI7O0FBS1AsU0FBVXRCLFdBQVY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFSSxpQkFBTXVCLDhEQUFHLENBQUNDLDhEQUFjLEVBQWYsQ0FBVDs7QUFGSjtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJSSxpQkFBTUQsOERBQUcsQ0FBQ0UsNERBQVksYUFBYixDQUFUOztBQUpKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQVFBLFNBQVV4QixXQUFWLENBQXNCeUIsT0FBdEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFWUMsaUJBRlosR0FFd0JELE9BRnhCLENBRVlDLE9BRlo7QUFHVUMsbUJBSFYsR0FHc0JDLElBQUksQ0FBQ0MsS0FBTCxDQUFXQyxZQUFZLENBQUNDLE9BQWIsQ0FBcUIsa0JBQXJCLENBQVgsRUFBcURDLElBSDNFO0FBSVFDLHFCQUpSLEdBSXNCTCxJQUFJLENBQUNDLEtBQUwsQ0FBV0YsU0FBWCxDQUp0QjtBQUtRTyxtQkFMUixHQUtvQkQsV0FBVyxDQUFDRSxTQUFaLENBQXNCQyxJQUF0QixDQUNkLFVBQUNDLElBQUQ7QUFBQSxtQkFBVUEsSUFBSSxDQUFDQyxFQUFMLEtBQVlaLE9BQU8sQ0FBQ1ksRUFBOUI7QUFBQSxXQURjLENBTHBCOztBQVFJLGNBQUlKLFNBQUosRUFBZTtBQUNiQSxxQkFBUyxDQUFDZixRQUFWLElBQXNCTyxPQUFPLENBQUNQLFFBQTlCO0FBQ0QsV0FGRCxNQUVPO0FBQ0wsZ0JBQUksQ0FBQ08sT0FBTyxDQUFDUCxRQUFiLEVBQXVCO0FBQ3JCTyxxQkFBTyxDQUFDUCxRQUFSLEdBQW1CLENBQW5CO0FBQ0Q7O0FBQ0RjLHVCQUFXLENBQUNFLFNBQVosc0tBQTRCVCxPQUE1QjtBQUNEOztBQUNETyxxQkFBVyxDQUFDTSxNQUFaLEdBQXFCMUIsZUFBZSxDQUFDb0IsV0FBVyxDQUFDRSxTQUFiLENBQXBDO0FBQ0FGLHFCQUFXLENBQUNPLFNBQVo7QUFqQko7QUFrQkksaUJBQU1sQiw4REFBRyxDQUFDbUIsaUVBQWlCLENBQUNSLFdBQUQsQ0FBbEIsQ0FBVDs7QUFsQko7QUFtQkkzQixzQkFBWSxDQUFDLFNBQUQsQ0FBWjtBQW5CSjtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBcUJJLGlCQUFNZ0IsOERBQUcsQ0FBQ0UsNERBQVksY0FBYixDQUFUOztBQXJCSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUF5QkEsU0FBVXZCLGNBQVYsQ0FBeUJ3QixPQUF6QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVZQyxpQkFGWixHQUV3QkQsT0FGeEIsQ0FFWUMsT0FGWjtBQUdRQyxtQkFIUixHQUdvQkMsSUFBSSxDQUFDQyxLQUFMLENBQ2RELElBQUksQ0FBQ0MsS0FBTCxDQUFXQyxZQUFZLENBQUNDLE9BQWIsQ0FBcUIsa0JBQXJCLENBQVgsRUFBcURDLElBRHZDLENBSHBCO0FBTVFVLGVBTlIsR0FNZ0JmLFNBQVMsQ0FBQ1EsU0FBVixDQUFvQlEsU0FBcEIsQ0FBOEIsVUFBQ04sSUFBRDtBQUFBLG1CQUFVQSxJQUFJLENBQUNDLEVBQUwsS0FBWVosT0FBTyxDQUFDWSxFQUE5QjtBQUFBLFdBQTlCLENBTmhCO0FBT0lYLG1CQUFTLENBQUNhLFNBQVYsR0FBc0JiLFNBQVMsQ0FBQ2EsU0FBVixHQUFzQmQsT0FBTyxDQUFDUCxRQUFwRDtBQUNBUSxtQkFBUyxDQUFDUSxTQUFWLENBQW9CUyxNQUFwQixDQUEyQkYsS0FBM0IsRUFBa0MsQ0FBbEM7QUFDQWYsbUJBQVMsQ0FBQ1ksTUFBVixHQUFtQjFCLGVBQWUsQ0FBQ2MsU0FBUyxDQUFDUSxTQUFYLENBQWxDOztBQUNBLGNBQUlSLFNBQVMsQ0FBQ1EsU0FBVixDQUFvQlUsTUFBcEIsS0FBK0IsQ0FBbkMsRUFBc0M7QUFDcENsQixxQkFBUyxDQUFDUSxTQUFWLEdBQXNCLEVBQXRCO0FBQ0FSLHFCQUFTLENBQUNZLE1BQVYsR0FBbUIsQ0FBbkI7QUFDQVoscUJBQVMsQ0FBQ2EsU0FBVixHQUFzQixDQUF0QjtBQUNEOztBQWRMO0FBZUksaUJBQU1sQiw4REFBRyxDQUFDbUIsaUVBQWlCLENBQUNkLFNBQUQsQ0FBbEIsQ0FBVDs7QUFmSjtBQWdCSWYsc0JBQVksQ0FBQyxTQUFELENBQVo7QUFoQko7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWtCSSxpQkFBTVUsOERBQUcsQ0FBQ0UsNERBQVksY0FBYixDQUFUOztBQWxCSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFzQkEsU0FBVXRCLGVBQVYsQ0FBMEJ1QixPQUExQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVZQyxpQkFGWixHQUV3QkQsT0FGeEIsQ0FFWUMsT0FGWjtBQUdRQyxtQkFIUixHQUdvQkMsSUFBSSxDQUFDQyxLQUFMLENBQ2RELElBQUksQ0FBQ0MsS0FBTCxDQUFXQyxZQUFZLENBQUNDLE9BQWIsQ0FBcUIsa0JBQXJCLENBQVgsRUFBcURDLElBRHZDLENBSHBCO0FBTVFjLHNCQU5SLEdBTXVCbkIsU0FBUyxDQUFDUSxTQUFWLENBQW9CQyxJQUFwQixDQUNqQixVQUFDQyxJQUFEO0FBQUEsbUJBQVVBLElBQUksQ0FBQ0MsRUFBTCxLQUFZWixPQUFPLENBQUNZLEVBQTlCO0FBQUEsV0FEaUIsQ0FOdkI7O0FBU0ksY0FBSVEsWUFBSixFQUFrQjtBQUNoQkEsd0JBQVksQ0FBQzNCLFFBQWI7QUFDQVEscUJBQVMsQ0FBQ2EsU0FBVjtBQUNBYixxQkFBUyxDQUFDWSxNQUFWLEdBQW1CMUIsZUFBZSxDQUFDYyxTQUFTLENBQUNRLFNBQVgsQ0FBbEM7QUFDRDs7QUFiTDtBQWNJLGlCQUFNYiw4REFBRyxDQUFDbUIsaUVBQWlCLENBQUNkLFNBQUQsQ0FBbEIsQ0FBVDs7QUFkSjtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFnQkksaUJBQU1MLDhEQUFHLENBQUNFLDREQUFZLGNBQWIsQ0FBVDs7QUFoQko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBb0JBLFNBQVVyQixtQkFBVixDQUE4QnNCLE9BQTlCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRVlDLGlCQUZaLEdBRXdCRCxPQUZ4QixDQUVZQyxPQUZaO0FBR1VDLG1CQUhWLEdBR3NCQyxJQUFJLENBQUNDLEtBQUwsQ0FDaEJELElBQUksQ0FBQ0MsS0FBTCxDQUFXQyxZQUFZLENBQUNDLE9BQWIsQ0FBcUIsa0JBQXJCLENBQVgsRUFBcURDLElBRHJDLENBSHRCO0FBTVFjLHNCQU5SLEdBTXVCbkIsU0FBUyxDQUFDUSxTQUFWLENBQW9CQyxJQUFwQixDQUNqQixVQUFDQyxJQUFEO0FBQUEsbUJBQVVBLElBQUksQ0FBQ0MsRUFBTCxLQUFZWixPQUFPLENBQUNZLEVBQTlCO0FBQUEsV0FEaUIsQ0FOdkI7O0FBVUksY0FBSVEsWUFBSixFQUFrQjtBQUNoQkEsd0JBQVksQ0FBQzNCLFFBQWI7QUFDQVEscUJBQVMsQ0FBQ2EsU0FBVjtBQUNBYixxQkFBUyxDQUFDWSxNQUFWLEdBQW1CMUIsZUFBZSxDQUFDYyxTQUFTLENBQUNRLFNBQVgsQ0FBbEM7QUFDRDs7QUFkTDtBQWVJLGlCQUFNYiw4REFBRyxDQUFDbUIsaUVBQWlCLENBQUNkLFNBQUQsQ0FBbEIsQ0FBVDs7QUFmSjtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFpQkksaUJBQU1MLDhEQUFHLENBQUNFLDREQUFZLGNBQWIsQ0FBVDs7QUFqQko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBcUJBLFNBQVVwQixhQUFWO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRVUyQyxtQkFGVixHQUVzQjtBQUNoQloscUJBQVMsRUFBRSxFQURLO0FBRWhCSSxrQkFBTSxFQUFFLENBRlE7QUFHaEJDLHFCQUFTLEVBQUU7QUFISyxXQUZ0QjtBQUFBO0FBT0ksaUJBQU1sQiw4REFBRyxDQUFDbUIsaUVBQWlCLENBQUNNLFNBQUQsQ0FBbEIsQ0FBVDs7QUFQSjtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTSSxpQkFBTXpCLDhEQUFHLENBQUMwQiwrREFBZSxjQUFoQixDQUFUOztBQVRKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQWFlLFNBQVUzQyxRQUFWO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNiLGlCQUFNNEMsOERBQUcsQ0FBQyxDQUFDQyxvRUFBUyxDQUFDQyxtREFBVyxDQUFDQyxRQUFiLEVBQXVCckQsV0FBdkIsQ0FBVixDQUFELENBQVQ7O0FBRGE7QUFBQTtBQUViLGlCQUFNa0QsOERBQUcsQ0FBQyxDQUFDQyxvRUFBUyxDQUFDQyxtREFBVyxDQUFDRSxRQUFiLEVBQXVCckQsV0FBdkIsQ0FBVixDQUFELENBQVQ7O0FBRmE7QUFBQTtBQUdiLGlCQUFNaUQsOERBQUcsQ0FBQyxDQUFDQyxvRUFBUyxDQUFDQyxtREFBVyxDQUFDRyxXQUFiLEVBQTBCckQsY0FBMUIsQ0FBVixDQUFELENBQVQ7O0FBSGE7QUFBQTtBQUliLGlCQUFNZ0QsOERBQUcsQ0FBQyxDQUFDQyxvRUFBUyxDQUFDQyxtREFBVyxDQUFDSSxZQUFiLEVBQTJCckQsZUFBM0IsQ0FBVixDQUFELENBQVQ7O0FBSmE7QUFBQTtBQUtiLGlCQUFNK0MsOERBQUcsQ0FBQyxDQUFDQyxvRUFBUyxDQUFDQyxtREFBVyxDQUFDSyxZQUFiLEVBQTJCckQsbUJBQTNCLENBQVYsQ0FBRCxDQUFUOztBQUxhO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL19hcHAuMzgzZmE3OWQwNjU2MGM0YWQ3M2MuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGFsbCwgcHV0LCB0YWtlRXZlcnkgfSBmcm9tIFwicmVkdXgtc2FnYS9lZmZlY3RzXCI7XHJcbmltcG9ydCB7IG5vdGlmaWNhdGlvbiB9IGZyb20gXCJhbnRkXCI7XHJcblxyXG5pbXBvcnQge1xyXG4gIGFjdGlvblR5cGVzLFxyXG4gIGdldENhcnRFcnJvcixcclxuICBnZXRDYXJ0U3VjY2VzcyxcclxuICB1cGRhdGVDYXJ0U3VjY2VzcyxcclxuICB1cGRhdGVDYXJ0RXJyb3IsXHJcbn0gZnJvbSBcIi4vYWN0aW9uXCI7XHJcblxyXG5jb25zdCBtb2RhbFN1Y2Nlc3MgPSAodHlwZSkgPT4ge1xyXG4gIG5vdGlmaWNhdGlvblt0eXBlXSh7XHJcbiAgICBtZXNzYWdlOiBcIlN1Y2Nlc3NcIixcclxuICAgIGRlc2NyaXB0aW9uOiBcIlRoaXMgcHJvZHVjdCBoYXMgYmVlbiBhZGRlZCB0byB5b3VyIGNhcnQhXCIsXHJcbiAgICBkdXJhdGlvbjogMSxcclxuICB9KTtcclxufTtcclxuY29uc3QgbW9kYWxXYXJuaW5nID0gKHR5cGUpID0+IHtcclxuICBub3RpZmljYXRpb25bdHlwZV0oe1xyXG4gICAgbWVzc2FnZTogXCJSZW1vdmUgQSBJdGVtXCIsXHJcbiAgICBkZXNjcmlwdGlvbjogXCJUaGlzIHByb2R1Y3QgaGFzIGJlZW4gcmVtb3ZlZCBmcm9tIHlvdXIgY2FydCFcIixcclxuICAgIGR1cmF0aW9uOiAxLFxyXG4gIH0pO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGNhbGN1bGF0ZUFtb3VudCA9IChvYmopID0+XHJcbiAgT2JqZWN0LnZhbHVlcyhvYmopXHJcbiAgICAucmVkdWNlKChhY2MsIHsgcXVhbnRpdHksIHByaWNlIH0pID0+IGFjYyArIHF1YW50aXR5ICogcHJpY2UsIDApXHJcbiAgICAudG9GaXhlZCgyKTtcclxuXHJcbmZ1bmN0aW9uKiBnZXRDYXJ0U2FnYSgpIHtcclxuICB0cnkge1xyXG4gICAgeWllbGQgcHV0KGdldENhcnRTdWNjZXNzKCkpO1xyXG4gIH0gY2F0Y2ggKGVycikge1xyXG4gICAgeWllbGQgcHV0KGdldENhcnRFcnJvcihlcnIpKTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uKiBhZGRJdGVtU2FnYShwYXlsb2FkKSB7XHJcbiAgdHJ5IHtcclxuICAgIGNvbnN0IHsgcHJvZHVjdCB9ID0gcGF5bG9hZDtcclxuICAgIGNvbnN0IGxvY2FsQ2FydCA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJwZXJzaXN0Om1hcnRmdXJ5XCIpKS5jYXJ0O1xyXG4gICAgbGV0IGN1cnJlbnRDYXJ0ID0gSlNPTi5wYXJzZShsb2NhbENhcnQpO1xyXG4gICAgbGV0IGV4aXN0SXRlbSA9IGN1cnJlbnRDYXJ0LmNhcnRJdGVtcy5maW5kKFxyXG4gICAgICAoaXRlbSkgPT4gaXRlbS5pZCA9PT0gcHJvZHVjdC5pZFxyXG4gICAgKTtcclxuICAgIGlmIChleGlzdEl0ZW0pIHtcclxuICAgICAgZXhpc3RJdGVtLnF1YW50aXR5ICs9IHByb2R1Y3QucXVhbnRpdHk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBpZiAoIXByb2R1Y3QucXVhbnRpdHkpIHtcclxuICAgICAgICBwcm9kdWN0LnF1YW50aXR5ID0gMTtcclxuICAgICAgfVxyXG4gICAgICBjdXJyZW50Q2FydC5jYXJ0SXRlbXMgPSBbLi4ucHJvZHVjdF07XHJcbiAgICB9XHJcbiAgICBjdXJyZW50Q2FydC5hbW91bnQgPSBjYWxjdWxhdGVBbW91bnQoY3VycmVudENhcnQuY2FydEl0ZW1zKTtcclxuICAgIGN1cnJlbnRDYXJ0LmNhcnRUb3RhbCsrO1xyXG4gICAgeWllbGQgcHV0KHVwZGF0ZUNhcnRTdWNjZXNzKGN1cnJlbnRDYXJ0KSk7XHJcbiAgICBtb2RhbFN1Y2Nlc3MoXCJzdWNjZXNzXCIpO1xyXG4gIH0gY2F0Y2ggKGVycikge1xyXG4gICAgeWllbGQgcHV0KGdldENhcnRFcnJvcihlcnIpKTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uKiByZW1vdmVJdGVtU2FnYShwYXlsb2FkKSB7XHJcbiAgdHJ5IHtcclxuICAgIGNvbnN0IHsgcHJvZHVjdCB9ID0gcGF5bG9hZDtcclxuICAgIGxldCBsb2NhbENhcnQgPSBKU09OLnBhcnNlKFxyXG4gICAgICBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwicGVyc2lzdDptYXJ0ZnVyeVwiKSkuY2FydFxyXG4gICAgKTtcclxuICAgIGxldCBpbmRleCA9IGxvY2FsQ2FydC5jYXJ0SXRlbXMuZmluZEluZGV4KChpdGVtKSA9PiBpdGVtLmlkID09PSBwcm9kdWN0LmlkKTtcclxuICAgIGxvY2FsQ2FydC5jYXJ0VG90YWwgPSBsb2NhbENhcnQuY2FydFRvdGFsIC0gcHJvZHVjdC5xdWFudGl0eTtcclxuICAgIGxvY2FsQ2FydC5jYXJ0SXRlbXMuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgIGxvY2FsQ2FydC5hbW91bnQgPSBjYWxjdWxhdGVBbW91bnQobG9jYWxDYXJ0LmNhcnRJdGVtcyk7XHJcbiAgICBpZiAobG9jYWxDYXJ0LmNhcnRJdGVtcy5sZW5ndGggPT09IDApIHtcclxuICAgICAgbG9jYWxDYXJ0LmNhcnRJdGVtcyA9IFtdO1xyXG4gICAgICBsb2NhbENhcnQuYW1vdW50ID0gMDtcclxuICAgICAgbG9jYWxDYXJ0LmNhcnRUb3RhbCA9IDA7XHJcbiAgICB9XHJcbiAgICB5aWVsZCBwdXQodXBkYXRlQ2FydFN1Y2Nlc3MobG9jYWxDYXJ0KSk7XHJcbiAgICBtb2RhbFdhcm5pbmcoXCJ3YXJuaW5nXCIpO1xyXG4gIH0gY2F0Y2ggKGVycikge1xyXG4gICAgeWllbGQgcHV0KGdldENhcnRFcnJvcihlcnIpKTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uKiBpbmNyZWFzZVF0eVNhZ2EocGF5bG9hZCkge1xyXG4gIHRyeSB7XHJcbiAgICBjb25zdCB7IHByb2R1Y3QgfSA9IHBheWxvYWQ7XHJcbiAgICBsZXQgbG9jYWxDYXJ0ID0gSlNPTi5wYXJzZShcclxuICAgICAgSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcInBlcnNpc3Q6bWFydGZ1cnlcIikpLmNhcnRcclxuICAgICk7XHJcbiAgICBsZXQgc2VsZWN0ZWRJdGVtID0gbG9jYWxDYXJ0LmNhcnRJdGVtcy5maW5kKFxyXG4gICAgICAoaXRlbSkgPT4gaXRlbS5pZCA9PT0gcHJvZHVjdC5pZFxyXG4gICAgKTtcclxuICAgIGlmIChzZWxlY3RlZEl0ZW0pIHtcclxuICAgICAgc2VsZWN0ZWRJdGVtLnF1YW50aXR5Kys7XHJcbiAgICAgIGxvY2FsQ2FydC5jYXJ0VG90YWwrKztcclxuICAgICAgbG9jYWxDYXJ0LmFtb3VudCA9IGNhbGN1bGF0ZUFtb3VudChsb2NhbENhcnQuY2FydEl0ZW1zKTtcclxuICAgIH1cclxuICAgIHlpZWxkIHB1dCh1cGRhdGVDYXJ0U3VjY2Vzcyhsb2NhbENhcnQpKTtcclxuICB9IGNhdGNoIChlcnIpIHtcclxuICAgIHlpZWxkIHB1dChnZXRDYXJ0RXJyb3IoZXJyKSk7XHJcbiAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiogZGVjcmVhc2VJdGVtUXR5U2FnYShwYXlsb2FkKSB7XHJcbiAgdHJ5IHtcclxuICAgIGNvbnN0IHsgcHJvZHVjdCB9ID0gcGF5bG9hZDtcclxuICAgIGNvbnN0IGxvY2FsQ2FydCA9IEpTT04ucGFyc2UoXHJcbiAgICAgIEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJwZXJzaXN0Om1hcnRmdXJ5XCIpKS5jYXJ0XHJcbiAgICApO1xyXG4gICAgbGV0IHNlbGVjdGVkSXRlbSA9IGxvY2FsQ2FydC5jYXJ0SXRlbXMuZmluZChcclxuICAgICAgKGl0ZW0pID0+IGl0ZW0uaWQgPT09IHByb2R1Y3QuaWRcclxuICAgICk7XHJcblxyXG4gICAgaWYgKHNlbGVjdGVkSXRlbSkge1xyXG4gICAgICBzZWxlY3RlZEl0ZW0ucXVhbnRpdHktLTtcclxuICAgICAgbG9jYWxDYXJ0LmNhcnRUb3RhbC0tO1xyXG4gICAgICBsb2NhbENhcnQuYW1vdW50ID0gY2FsY3VsYXRlQW1vdW50KGxvY2FsQ2FydC5jYXJ0SXRlbXMpO1xyXG4gICAgfVxyXG4gICAgeWllbGQgcHV0KHVwZGF0ZUNhcnRTdWNjZXNzKGxvY2FsQ2FydCkpO1xyXG4gIH0gY2F0Y2ggKGVycikge1xyXG4gICAgeWllbGQgcHV0KGdldENhcnRFcnJvcihlcnIpKTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uKiBjbGVhckNhcnRTYWdhKCkge1xyXG4gIHRyeSB7XHJcbiAgICBjb25zdCBlbXB0eUNhcnQgPSB7XHJcbiAgICAgIGNhcnRJdGVtczogW10sXHJcbiAgICAgIGFtb3VudDogMCxcclxuICAgICAgY2FydFRvdGFsOiAwLFxyXG4gICAgfTtcclxuICAgIHlpZWxkIHB1dCh1cGRhdGVDYXJ0U3VjY2VzcyhlbXB0eUNhcnQpKTtcclxuICB9IGNhdGNoIChlcnIpIHtcclxuICAgIHlpZWxkIHB1dCh1cGRhdGVDYXJ0RXJyb3IoZXJyKSk7XHJcbiAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiogcm9vdFNhZ2EoKSB7XHJcbiAgeWllbGQgYWxsKFt0YWtlRXZlcnkoYWN0aW9uVHlwZXMuR0VUX0NBUlQsIGdldENhcnRTYWdhKV0pO1xyXG4gIHlpZWxkIGFsbChbdGFrZUV2ZXJ5KGFjdGlvblR5cGVzLkFERF9JVEVNLCBhZGRJdGVtU2FnYSldKTtcclxuICB5aWVsZCBhbGwoW3Rha2VFdmVyeShhY3Rpb25UeXBlcy5SRU1PVkVfSVRFTSwgcmVtb3ZlSXRlbVNhZ2EpXSk7XHJcbiAgeWllbGQgYWxsKFt0YWtlRXZlcnkoYWN0aW9uVHlwZXMuSU5DUkVBU0VfUVRZLCBpbmNyZWFzZVF0eVNhZ2EpXSk7XHJcbiAgeWllbGQgYWxsKFt0YWtlRXZlcnkoYWN0aW9uVHlwZXMuREVDUkVBU0VfUVRZLCBkZWNyZWFzZUl0ZW1RdHlTYWdhKV0pO1xyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiIn0=