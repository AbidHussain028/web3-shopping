webpackHotUpdate_N_E("pages/account/login",{

/***/ "./components/partials/account/Login.jsx":
/*!***********************************************!*\
  !*** ./components/partials/account/Login.jsx ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/regenerator */ "./node_modules/next/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _store_auth_action__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../store/auth/action */ "./store/auth/action.js");
/* harmony import */ var web3__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! web3 */ "./node_modules/web3/lib/index.js");
/* harmony import */ var web3__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(web3__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var react_lazyload__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! react-lazyload */ "./node_modules/react-lazyload/lib/index.js");
/* harmony import */ var react_lazyload__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(react_lazyload__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");










var _jsxFileName = "F:\\template\\New folder\\web3-shopping\\components\\partials\\account\\Login.jsx";

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = Object(F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_8__["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = Object(F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_8__["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return Object(F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_7__["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }










var Login = /*#__PURE__*/function (_Component) {
  Object(F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(Login, _Component);

  var _super = _createSuper(Login);

  function Login(props) {
    var _this;

    Object(F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Login);

    _this = _super.call(this, props);

    Object(F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_9__["default"])(Object(F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5__["default"])(_this), "handleLoginSubmit", function (e) {
      console.log('test');

      _this.loadWeb3(); // this.props.dispatch(login());
      // Router.push('/');

    });

    Object(F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_9__["default"])(Object(F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5__["default"])(_this), "loadWeb3", /*#__PURE__*/Object(F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])( /*#__PURE__*/F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee() {
      var isConnected, web3, accounts;
      return F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              isConnected = false;
              _context.prev = 1;

              if (!window.ethereum) {
                _context.next = 9;
                break;
              }

              window.web3 = new web3__WEBPACK_IMPORTED_MODULE_14___default.a(window.ethereum);
              _context.next = 6;
              return window.ethereum.enable();

            case 6:
              isConnected = true;
              _context.next = 10;
              break;

            case 9:
              if (window.web3) {
                window.web3 = new web3__WEBPACK_IMPORTED_MODULE_14___default.a(window.web3.currentProvider);
                isConnected = true;
              } else {
                isConnected = false;
                antd__WEBPACK_IMPORTED_MODULE_16__["notification"].open({
                  message: 'Metamask is not installed',
                  description: ' please install it on your browser to connect.',
                  duration: 500
                }); // "Metamask is not installed, please install it on your browser to connect.",
              }

            case 10:
              if (!(isConnected === true)) {
                _context.next = 18;
                break;
              }

              web3 = window.web3;
              _context.next = 14;
              return web3.eth.getAccounts();

            case 14:
              accounts = _context.sent;

              _this.props.dispatch(Object(_store_auth_action__WEBPACK_IMPORTED_MODULE_13__["login"])(accounts[0]));

              next_router__WEBPACK_IMPORTED_MODULE_12___default.a.push('/');
              window.ethereum.on('accountsChanged', function (accounts) {
                this.props.dispatch(Object(_store_auth_action__WEBPACK_IMPORTED_MODULE_13__["login"])(accounts[0]));
              });

            case 18:
              _context.next = 23;
              break;

            case 20:
              _context.prev = 20;
              _context.t0 = _context["catch"](1);
              console.log(_context.t0);

            case 23:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[1, 20]]);
    })));

    _this.state = {};
    return _this;
  }

  Object(F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__["default"])(Login, [{
    key: "handleFeatureWillUpdate",
    value: function handleFeatureWillUpdate(e) {
      e.preventDefault();
      antd__WEBPACK_IMPORTED_MODULE_16__["notification"].open({
        message: 'Opp! Something went wrong.',
        description: 'This feature has been updated later!',
        duration: 500
      });
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        className: "ps-my-account",
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          className: "container",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_16__["Form"], {
            className: "ps-form--account",
            onFinish: this.handleLoginSubmit.bind(this),
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              className: "ps-tab active",
              id: "sign-in",
              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                className: "ps-form__content",
                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: "Connect to your wallet"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 82,
                  columnNumber: 33
                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                  className: "form-group submit",
                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("button", {
                    type: "submit",
                    className: "ps-btn ps-btn--fullwidth",
                    style: {
                      padding: '15px 0px'
                    },
                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_lazyload__WEBPACK_IMPORTED_MODULE_15___default.a, {
                      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                        src: "/static/img/meta-mask.png",
                        alt: "martfury",
                        style: {
                          paddingRight: '15px'
                        }
                      }, void 0, false, {
                        fileName: _jsxFileName,
                        lineNumber: 90,
                        columnNumber: 45
                      }, this)
                    }, void 0, false, {
                      fileName: _jsxFileName,
                      lineNumber: 89,
                      columnNumber: 41
                    }, this), "Connect metamask"]
                  }, void 0, true, {
                    fileName: _jsxFileName,
                    lineNumber: 85,
                    columnNumber: 37
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 84,
                  columnNumber: 33
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 81,
                columnNumber: 29
              }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                className: "ps-form__footer"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 100,
                columnNumber: 29
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 80,
              columnNumber: 25
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 77,
            columnNumber: 21
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 76,
          columnNumber: 17
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 75,
        columnNumber: 13
      }, this);
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(props) {
      if (props.isLoggedIn === true) {
        next_router__WEBPACK_IMPORTED_MODULE_12___default.a.push('/');
      }

      return false;
    }
  }]);

  return Login;
}(react__WEBPACK_IMPORTED_MODULE_10__["Component"]);

var mapStateToProps = function mapStateToProps(state) {
  return state.auth;
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_17__["connect"])(mapStateToProps)(Login));

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9wYXJ0aWFscy9hY2NvdW50L0xvZ2luLmpzeCJdLCJuYW1lcyI6WyJMb2dpbiIsInByb3BzIiwiZSIsImNvbnNvbGUiLCJsb2ciLCJsb2FkV2ViMyIsImlzQ29ubmVjdGVkIiwid2luZG93IiwiZXRoZXJldW0iLCJ3ZWIzIiwiV2ViMyIsImVuYWJsZSIsImN1cnJlbnRQcm92aWRlciIsIm5vdGlmaWNhdGlvbiIsIm9wZW4iLCJtZXNzYWdlIiwiZGVzY3JpcHRpb24iLCJkdXJhdGlvbiIsImV0aCIsImdldEFjY291bnRzIiwiYWNjb3VudHMiLCJkaXNwYXRjaCIsImxvZ2luIiwiUm91dGVyIiwicHVzaCIsIm9uIiwic3RhdGUiLCJwcmV2ZW50RGVmYXVsdCIsImhhbmRsZUxvZ2luU3VibWl0IiwiYmluZCIsInBhZGRpbmciLCJwYWRkaW5nUmlnaHQiLCJpc0xvZ2dlZEluIiwiQ29tcG9uZW50IiwibWFwU3RhdGVUb1Byb3BzIiwiYXV0aCIsImNvbm5lY3QiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBOztJQUVNQSxLOzs7OztBQUNGLGlCQUFZQyxLQUFaLEVBQW1CO0FBQUE7O0FBQUE7O0FBQ2YsOEJBQU1BLEtBQU47O0FBRGUsZ1dBcUJDLFVBQUNDLENBQUQsRUFBTztBQUN2QkMsYUFBTyxDQUFDQyxHQUFSLENBQVksTUFBWjs7QUFDQSxZQUFLQyxRQUFMLEdBRnVCLENBR3ZCO0FBQ0E7O0FBQ0gsS0ExQmtCOztBQUFBLHlwQkEyQlI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0hDLHlCQURHLEdBQ1csS0FEWDtBQUFBOztBQUFBLG1CQUdDQyxNQUFNLENBQUNDLFFBSFI7QUFBQTtBQUFBO0FBQUE7O0FBSUNELG9CQUFNLENBQUNFLElBQVAsR0FBYyxJQUFJQyw0Q0FBSixDQUFTSCxNQUFNLENBQUNDLFFBQWhCLENBQWQ7QUFKRDtBQUFBLHFCQUtPRCxNQUFNLENBQUNDLFFBQVAsQ0FBZ0JHLE1BQWhCLEVBTFA7O0FBQUE7QUFNQ0wseUJBQVcsR0FBRyxJQUFkO0FBTkQ7QUFBQTs7QUFBQTtBQU9JLGtCQUFJQyxNQUFNLENBQUNFLElBQVgsRUFBaUI7QUFDcEJGLHNCQUFNLENBQUNFLElBQVAsR0FBYyxJQUFJQyw0Q0FBSixDQUFTSCxNQUFNLENBQUNFLElBQVAsQ0FBWUcsZUFBckIsQ0FBZDtBQUNBTiwyQkFBVyxHQUFHLElBQWQ7QUFDSCxlQUhNLE1BR0E7QUFDSEEsMkJBQVcsR0FBRyxLQUFkO0FBQ0FPLGtFQUFZLENBQUNDLElBQWIsQ0FBa0I7QUFDZEMseUJBQU8sRUFBRSwyQkFESztBQUVkQyw2QkFBVyxFQUNQLGdEQUhVO0FBSWRDLDBCQUFRLEVBQUU7QUFKSSxpQkFBbEIsRUFGRyxDQVFIO0FBQ0g7O0FBbkJFO0FBQUEsb0JBb0JDWCxXQUFXLEtBQUssSUFwQmpCO0FBQUE7QUFBQTtBQUFBOztBQXFCT0csa0JBckJQLEdBcUJjRixNQUFNLENBQUNFLElBckJyQjtBQUFBO0FBQUEscUJBc0JzQkEsSUFBSSxDQUFDUyxHQUFMLENBQVNDLFdBQVQsRUF0QnRCOztBQUFBO0FBc0JLQyxzQkF0Qkw7O0FBdUJDLG9CQUFLbkIsS0FBTCxDQUFXb0IsUUFBWCxDQUFvQkMsaUVBQUssQ0FBQ0YsUUFBUSxDQUFDLENBQUQsQ0FBVCxDQUF6Qjs7QUFDQUcsaUVBQU0sQ0FBQ0MsSUFBUCxDQUFZLEdBQVo7QUFFQWpCLG9CQUFNLENBQUNDLFFBQVAsQ0FBZ0JpQixFQUFoQixDQUFtQixpQkFBbkIsRUFBc0MsVUFBVUwsUUFBVixFQUFvQjtBQUN0RCxxQkFBS25CLEtBQUwsQ0FBV29CLFFBQVgsQ0FBb0JDLGlFQUFLLENBQUNGLFFBQVEsQ0FBQyxDQUFELENBQVQsQ0FBekI7QUFDSCxlQUZEOztBQTFCRDtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBK0JIakIscUJBQU8sQ0FBQ0MsR0FBUjs7QUEvQkc7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0EzQlE7O0FBRWYsVUFBS3NCLEtBQUwsR0FBYSxFQUFiO0FBRmU7QUFHbEI7Ozs7NENBU3VCeEIsQyxFQUFHO0FBQ3ZCQSxPQUFDLENBQUN5QixjQUFGO0FBQ0FkLHdEQUFZLENBQUNDLElBQWIsQ0FBa0I7QUFDZEMsZUFBTyxFQUFFLDRCQURLO0FBRWRDLG1CQUFXLEVBQUUsc0NBRkM7QUFHZEMsZ0JBQVEsRUFBRTtBQUhJLE9BQWxCO0FBS0g7Ozs2QkEwQ1E7QUFDTCwwQkFDSTtBQUFLLGlCQUFTLEVBQUMsZUFBZjtBQUFBLCtCQUNJO0FBQUssbUJBQVMsRUFBQyxXQUFmO0FBQUEsaUNBQ0kscUVBQUMsMENBQUQ7QUFDSSxxQkFBUyxFQUFDLGtCQURkO0FBRUksb0JBQVEsRUFBRSxLQUFLVyxpQkFBTCxDQUF1QkMsSUFBdkIsQ0FBNEIsSUFBNUIsQ0FGZDtBQUFBLG1DQUdJO0FBQUssdUJBQVMsRUFBQyxlQUFmO0FBQStCLGdCQUFFLEVBQUMsU0FBbEM7QUFBQSxzQ0FDSTtBQUFLLHlCQUFTLEVBQUMsa0JBQWY7QUFBQSx3Q0FDSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFESixlQUdJO0FBQUssMkJBQVMsRUFBQyxtQkFBZjtBQUFBLHlDQUNJO0FBQ0ksd0JBQUksRUFBQyxRQURUO0FBRUksNkJBQVMsRUFBQywwQkFGZDtBQUdJLHlCQUFLLEVBQUU7QUFBRUMsNkJBQU8sRUFBRTtBQUFYLHFCQUhYO0FBQUEsNENBSUkscUVBQUMsc0RBQUQ7QUFBQSw2Q0FDSTtBQUNJLDJCQUFHLEVBQUMsMkJBRFI7QUFFSSwyQkFBRyxFQUFDLFVBRlI7QUFHSSw2QkFBSyxFQUFFO0FBQUVDLHNDQUFZLEVBQUU7QUFBaEI7QUFIWDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSw0QkFKSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQUhKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFESixlQW9CSTtBQUFLLHlCQUFTLEVBQUM7QUFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQXBCSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFISjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FESjtBQWdDSDs7OzZDQXpGK0I5QixLLEVBQU87QUFDbkMsVUFBSUEsS0FBSyxDQUFDK0IsVUFBTixLQUFxQixJQUF6QixFQUErQjtBQUMzQlQsMkRBQU0sQ0FBQ0MsSUFBUCxDQUFZLEdBQVo7QUFDSDs7QUFDRCxhQUFPLEtBQVA7QUFDSDs7OztFQVhlUyxnRDs7QUFpR3BCLElBQU1DLGVBQWUsR0FBRyxTQUFsQkEsZUFBa0IsQ0FBQ1IsS0FBRCxFQUFXO0FBQy9CLFNBQU9BLEtBQUssQ0FBQ1MsSUFBYjtBQUNILENBRkQ7O0FBR2VDLDJIQUFPLENBQUNGLGVBQUQsQ0FBUCxDQUF5QmxDLEtBQXpCLENBQWYiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvYWNjb3VudC9sb2dpbi4zNzI2MGQ3NjNiYjQyMmQ2MTRjZC5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBMaW5rIGZyb20gJ25leHQvbGluayc7XG5pbXBvcnQgUm91dGVyIGZyb20gJ25leHQvcm91dGVyJztcbmltcG9ydCB7IGxvZ2luIH0gZnJvbSAnLi4vLi4vLi4vc3RvcmUvYXV0aC9hY3Rpb24nO1xuaW1wb3J0IFdlYjMgZnJvbSAnd2ViMyc7XG5pbXBvcnQgTGF6eUxvYWQgZnJvbSAncmVhY3QtbGF6eWxvYWQnO1xuXG5pbXBvcnQgeyBGb3JtLCBJbnB1dCwgbm90aWZpY2F0aW9uIH0gZnJvbSAnYW50ZCc7XG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xuXG5jbGFzcyBMb2dpbiBleHRlbmRzIENvbXBvbmVudCB7XG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICAgICAgc3VwZXIocHJvcHMpO1xuICAgICAgICB0aGlzLnN0YXRlID0ge307XG4gICAgfVxuXG4gICAgc3RhdGljIGdldERlcml2ZWRTdGF0ZUZyb21Qcm9wcyhwcm9wcykge1xuICAgICAgICBpZiAocHJvcHMuaXNMb2dnZWRJbiA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgUm91dGVyLnB1c2goJy8nKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgaGFuZGxlRmVhdHVyZVdpbGxVcGRhdGUoZSkge1xuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIG5vdGlmaWNhdGlvbi5vcGVuKHtcbiAgICAgICAgICAgIG1lc3NhZ2U6ICdPcHAhIFNvbWV0aGluZyB3ZW50IHdyb25nLicsXG4gICAgICAgICAgICBkZXNjcmlwdGlvbjogJ1RoaXMgZmVhdHVyZSBoYXMgYmVlbiB1cGRhdGVkIGxhdGVyIScsXG4gICAgICAgICAgICBkdXJhdGlvbjogNTAwLFxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBoYW5kbGVMb2dpblN1Ym1pdCA9IChlKSA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKCd0ZXN0Jyk7XG4gICAgICAgIHRoaXMubG9hZFdlYjMoKTtcbiAgICAgICAgLy8gdGhpcy5wcm9wcy5kaXNwYXRjaChsb2dpbigpKTtcbiAgICAgICAgLy8gUm91dGVyLnB1c2goJy8nKTtcbiAgICB9O1xuICAgIGxvYWRXZWIzID0gYXN5bmMgKCkgPT4ge1xuICAgICAgICBsZXQgaXNDb25uZWN0ZWQgPSBmYWxzZTtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGlmICh3aW5kb3cuZXRoZXJldW0pIHtcbiAgICAgICAgICAgICAgICB3aW5kb3cud2ViMyA9IG5ldyBXZWIzKHdpbmRvdy5ldGhlcmV1bSk7XG4gICAgICAgICAgICAgICAgYXdhaXQgd2luZG93LmV0aGVyZXVtLmVuYWJsZSgpO1xuICAgICAgICAgICAgICAgIGlzQ29ubmVjdGVkID0gdHJ1ZTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAod2luZG93LndlYjMpIHtcbiAgICAgICAgICAgICAgICB3aW5kb3cud2ViMyA9IG5ldyBXZWIzKHdpbmRvdy53ZWIzLmN1cnJlbnRQcm92aWRlcik7XG4gICAgICAgICAgICAgICAgaXNDb25uZWN0ZWQgPSB0cnVlO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBpc0Nvbm5lY3RlZCA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIG5vdGlmaWNhdGlvbi5vcGVuKHtcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogJ01ldGFtYXNrIGlzIG5vdCBpbnN0YWxsZWQnLFxuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjpcbiAgICAgICAgICAgICAgICAgICAgICAgICcgcGxlYXNlIGluc3RhbGwgaXQgb24geW91ciBicm93c2VyIHRvIGNvbm5lY3QuJyxcbiAgICAgICAgICAgICAgICAgICAgZHVyYXRpb246IDUwMCxcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAvLyBcIk1ldGFtYXNrIGlzIG5vdCBpbnN0YWxsZWQsIHBsZWFzZSBpbnN0YWxsIGl0IG9uIHlvdXIgYnJvd3NlciB0byBjb25uZWN0LlwiLFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGlzQ29ubmVjdGVkID09PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgY29uc3Qgd2ViMyA9IHdpbmRvdy53ZWIzO1xuICAgICAgICAgICAgICAgIGxldCBhY2NvdW50cyA9IGF3YWl0IHdlYjMuZXRoLmdldEFjY291bnRzKCk7XG4gICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5kaXNwYXRjaChsb2dpbihhY2NvdW50c1swXSkpO1xuICAgICAgICAgICAgICAgIFJvdXRlci5wdXNoKCcvJyk7XG5cbiAgICAgICAgICAgICAgICB3aW5kb3cuZXRoZXJldW0ub24oJ2FjY291bnRzQ2hhbmdlZCcsIGZ1bmN0aW9uIChhY2NvdW50cykge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLmRpc3BhdGNoKGxvZ2luKGFjY291bnRzWzBdKSk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgICAgIH1cbiAgICB9O1xuICAgIHJlbmRlcigpIHtcbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHMtbXktYWNjb3VudFwiPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyXCI+XG4gICAgICAgICAgICAgICAgICAgIDxGb3JtXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJwcy1mb3JtLS1hY2NvdW50XCJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uRmluaXNoPXt0aGlzLmhhbmRsZUxvZ2luU3VibWl0LmJpbmQodGhpcyl9PlxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcy10YWIgYWN0aXZlXCIgaWQ9XCJzaWduLWluXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcy1mb3JtX19jb250ZW50XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxoNT5Db25uZWN0IHRvIHlvdXIgd2FsbGV0PC9oNT5cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZvcm0tZ3JvdXAgc3VibWl0XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cInN1Ym1pdFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwicHMtYnRuIHBzLWJ0bi0tZnVsbHdpZHRoXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17eyBwYWRkaW5nOiAnMTVweCAwcHgnIH19PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYXp5TG9hZD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3JjPVwiL3N0YXRpYy9pbWcvbWV0YS1tYXNrLnBuZ1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbHQ9XCJtYXJ0ZnVyeVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17eyBwYWRkaW5nUmlnaHQ6ICcxNXB4JyB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvTGF6eUxvYWQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgQ29ubmVjdCBtZXRhbWFza1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9idXR0b24+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHMtZm9ybV9fZm9vdGVyXCI+PC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPC9Gb3JtPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICk7XG4gICAgfVxufVxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gKHN0YXRlKSA9PiB7XG4gICAgcmV0dXJuIHN0YXRlLmF1dGg7XG59O1xuZXhwb3J0IGRlZmF1bHQgY29ubmVjdChtYXBTdGF0ZVRvUHJvcHMpKExvZ2luKTtcbiJdLCJzb3VyY2VSb290IjoiIn0=