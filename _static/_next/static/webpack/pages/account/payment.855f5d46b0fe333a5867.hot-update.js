webpackHotUpdate_N_E("pages/account/payment",{

/***/ "./components/partials/account/Payment.jsx":
/*!*************************************************!*\
  !*** ./components/partials/account/Payment.jsx ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _components_partials_account_modules_ModulePaymentOrderSummary__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ~/components/partials/account/modules/ModulePaymentOrderSummary */ "./components/partials/account/modules/ModulePaymentOrderSummary.js");
/* harmony import */ var _utilities_data__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../utilities/data */ "./utilities/data.js");








var _jsxFileName = "F:\\template\\New folder\\web3-shopping\\components\\partials\\account\\Payment.jsx";

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = Object(F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = Object(F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return Object(F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }







var Option = antd__WEBPACK_IMPORTED_MODULE_11__["Select"].Option;

var Payment = /*#__PURE__*/function (_Component) {
  Object(F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(Payment, _Component);

  var _super = _createSuper(Payment);

  function Payment(props) {
    var _this;

    Object(F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, Payment);

    _this = _super.call(this, props);

    Object(F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(Object(F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3__["default"])(_this), "handleChangePaymentMethod", function (e) {
      _this.setState({
        method: e.target.value
      });
    });

    _this.state = {
      method: 2
    };
    return _this;
  }

  Object(F_template_New_folder_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(Payment, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var month = [],
          year = [];

      for (var i = 1; i <= 12; i++) {
        month.push(i);
      }

      for (var _i = 2019; _i <= 2050; _i++) {
        year.push(_i);
      }

      return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        className: "ps-checkout ps-section--shopping",
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          className: "container",
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
            className: "ps-section__header",
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h1", {
              children: "Payment"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 34,
              columnNumber: 25
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 33,
            columnNumber: 21
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
            className: "ps-section__content",
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              className: "row",
              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                className: "col-xl-8 col-lg-8 col-md-12 col-sm-12",
                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                  className: "ps-block--shipping",
                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                    className: "ps-block__panel",
                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("figure", {
                      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("small", {
                        children: "Contact"
                      }, void 0, false, {
                        fileName: _jsxFileName,
                        lineNumber: 42,
                        columnNumber: 45
                      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
                        children: "test@gmail.com"
                      }, void 0, false, {
                        fileName: _jsxFileName,
                        lineNumber: 43,
                        columnNumber: 45
                      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_link__WEBPACK_IMPORTED_MODULE_10___default.a, {
                        href: "/account/checkout",
                        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                          children: "Change"
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 45,
                          columnNumber: 49
                        }, this)
                      }, void 0, false, {
                        fileName: _jsxFileName,
                        lineNumber: 44,
                        columnNumber: 45
                      }, this)]
                    }, void 0, true, {
                      fileName: _jsxFileName,
                      lineNumber: 41,
                      columnNumber: 41
                    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("figure", {
                      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("small", {
                        children: "Ship to"
                      }, void 0, false, {
                        fileName: _jsxFileName,
                        lineNumber: 49,
                        columnNumber: 45
                      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
                        children: "2015 South Street, Midland, Texas"
                      }, void 0, false, {
                        fileName: _jsxFileName,
                        lineNumber: 50,
                        columnNumber: 45
                      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_link__WEBPACK_IMPORTED_MODULE_10___default.a, {
                        href: "/account/checkout",
                        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                          children: "Change"
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 55,
                          columnNumber: 49
                        }, this)
                      }, void 0, false, {
                        fileName: _jsxFileName,
                        lineNumber: 54,
                        columnNumber: 45
                      }, this)]
                    }, void 0, true, {
                      fileName: _jsxFileName,
                      lineNumber: 48,
                      columnNumber: 41
                    }, this)]
                  }, void 0, true, {
                    fileName: _jsxFileName,
                    lineNumber: 40,
                    columnNumber: 37
                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h4", {
                    children: "Shipping Method"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 59,
                    columnNumber: 37
                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                    className: "ps-block__panel",
                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("figure", {
                      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("small", {
                        children: "International Shipping"
                      }, void 0, false, {
                        fileName: _jsxFileName,
                        lineNumber: 62,
                        columnNumber: 45
                      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("strong", {
                        children: "$20.00"
                      }, void 0, false, {
                        fileName: _jsxFileName,
                        lineNumber: 65,
                        columnNumber: 45
                      }, this)]
                    }, void 0, true, {
                      fileName: _jsxFileName,
                      lineNumber: 61,
                      columnNumber: 41
                    }, this)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 60,
                    columnNumber: 37
                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h4", {
                    children: "Payment Methods"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 68,
                    columnNumber: 37
                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                    className: "ps-block--payment-method",
                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                      className: "ps-block__content",
                      children: this.state.method === 1 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                        className: "ps-block__tab",
                        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "form-group",
                          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
                            children: "Card Number"
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 88,
                            columnNumber: 57
                          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
                            type: "text",
                            className: "form-control"
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 91,
                            columnNumber: 57
                          }, this)]
                        }, void 0, true, {
                          fileName: _jsxFileName,
                          lineNumber: 87,
                          columnNumber: 53
                        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "form-group",
                          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
                            children: "Card Holders"
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 97,
                            columnNumber: 57
                          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
                            type: "text",
                            className: "form-control"
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 100,
                            columnNumber: 57
                          }, this)]
                        }, void 0, true, {
                          fileName: _jsxFileName,
                          lineNumber: 96,
                          columnNumber: 53
                        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "row",
                          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "col-8",
                            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "form-group",
                              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
                                children: "Expiration Date"
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 108,
                                columnNumber: 65
                              }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "row",
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "col-6",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_11__["Select"], {
                                    defaultValue: 1,
                                    children: month.map(function (item) {
                                      return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(Option, {
                                        value: item,
                                        children: item
                                      }, item, false, {
                                        fileName: _jsxFileName,
                                        lineNumber: 122,
                                        columnNumber: 85
                                      }, _this2);
                                    })
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 114,
                                    columnNumber: 73
                                  }, this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 113,
                                  columnNumber: 69
                                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "col-6",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_11__["Select"], {
                                    defaultValue: 2020,
                                    children: year.map(function (item) {
                                      return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(Option, {
                                        value: item,
                                        children: item
                                      }, item, false, {
                                        fileName: _jsxFileName,
                                        lineNumber: 146,
                                        columnNumber: 85
                                      }, _this2);
                                    })
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 138,
                                    columnNumber: 73
                                  }, this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 137,
                                  columnNumber: 69
                                }, this)]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 112,
                                columnNumber: 65
                              }, this)]
                            }, void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 107,
                              columnNumber: 61
                            }, this)
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 106,
                            columnNumber: 57
                          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "col-4",
                            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "form-group",
                              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
                                children: "CVV"
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 166,
                                columnNumber: 65
                              }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
                                type: "text",
                                className: "form-control"
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 169,
                                columnNumber: 65
                              }, this)]
                            }, void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 165,
                              columnNumber: 61
                            }, this)
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 164,
                            columnNumber: 57
                          }, this)]
                        }, void 0, true, {
                          fileName: _jsxFileName,
                          lineNumber: 105,
                          columnNumber: 53
                        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "form-group",
                          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("button", {
                            className: "ps-btn ps-btn--fullwidth",
                            children: "Submit"
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 177,
                            columnNumber: 57
                          }, this)
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 176,
                          columnNumber: 53
                        }, this)]
                      }, void 0, true, {
                        fileName: _jsxFileName,
                        lineNumber: 86,
                        columnNumber: 49
                      }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                        className: "ps-block__tab",
                        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                          className: "ps-btn",
                          children: "Pay Now"
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 184,
                          columnNumber: 53
                        }, this)
                      }, void 0, false, {
                        fileName: _jsxFileName,
                        lineNumber: 183,
                        columnNumber: 49
                      }, this)
                    }, void 0, false, {
                      fileName: _jsxFileName,
                      lineNumber: 84,
                      columnNumber: 41
                    }, this)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 69,
                    columnNumber: 37
                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                    className: "ps-block__footer",
                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_link__WEBPACK_IMPORTED_MODULE_10___default.a, {
                      href: "/account/shipping",
                      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                          className: "icon-arrow-left mr-2"
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 194,
                          columnNumber: 49
                        }, this), "Return to shipping"]
                      }, void 0, true, {
                        fileName: _jsxFileName,
                        lineNumber: 193,
                        columnNumber: 45
                      }, this)
                    }, void 0, false, {
                      fileName: _jsxFileName,
                      lineNumber: 192,
                      columnNumber: 41
                    }, this)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 191,
                    columnNumber: 37
                  }, this)]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 39,
                  columnNumber: 33
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 38,
                columnNumber: 29
              }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                className: "col-xl-4 col-lg-4 col-md-12 col-sm-12 ",
                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                  className: "ps-form__orders",
                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_partials_account_modules_ModulePaymentOrderSummary__WEBPACK_IMPORTED_MODULE_12__["default"], {}, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 203,
                    columnNumber: 37
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 202,
                  columnNumber: 33
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 201,
                columnNumber: 29
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 37,
              columnNumber: 25
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 36,
            columnNumber: 21
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 32,
          columnNumber: 17
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 31,
        columnNumber: 13
      }, this);
    }
  }]);

  return Payment;
}(react__WEBPACK_IMPORTED_MODULE_8__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_9__["connect"])()(Payment));

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ }),

/***/ "./utilities/data.js":
/*!***************************!*\
  !*** ./utilities/data.js ***!
  \***************************/
/*! exports provided: contractAddress, contractAbi */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "contractAddress", function() { return contractAddress; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "contractAbi", function() { return contractAbi; });
var contractAddress = '0xc51C8935b9662eBA3730Dac28d9e27Eb9e5e2eA7'; // Deployed manually

var contractAbi = [{
  inputs: [],
  name: 'destroy',
  outputs: [],
  stateMutability: 'nonpayable',
  type: 'function'
}, {
  inputs: [{
    internalType: 'uint256',
    name: '_amount',
    type: 'uint256'
  }],
  name: 'drainEth',
  outputs: [],
  stateMutability: 'nonpayable',
  type: 'function'
}, {
  inputs: [],
  name: 'stakeEth',
  outputs: [],
  stateMutability: 'payable',
  type: 'function'
}, {
  inputs: [{
    internalType: 'contract ZinFinance',
    name: '_token',
    type: 'address'
  }, {
    internalType: 'address payable',
    name: '_admin',
    type: 'address'
  }],
  stateMutability: 'nonpayable',
  type: 'constructor'
}, {
  inputs: [],
  name: 'unstake',
  outputs: [],
  stateMutability: 'nonpayable',
  type: 'function'
}, {
  inputs: [],
  name: 'withdrawReward',
  outputs: [],
  stateMutability: 'nonpayable',
  type: 'function'
}, {
  inputs: [{
    internalType: 'address',
    name: '_stakeholder',
    type: 'address'
  }],
  name: 'calculateDividend',
  outputs: [{
    internalType: 'uint256',
    name: '',
    type: 'uint256'
  }],
  stateMutability: 'view',
  type: 'function'
}, {
  inputs: [{
    internalType: 'address',
    name: '',
    type: 'address'
  }],
  name: 'deposit_time',
  outputs: [{
    internalType: 'uint256',
    name: '',
    type: 'uint256'
  }],
  stateMutability: 'view',
  type: 'function'
}, {
  inputs: [{
    internalType: 'address',
    name: '_address',
    type: 'address'
  }],
  name: 'isStakeholder',
  outputs: [{
    internalType: 'bool',
    name: '',
    type: 'bool'
  }, {
    internalType: 'uint256',
    name: '',
    type: 'uint256'
  }],
  stateMutability: 'view',
  type: 'function'
}, {
  inputs: [{
    internalType: 'uint256',
    name: '_amount',
    type: 'uint256'
  }],
  name: 'maxPayoutOf',
  outputs: [{
    internalType: 'uint256',
    name: '',
    type: 'uint256'
  }],
  stateMutability: 'pure',
  type: 'function'
}, {
  inputs: [{
    internalType: 'address',
    name: '_addr',
    type: 'address'
  }],
  name: 'rewardOfEachUser',
  outputs: [{
    internalType: 'uint256',
    name: 'payout',
    type: 'uint256'
  }, {
    internalType: 'uint256',
    name: 'max_payout',
    type: 'uint256'
  }],
  stateMutability: 'view',
  type: 'function'
}, {
  inputs: [{
    internalType: 'uint256',
    name: '',
    type: 'uint256'
  }],
  name: 'stakeholders',
  outputs: [{
    internalType: 'address',
    name: '',
    type: 'address'
  }],
  stateMutability: 'view',
  type: 'function'
}, {
  inputs: [],
  name: 'token',
  outputs: [{
    internalType: 'contract ZinFinance',
    name: '',
    type: 'address'
  }],
  stateMutability: 'view',
  type: 'function'
}, {
  inputs: [],
  name: 'totalStakes',
  outputs: [{
    internalType: 'uint256',
    name: '',
    type: 'uint256'
  }],
  stateMutability: 'view',
  type: 'function'
}, {
  inputs: [{
    internalType: 'address',
    name: '',
    type: 'address'
  }],
  name: 'userData',
  outputs: [{
    internalType: 'uint256',
    name: 'stakes',
    type: 'uint256'
  }, {
    internalType: 'uint256',
    name: 'deposit_time',
    type: 'uint256'
  }, {
    internalType: 'uint256',
    name: 'deposit_payouts',
    type: 'uint256'
  }],
  stateMutability: 'view',
  type: 'function'
}];

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9wYXJ0aWFscy9hY2NvdW50L1BheW1lbnQuanN4Iiwid2VicGFjazovL19OX0UvLi91dGlsaXRpZXMvZGF0YS5qcyJdLCJuYW1lcyI6WyJPcHRpb24iLCJTZWxlY3QiLCJQYXltZW50IiwicHJvcHMiLCJlIiwic2V0U3RhdGUiLCJtZXRob2QiLCJ0YXJnZXQiLCJ2YWx1ZSIsInN0YXRlIiwibW9udGgiLCJ5ZWFyIiwiaSIsInB1c2giLCJtYXAiLCJpdGVtIiwiQ29tcG9uZW50IiwiY29ubmVjdCIsImNvbnRyYWN0QWRkcmVzcyIsImNvbnRyYWN0QWJpIiwiaW5wdXRzIiwibmFtZSIsIm91dHB1dHMiLCJzdGF0ZU11dGFiaWxpdHkiLCJ0eXBlIiwiaW50ZXJuYWxUeXBlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7SUFDUUEsTSxHQUFXQyw0QyxDQUFYRCxNOztJQUVGRSxPOzs7OztBQUNGLG1CQUFZQyxLQUFaLEVBQW1CO0FBQUE7O0FBQUE7O0FBQ2YsOEJBQU1BLEtBQU47O0FBRGUsd1dBT1MsVUFBQ0MsQ0FBRCxFQUFPO0FBQy9CLFlBQUtDLFFBQUwsQ0FBYztBQUFFQyxjQUFNLEVBQUVGLENBQUMsQ0FBQ0csTUFBRixDQUFTQztBQUFuQixPQUFkO0FBQ0gsS0FUa0I7O0FBRWYsVUFBS0MsS0FBTCxHQUFhO0FBQ1RILFlBQU0sRUFBRTtBQURDLEtBQWI7QUFGZTtBQUtsQjs7Ozs2QkFNUTtBQUFBOztBQUNMLFVBQUlJLEtBQUssR0FBRyxFQUFaO0FBQUEsVUFDSUMsSUFBSSxHQUFHLEVBRFg7O0FBRUEsV0FBSyxJQUFJQyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxJQUFJLEVBQXJCLEVBQXlCQSxDQUFDLEVBQTFCLEVBQThCO0FBQzFCRixhQUFLLENBQUNHLElBQU4sQ0FBV0QsQ0FBWDtBQUNIOztBQUNELFdBQUssSUFBSUEsRUFBQyxHQUFHLElBQWIsRUFBbUJBLEVBQUMsSUFBSSxJQUF4QixFQUE4QkEsRUFBQyxFQUEvQixFQUFtQztBQUMvQkQsWUFBSSxDQUFDRSxJQUFMLENBQVVELEVBQVY7QUFDSDs7QUFDRCwwQkFDSTtBQUFLLGlCQUFTLEVBQUMsa0NBQWY7QUFBQSwrQkFDSTtBQUFLLG1CQUFTLEVBQUMsV0FBZjtBQUFBLGtDQUNJO0FBQUsscUJBQVMsRUFBQyxvQkFBZjtBQUFBLG1DQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFESixlQUlJO0FBQUsscUJBQVMsRUFBQyxxQkFBZjtBQUFBLG1DQUNJO0FBQUssdUJBQVMsRUFBQyxLQUFmO0FBQUEsc0NBQ0k7QUFBSyx5QkFBUyxFQUFDLHVDQUFmO0FBQUEsdUNBQ0k7QUFBSywyQkFBUyxFQUFDLG9CQUFmO0FBQUEsMENBQ0k7QUFBSyw2QkFBUyxFQUFDLGlCQUFmO0FBQUEsNENBQ0k7QUFBQSw4Q0FDSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSw4QkFESixlQUVJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDhCQUZKLGVBR0kscUVBQUMsaURBQUQ7QUFBTSw0QkFBSSxFQUFDLG1CQUFYO0FBQUEsK0NBQ0k7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLDhCQUhKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSw0QkFESixlQVFJO0FBQUEsOENBQ0k7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsOEJBREosZUFFSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSw4QkFGSixlQU1JLHFFQUFDLGlEQUFEO0FBQU0sNEJBQUksRUFBQyxtQkFBWDtBQUFBLCtDQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSw4QkFOSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsNEJBUko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDBCQURKLGVBb0JJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDBCQXBCSixlQXFCSTtBQUFLLDZCQUFTLEVBQUMsaUJBQWY7QUFBQSwyQ0FDSTtBQUFBLDhDQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDhCQURKLGVBSUk7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsOEJBSko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQkFyQkosZUE2Qkk7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMEJBN0JKLGVBOEJJO0FBQUssNkJBQVMsRUFBQywwQkFBZjtBQUFBLDJDQWVJO0FBQUssK0JBQVMsRUFBQyxtQkFBZjtBQUFBLGdDQUNLLEtBQUtILEtBQUwsQ0FBV0gsTUFBWCxLQUFzQixDQUF0QixnQkFDRztBQUFLLGlDQUFTLEVBQUMsZUFBZjtBQUFBLGdEQUNJO0FBQUssbUNBQVMsRUFBQyxZQUFmO0FBQUEsa0RBQ0k7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0NBREosZUFJSTtBQUNJLGdDQUFJLEVBQUMsTUFEVDtBQUVJLHFDQUFTLEVBQUM7QUFGZDtBQUFBO0FBQUE7QUFBQTtBQUFBLGtDQUpKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQ0FESixlQVVJO0FBQUssbUNBQVMsRUFBQyxZQUFmO0FBQUEsa0RBQ0k7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0NBREosZUFJSTtBQUNJLGdDQUFJLEVBQUMsTUFEVDtBQUVJLHFDQUFTLEVBQUM7QUFGZDtBQUFBO0FBQUE7QUFBQTtBQUFBLGtDQUpKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQ0FWSixlQW1CSTtBQUFLLG1DQUFTLEVBQUMsS0FBZjtBQUFBLGtEQUNJO0FBQUsscUNBQVMsRUFBQyxPQUFmO0FBQUEsbURBQ0k7QUFBSyx1Q0FBUyxFQUFDLFlBQWY7QUFBQSxzREFDSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQ0FESixlQUtJO0FBQUsseUNBQVMsRUFBQyxLQUFmO0FBQUEsd0RBQ0k7QUFBSywyQ0FBUyxFQUFDLE9BQWY7QUFBQSx5REFDSSxxRUFBQyw0Q0FBRDtBQUNJLGdEQUFZLEVBQ1IsQ0FGUjtBQUFBLDhDQUlLSSxLQUFLLENBQUNJLEdBQU4sQ0FDRyxVQUNJQyxJQURKO0FBQUEsMERBR0kscUVBQUMsTUFBRDtBQUNJLDZDQUFLLEVBQ0RBLElBRlI7QUFBQSxrREFRUUE7QUFSUix5Q0FLUUEsSUFMUjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdEQUhKO0FBQUEscUNBREg7QUFKTDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FESixlQXlCSTtBQUFLLDJDQUFTLEVBQUMsT0FBZjtBQUFBLHlEQUNJLHFFQUFDLDRDQUFEO0FBQ0ksZ0RBQVksRUFDUixJQUZSO0FBQUEsOENBSUtKLElBQUksQ0FBQ0csR0FBTCxDQUNHLFVBQ0lDLElBREo7QUFBQSwwREFHSSxxRUFBQyxNQUFEO0FBQ0ksNkNBQUssRUFDREEsSUFGUjtBQUFBLGtEQVFRQTtBQVJSLHlDQUtRQSxJQUxSO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0RBSEo7QUFBQSxxQ0FESDtBQUpMO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQXpCSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0NBTEo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQ0FESixlQTJESTtBQUFLLHFDQUFTLEVBQUMsT0FBZjtBQUFBLG1EQUNJO0FBQUssdUNBQVMsRUFBQyxZQUFmO0FBQUEsc0RBQ0k7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0NBREosZUFJSTtBQUNJLG9DQUFJLEVBQUMsTUFEVDtBQUVJLHlDQUFTLEVBQUM7QUFGZDtBQUFBO0FBQUE7QUFBQTtBQUFBLHNDQUpKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0NBM0RKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQ0FuQkosZUEwRkk7QUFBSyxtQ0FBUyxFQUFDLFlBQWY7QUFBQSxpREFDSTtBQUFRLHFDQUFTLEVBQUMsMEJBQWxCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQ0ExRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDhCQURILGdCQWtHRztBQUFLLGlDQUFTLEVBQUMsZUFBZjtBQUFBLCtDQUNJO0FBQUcsbUNBQVMsRUFBQyxRQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQW5HUjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZko7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQkE5QkosZUF3Skk7QUFBSyw2QkFBUyxFQUFDLGtCQUFmO0FBQUEsMkNBQ0kscUVBQUMsaURBQUQ7QUFBTSwwQkFBSSxFQUFDLG1CQUFYO0FBQUEsNkNBQ0k7QUFBQSxnREFDSTtBQUFHLG1DQUFTLEVBQUM7QUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdDQURKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLDBCQXhKSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQURKLGVBb0tJO0FBQUsseUJBQVMsRUFBQyx3Q0FBZjtBQUFBLHVDQUNJO0FBQUssMkJBQVMsRUFBQyxpQkFBZjtBQUFBLHlDQUNJLHFFQUFDLHVHQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFwS0o7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFKSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREo7QUFxTEg7Ozs7RUExTWlCQywrQzs7QUE2TVBDLDBIQUFPLEdBQUdmLE9BQUgsQ0FBdEI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3JOQTtBQUFBO0FBQUE7QUFBTyxJQUFNZ0IsZUFBZSxHQUFHLDRDQUF4QixDLENBQXNFOztBQUN0RSxJQUFNQyxXQUFXLEdBQUcsQ0FDdkI7QUFDSUMsUUFBTSxFQUFFLEVBRFo7QUFFSUMsTUFBSSxFQUFFLFNBRlY7QUFHSUMsU0FBTyxFQUFFLEVBSGI7QUFJSUMsaUJBQWUsRUFBRSxZQUpyQjtBQUtJQyxNQUFJLEVBQUU7QUFMVixDQUR1QixFQVF2QjtBQUNJSixRQUFNLEVBQUUsQ0FDSjtBQUNJSyxnQkFBWSxFQUFFLFNBRGxCO0FBRUlKLFFBQUksRUFBRSxTQUZWO0FBR0lHLFFBQUksRUFBRTtBQUhWLEdBREksQ0FEWjtBQVFJSCxNQUFJLEVBQUUsVUFSVjtBQVNJQyxTQUFPLEVBQUUsRUFUYjtBQVVJQyxpQkFBZSxFQUFFLFlBVnJCO0FBV0lDLE1BQUksRUFBRTtBQVhWLENBUnVCLEVBcUJ2QjtBQUNJSixRQUFNLEVBQUUsRUFEWjtBQUVJQyxNQUFJLEVBQUUsVUFGVjtBQUdJQyxTQUFPLEVBQUUsRUFIYjtBQUlJQyxpQkFBZSxFQUFFLFNBSnJCO0FBS0lDLE1BQUksRUFBRTtBQUxWLENBckJ1QixFQTRCdkI7QUFDSUosUUFBTSxFQUFFLENBQ0o7QUFDSUssZ0JBQVksRUFBRSxxQkFEbEI7QUFFSUosUUFBSSxFQUFFLFFBRlY7QUFHSUcsUUFBSSxFQUFFO0FBSFYsR0FESSxFQU1KO0FBQ0lDLGdCQUFZLEVBQUUsaUJBRGxCO0FBRUlKLFFBQUksRUFBRSxRQUZWO0FBR0lHLFFBQUksRUFBRTtBQUhWLEdBTkksQ0FEWjtBQWFJRCxpQkFBZSxFQUFFLFlBYnJCO0FBY0lDLE1BQUksRUFBRTtBQWRWLENBNUJ1QixFQTRDdkI7QUFDSUosUUFBTSxFQUFFLEVBRFo7QUFFSUMsTUFBSSxFQUFFLFNBRlY7QUFHSUMsU0FBTyxFQUFFLEVBSGI7QUFJSUMsaUJBQWUsRUFBRSxZQUpyQjtBQUtJQyxNQUFJLEVBQUU7QUFMVixDQTVDdUIsRUFtRHZCO0FBQ0lKLFFBQU0sRUFBRSxFQURaO0FBRUlDLE1BQUksRUFBRSxnQkFGVjtBQUdJQyxTQUFPLEVBQUUsRUFIYjtBQUlJQyxpQkFBZSxFQUFFLFlBSnJCO0FBS0lDLE1BQUksRUFBRTtBQUxWLENBbkR1QixFQTBEdkI7QUFDSUosUUFBTSxFQUFFLENBQ0o7QUFDSUssZ0JBQVksRUFBRSxTQURsQjtBQUVJSixRQUFJLEVBQUUsY0FGVjtBQUdJRyxRQUFJLEVBQUU7QUFIVixHQURJLENBRFo7QUFRSUgsTUFBSSxFQUFFLG1CQVJWO0FBU0lDLFNBQU8sRUFBRSxDQUNMO0FBQ0lHLGdCQUFZLEVBQUUsU0FEbEI7QUFFSUosUUFBSSxFQUFFLEVBRlY7QUFHSUcsUUFBSSxFQUFFO0FBSFYsR0FESyxDQVRiO0FBZ0JJRCxpQkFBZSxFQUFFLE1BaEJyQjtBQWlCSUMsTUFBSSxFQUFFO0FBakJWLENBMUR1QixFQTZFdkI7QUFDSUosUUFBTSxFQUFFLENBQ0o7QUFDSUssZ0JBQVksRUFBRSxTQURsQjtBQUVJSixRQUFJLEVBQUUsRUFGVjtBQUdJRyxRQUFJLEVBQUU7QUFIVixHQURJLENBRFo7QUFRSUgsTUFBSSxFQUFFLGNBUlY7QUFTSUMsU0FBTyxFQUFFLENBQ0w7QUFDSUcsZ0JBQVksRUFBRSxTQURsQjtBQUVJSixRQUFJLEVBQUUsRUFGVjtBQUdJRyxRQUFJLEVBQUU7QUFIVixHQURLLENBVGI7QUFnQklELGlCQUFlLEVBQUUsTUFoQnJCO0FBaUJJQyxNQUFJLEVBQUU7QUFqQlYsQ0E3RXVCLEVBZ0d2QjtBQUNJSixRQUFNLEVBQUUsQ0FDSjtBQUNJSyxnQkFBWSxFQUFFLFNBRGxCO0FBRUlKLFFBQUksRUFBRSxVQUZWO0FBR0lHLFFBQUksRUFBRTtBQUhWLEdBREksQ0FEWjtBQVFJSCxNQUFJLEVBQUUsZUFSVjtBQVNJQyxTQUFPLEVBQUUsQ0FDTDtBQUNJRyxnQkFBWSxFQUFFLE1BRGxCO0FBRUlKLFFBQUksRUFBRSxFQUZWO0FBR0lHLFFBQUksRUFBRTtBQUhWLEdBREssRUFNTDtBQUNJQyxnQkFBWSxFQUFFLFNBRGxCO0FBRUlKLFFBQUksRUFBRSxFQUZWO0FBR0lHLFFBQUksRUFBRTtBQUhWLEdBTkssQ0FUYjtBQXFCSUQsaUJBQWUsRUFBRSxNQXJCckI7QUFzQklDLE1BQUksRUFBRTtBQXRCVixDQWhHdUIsRUF3SHZCO0FBQ0lKLFFBQU0sRUFBRSxDQUNKO0FBQ0lLLGdCQUFZLEVBQUUsU0FEbEI7QUFFSUosUUFBSSxFQUFFLFNBRlY7QUFHSUcsUUFBSSxFQUFFO0FBSFYsR0FESSxDQURaO0FBUUlILE1BQUksRUFBRSxhQVJWO0FBU0lDLFNBQU8sRUFBRSxDQUNMO0FBQ0lHLGdCQUFZLEVBQUUsU0FEbEI7QUFFSUosUUFBSSxFQUFFLEVBRlY7QUFHSUcsUUFBSSxFQUFFO0FBSFYsR0FESyxDQVRiO0FBZ0JJRCxpQkFBZSxFQUFFLE1BaEJyQjtBQWlCSUMsTUFBSSxFQUFFO0FBakJWLENBeEh1QixFQTJJdkI7QUFDSUosUUFBTSxFQUFFLENBQ0o7QUFDSUssZ0JBQVksRUFBRSxTQURsQjtBQUVJSixRQUFJLEVBQUUsT0FGVjtBQUdJRyxRQUFJLEVBQUU7QUFIVixHQURJLENBRFo7QUFRSUgsTUFBSSxFQUFFLGtCQVJWO0FBU0lDLFNBQU8sRUFBRSxDQUNMO0FBQ0lHLGdCQUFZLEVBQUUsU0FEbEI7QUFFSUosUUFBSSxFQUFFLFFBRlY7QUFHSUcsUUFBSSxFQUFFO0FBSFYsR0FESyxFQU1MO0FBQ0lDLGdCQUFZLEVBQUUsU0FEbEI7QUFFSUosUUFBSSxFQUFFLFlBRlY7QUFHSUcsUUFBSSxFQUFFO0FBSFYsR0FOSyxDQVRiO0FBcUJJRCxpQkFBZSxFQUFFLE1BckJyQjtBQXNCSUMsTUFBSSxFQUFFO0FBdEJWLENBM0l1QixFQW1LdkI7QUFDSUosUUFBTSxFQUFFLENBQ0o7QUFDSUssZ0JBQVksRUFBRSxTQURsQjtBQUVJSixRQUFJLEVBQUUsRUFGVjtBQUdJRyxRQUFJLEVBQUU7QUFIVixHQURJLENBRFo7QUFRSUgsTUFBSSxFQUFFLGNBUlY7QUFTSUMsU0FBTyxFQUFFLENBQ0w7QUFDSUcsZ0JBQVksRUFBRSxTQURsQjtBQUVJSixRQUFJLEVBQUUsRUFGVjtBQUdJRyxRQUFJLEVBQUU7QUFIVixHQURLLENBVGI7QUFnQklELGlCQUFlLEVBQUUsTUFoQnJCO0FBaUJJQyxNQUFJLEVBQUU7QUFqQlYsQ0FuS3VCLEVBc0x2QjtBQUNJSixRQUFNLEVBQUUsRUFEWjtBQUVJQyxNQUFJLEVBQUUsT0FGVjtBQUdJQyxTQUFPLEVBQUUsQ0FDTDtBQUNJRyxnQkFBWSxFQUFFLHFCQURsQjtBQUVJSixRQUFJLEVBQUUsRUFGVjtBQUdJRyxRQUFJLEVBQUU7QUFIVixHQURLLENBSGI7QUFVSUQsaUJBQWUsRUFBRSxNQVZyQjtBQVdJQyxNQUFJLEVBQUU7QUFYVixDQXRMdUIsRUFtTXZCO0FBQ0lKLFFBQU0sRUFBRSxFQURaO0FBRUlDLE1BQUksRUFBRSxhQUZWO0FBR0lDLFNBQU8sRUFBRSxDQUNMO0FBQ0lHLGdCQUFZLEVBQUUsU0FEbEI7QUFFSUosUUFBSSxFQUFFLEVBRlY7QUFHSUcsUUFBSSxFQUFFO0FBSFYsR0FESyxDQUhiO0FBVUlELGlCQUFlLEVBQUUsTUFWckI7QUFXSUMsTUFBSSxFQUFFO0FBWFYsQ0FuTXVCLEVBZ052QjtBQUNJSixRQUFNLEVBQUUsQ0FDSjtBQUNJSyxnQkFBWSxFQUFFLFNBRGxCO0FBRUlKLFFBQUksRUFBRSxFQUZWO0FBR0lHLFFBQUksRUFBRTtBQUhWLEdBREksQ0FEWjtBQVFJSCxNQUFJLEVBQUUsVUFSVjtBQVNJQyxTQUFPLEVBQUUsQ0FDTDtBQUNJRyxnQkFBWSxFQUFFLFNBRGxCO0FBRUlKLFFBQUksRUFBRSxRQUZWO0FBR0lHLFFBQUksRUFBRTtBQUhWLEdBREssRUFNTDtBQUNJQyxnQkFBWSxFQUFFLFNBRGxCO0FBRUlKLFFBQUksRUFBRSxjQUZWO0FBR0lHLFFBQUksRUFBRTtBQUhWLEdBTkssRUFXTDtBQUNJQyxnQkFBWSxFQUFFLFNBRGxCO0FBRUlKLFFBQUksRUFBRSxpQkFGVjtBQUdJRyxRQUFJLEVBQUU7QUFIVixHQVhLLENBVGI7QUEwQklELGlCQUFlLEVBQUUsTUExQnJCO0FBMkJJQyxNQUFJLEVBQUU7QUEzQlYsQ0FoTnVCLENBQXBCIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL2FjY291bnQvcGF5bWVudC44NTVmNWQ0NmIwZmUzMzNhNTg2Ny5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5pbXBvcnQgTGluayBmcm9tICduZXh0L2xpbmsnO1xuaW1wb3J0IHsgUmFkaW8sIFNlbGVjdCB9IGZyb20gJ2FudGQnO1xuaW1wb3J0IE1vZHVsZVBheW1lbnRPcmRlclN1bW1hcnkgZnJvbSAnfi9jb21wb25lbnRzL3BhcnRpYWxzL2FjY291bnQvbW9kdWxlcy9Nb2R1bGVQYXltZW50T3JkZXJTdW1tYXJ5JztcbmltcG9ydCB7IGNvbnRyYWN0QWJpLCBjb250cmFjdEFkZHJlc3MgfSBmcm9tICcuLi8uLi8uLi91dGlsaXRpZXMvZGF0YSc7XG5jb25zdCB7IE9wdGlvbiB9ID0gU2VsZWN0O1xuXG5jbGFzcyBQYXltZW50IGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgICAgICBzdXBlcihwcm9wcyk7XG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICAgICAgICBtZXRob2Q6IDIsXG4gICAgICAgIH07XG4gICAgfVxuXG4gICAgaGFuZGxlQ2hhbmdlUGF5bWVudE1ldGhvZCA9IChlKSA9PiB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyBtZXRob2Q6IGUudGFyZ2V0LnZhbHVlIH0pO1xuICAgIH07XG5cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIGxldCBtb250aCA9IFtdLFxuICAgICAgICAgICAgeWVhciA9IFtdO1xuICAgICAgICBmb3IgKGxldCBpID0gMTsgaSA8PSAxMjsgaSsrKSB7XG4gICAgICAgICAgICBtb250aC5wdXNoKGkpO1xuICAgICAgICB9XG4gICAgICAgIGZvciAobGV0IGkgPSAyMDE5OyBpIDw9IDIwNTA7IGkrKykge1xuICAgICAgICAgICAgeWVhci5wdXNoKGkpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInBzLWNoZWNrb3V0IHBzLXNlY3Rpb24tLXNob3BwaW5nXCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXJcIj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcy1zZWN0aW9uX19oZWFkZXJcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxoMT5QYXltZW50PC9oMT5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHMtc2VjdGlvbl9fY29udGVudFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3dcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC14bC04IGNvbC1sZy04IGNvbC1tZC0xMiBjb2wtc20tMTJcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcy1ibG9jay0tc2hpcHBpbmdcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHMtYmxvY2tfX3BhbmVsXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGZpZ3VyZT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNtYWxsPkNvbnRhY3Q8L3NtYWxsPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cD50ZXN0QGdtYWlsLmNvbTwvcD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPExpbmsgaHJlZj1cIi9hY2NvdW50L2NoZWNrb3V0XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YT5DaGFuZ2U8L2E+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2ZpZ3VyZT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZmlndXJlPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c21hbGw+U2hpcCB0bzwvc21hbGw+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgMjAxNSBTb3V0aCBTdHJlZXQsIE1pZGxhbmQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBUZXhhc1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3A+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMaW5rIGhyZWY9XCIvYWNjb3VudC9jaGVja291dFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGE+Q2hhbmdlPC9hPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9maWd1cmU+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxoND5TaGlwcGluZyBNZXRob2Q8L2g0PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcy1ibG9ja19fcGFuZWxcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZmlndXJlPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c21hbGw+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBJbnRlcm5hdGlvbmFsIFNoaXBwaW5nXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc21hbGw+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzdHJvbmc+JDIwLjAwPC9zdHJvbmc+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9maWd1cmU+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxoND5QYXltZW50IE1ldGhvZHM8L2g0PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcy1ibG9jay0tcGF5bWVudC1tZXRob2RcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7LyogPGRpdiBjbGFzc05hbWU9XCJwcy1ibG9ja19faGVhZGVyXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxSYWRpby5Hcm91cFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e2UgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmhhbmRsZUNoYW5nZVBheW1lbnRNZXRob2QoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17dGhpcy5zdGF0ZS5tZXRob2R9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFJhZGlvIHZhbHVlPXsxfT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBWaXNhIC8gTWFzdGVyIENhcmRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvUmFkaW8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8UmFkaW8gdmFsdWU9ezJ9PlBheXBhbDwvUmFkaW8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvUmFkaW8uR3JvdXA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+ICovfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHMtYmxvY2tfX2NvbnRlbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3RoaXMuc3RhdGUubWV0aG9kID09PSAxID8gKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcy1ibG9ja19fdGFiXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmb3JtLWdyb3VwXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIENhcmQgTnVtYmVyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGFiZWw+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cInRleHRcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZm9ybS1jb250cm9sXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZvcm0tZ3JvdXBcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgQ2FyZCBIb2xkZXJzXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGFiZWw+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cInRleHRcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZm9ybS1jb250cm9sXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC04XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZvcm0tZ3JvdXBcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWw+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIEV4cGlyYXRpb25cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgRGF0ZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGFiZWw+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3dcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wtNlwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFNlbGVjdFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHRWYWx1ZT17XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDFcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHttb250aC5tYXAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGl0ZW1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKSA9PiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8T3B0aW9uXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpdGVtXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtleT17XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGl0ZW1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaXRlbVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvT3B0aW9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvU2VsZWN0PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wtNlwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFNlbGVjdFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHRWYWx1ZT17XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDIwMjBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt5ZWFyLm1hcChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaXRlbVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApID0+IChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxPcHRpb25cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGl0ZW1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5PXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaXRlbVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpdGVtXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9PcHRpb24+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9TZWxlY3Q+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC00XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZvcm0tZ3JvdXBcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWw+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIENWVlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGFiZWw+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZm9ybS1jb250cm9sXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZm9ybS1ncm91cFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzTmFtZT1cInBzLWJ0biBwcy1idG4tLWZ1bGx3aWR0aFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgU3VibWl0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICkgOiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInBzLWJsb2NrX190YWJcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzc05hbWU9XCJwcy1idG5cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgUGF5IE5vd1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInBzLWJsb2NrX19mb290ZXJcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGluayBocmVmPVwiL2FjY291bnQvc2hpcHBpbmdcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGE+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJpY29uLWFycm93LWxlZnQgbXItMlwiPjwvaT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJldHVybiB0byBzaGlwcGluZ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLXhsLTQgY29sLWxnLTQgY29sLW1kLTEyIGNvbC1zbS0xMiBcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcy1mb3JtX19vcmRlcnNcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxNb2R1bGVQYXltZW50T3JkZXJTdW1tYXJ5IC8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICk7XG4gICAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KCkoUGF5bWVudCk7XG4iLCJleHBvcnQgY29uc3QgY29udHJhY3RBZGRyZXNzID0gJzB4YzUxQzg5MzViOTY2MmVCQTM3MzBEYWMyOGQ5ZTI3RWI5ZTVlMmVBNyc7IC8vIERlcGxveWVkIG1hbnVhbGx5XHJcbmV4cG9ydCBjb25zdCBjb250cmFjdEFiaSA9IFtcclxuICAgIHtcclxuICAgICAgICBpbnB1dHM6IFtdLFxyXG4gICAgICAgIG5hbWU6ICdkZXN0cm95JyxcclxuICAgICAgICBvdXRwdXRzOiBbXSxcclxuICAgICAgICBzdGF0ZU11dGFiaWxpdHk6ICdub25wYXlhYmxlJyxcclxuICAgICAgICB0eXBlOiAnZnVuY3Rpb24nLFxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICBpbnB1dHM6IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgaW50ZXJuYWxUeXBlOiAndWludDI1NicsXHJcbiAgICAgICAgICAgICAgICBuYW1lOiAnX2Ftb3VudCcsXHJcbiAgICAgICAgICAgICAgICB0eXBlOiAndWludDI1NicsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgXSxcclxuICAgICAgICBuYW1lOiAnZHJhaW5FdGgnLFxyXG4gICAgICAgIG91dHB1dHM6IFtdLFxyXG4gICAgICAgIHN0YXRlTXV0YWJpbGl0eTogJ25vbnBheWFibGUnLFxyXG4gICAgICAgIHR5cGU6ICdmdW5jdGlvbicsXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICAgIGlucHV0czogW10sXHJcbiAgICAgICAgbmFtZTogJ3N0YWtlRXRoJyxcclxuICAgICAgICBvdXRwdXRzOiBbXSxcclxuICAgICAgICBzdGF0ZU11dGFiaWxpdHk6ICdwYXlhYmxlJyxcclxuICAgICAgICB0eXBlOiAnZnVuY3Rpb24nLFxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICBpbnB1dHM6IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgaW50ZXJuYWxUeXBlOiAnY29udHJhY3QgWmluRmluYW5jZScsXHJcbiAgICAgICAgICAgICAgICBuYW1lOiAnX3Rva2VuJyxcclxuICAgICAgICAgICAgICAgIHR5cGU6ICdhZGRyZXNzJyxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgaW50ZXJuYWxUeXBlOiAnYWRkcmVzcyBwYXlhYmxlJyxcclxuICAgICAgICAgICAgICAgIG5hbWU6ICdfYWRtaW4nLFxyXG4gICAgICAgICAgICAgICAgdHlwZTogJ2FkZHJlc3MnLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIF0sXHJcbiAgICAgICAgc3RhdGVNdXRhYmlsaXR5OiAnbm9ucGF5YWJsZScsXHJcbiAgICAgICAgdHlwZTogJ2NvbnN0cnVjdG9yJyxcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgaW5wdXRzOiBbXSxcclxuICAgICAgICBuYW1lOiAndW5zdGFrZScsXHJcbiAgICAgICAgb3V0cHV0czogW10sXHJcbiAgICAgICAgc3RhdGVNdXRhYmlsaXR5OiAnbm9ucGF5YWJsZScsXHJcbiAgICAgICAgdHlwZTogJ2Z1bmN0aW9uJyxcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgaW5wdXRzOiBbXSxcclxuICAgICAgICBuYW1lOiAnd2l0aGRyYXdSZXdhcmQnLFxyXG4gICAgICAgIG91dHB1dHM6IFtdLFxyXG4gICAgICAgIHN0YXRlTXV0YWJpbGl0eTogJ25vbnBheWFibGUnLFxyXG4gICAgICAgIHR5cGU6ICdmdW5jdGlvbicsXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICAgIGlucHV0czogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBpbnRlcm5hbFR5cGU6ICdhZGRyZXNzJyxcclxuICAgICAgICAgICAgICAgIG5hbWU6ICdfc3Rha2Vob2xkZXInLFxyXG4gICAgICAgICAgICAgICAgdHlwZTogJ2FkZHJlc3MnLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIF0sXHJcbiAgICAgICAgbmFtZTogJ2NhbGN1bGF0ZURpdmlkZW5kJyxcclxuICAgICAgICBvdXRwdXRzOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGludGVybmFsVHlwZTogJ3VpbnQyNTYnLFxyXG4gICAgICAgICAgICAgICAgbmFtZTogJycsXHJcbiAgICAgICAgICAgICAgICB0eXBlOiAndWludDI1NicsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgXSxcclxuICAgICAgICBzdGF0ZU11dGFiaWxpdHk6ICd2aWV3JyxcclxuICAgICAgICB0eXBlOiAnZnVuY3Rpb24nLFxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICBpbnB1dHM6IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgaW50ZXJuYWxUeXBlOiAnYWRkcmVzcycsXHJcbiAgICAgICAgICAgICAgICBuYW1lOiAnJyxcclxuICAgICAgICAgICAgICAgIHR5cGU6ICdhZGRyZXNzJyxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICBdLFxyXG4gICAgICAgIG5hbWU6ICdkZXBvc2l0X3RpbWUnLFxyXG4gICAgICAgIG91dHB1dHM6IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgaW50ZXJuYWxUeXBlOiAndWludDI1NicsXHJcbiAgICAgICAgICAgICAgICBuYW1lOiAnJyxcclxuICAgICAgICAgICAgICAgIHR5cGU6ICd1aW50MjU2JyxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICBdLFxyXG4gICAgICAgIHN0YXRlTXV0YWJpbGl0eTogJ3ZpZXcnLFxyXG4gICAgICAgIHR5cGU6ICdmdW5jdGlvbicsXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICAgIGlucHV0czogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBpbnRlcm5hbFR5cGU6ICdhZGRyZXNzJyxcclxuICAgICAgICAgICAgICAgIG5hbWU6ICdfYWRkcmVzcycsXHJcbiAgICAgICAgICAgICAgICB0eXBlOiAnYWRkcmVzcycsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgXSxcclxuICAgICAgICBuYW1lOiAnaXNTdGFrZWhvbGRlcicsXHJcbiAgICAgICAgb3V0cHV0czogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBpbnRlcm5hbFR5cGU6ICdib29sJyxcclxuICAgICAgICAgICAgICAgIG5hbWU6ICcnLFxyXG4gICAgICAgICAgICAgICAgdHlwZTogJ2Jvb2wnLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBpbnRlcm5hbFR5cGU6ICd1aW50MjU2JyxcclxuICAgICAgICAgICAgICAgIG5hbWU6ICcnLFxyXG4gICAgICAgICAgICAgICAgdHlwZTogJ3VpbnQyNTYnLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIF0sXHJcbiAgICAgICAgc3RhdGVNdXRhYmlsaXR5OiAndmlldycsXHJcbiAgICAgICAgdHlwZTogJ2Z1bmN0aW9uJyxcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgaW5wdXRzOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGludGVybmFsVHlwZTogJ3VpbnQyNTYnLFxyXG4gICAgICAgICAgICAgICAgbmFtZTogJ19hbW91bnQnLFxyXG4gICAgICAgICAgICAgICAgdHlwZTogJ3VpbnQyNTYnLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIF0sXHJcbiAgICAgICAgbmFtZTogJ21heFBheW91dE9mJyxcclxuICAgICAgICBvdXRwdXRzOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGludGVybmFsVHlwZTogJ3VpbnQyNTYnLFxyXG4gICAgICAgICAgICAgICAgbmFtZTogJycsXHJcbiAgICAgICAgICAgICAgICB0eXBlOiAndWludDI1NicsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgXSxcclxuICAgICAgICBzdGF0ZU11dGFiaWxpdHk6ICdwdXJlJyxcclxuICAgICAgICB0eXBlOiAnZnVuY3Rpb24nLFxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICBpbnB1dHM6IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgaW50ZXJuYWxUeXBlOiAnYWRkcmVzcycsXHJcbiAgICAgICAgICAgICAgICBuYW1lOiAnX2FkZHInLFxyXG4gICAgICAgICAgICAgICAgdHlwZTogJ2FkZHJlc3MnLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIF0sXHJcbiAgICAgICAgbmFtZTogJ3Jld2FyZE9mRWFjaFVzZXInLFxyXG4gICAgICAgIG91dHB1dHM6IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgaW50ZXJuYWxUeXBlOiAndWludDI1NicsXHJcbiAgICAgICAgICAgICAgICBuYW1lOiAncGF5b3V0JyxcclxuICAgICAgICAgICAgICAgIHR5cGU6ICd1aW50MjU2JyxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgaW50ZXJuYWxUeXBlOiAndWludDI1NicsXHJcbiAgICAgICAgICAgICAgICBuYW1lOiAnbWF4X3BheW91dCcsXHJcbiAgICAgICAgICAgICAgICB0eXBlOiAndWludDI1NicsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgXSxcclxuICAgICAgICBzdGF0ZU11dGFiaWxpdHk6ICd2aWV3JyxcclxuICAgICAgICB0eXBlOiAnZnVuY3Rpb24nLFxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICBpbnB1dHM6IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgaW50ZXJuYWxUeXBlOiAndWludDI1NicsXHJcbiAgICAgICAgICAgICAgICBuYW1lOiAnJyxcclxuICAgICAgICAgICAgICAgIHR5cGU6ICd1aW50MjU2JyxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICBdLFxyXG4gICAgICAgIG5hbWU6ICdzdGFrZWhvbGRlcnMnLFxyXG4gICAgICAgIG91dHB1dHM6IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgaW50ZXJuYWxUeXBlOiAnYWRkcmVzcycsXHJcbiAgICAgICAgICAgICAgICBuYW1lOiAnJyxcclxuICAgICAgICAgICAgICAgIHR5cGU6ICdhZGRyZXNzJyxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICBdLFxyXG4gICAgICAgIHN0YXRlTXV0YWJpbGl0eTogJ3ZpZXcnLFxyXG4gICAgICAgIHR5cGU6ICdmdW5jdGlvbicsXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICAgIGlucHV0czogW10sXHJcbiAgICAgICAgbmFtZTogJ3Rva2VuJyxcclxuICAgICAgICBvdXRwdXRzOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGludGVybmFsVHlwZTogJ2NvbnRyYWN0IFppbkZpbmFuY2UnLFxyXG4gICAgICAgICAgICAgICAgbmFtZTogJycsXHJcbiAgICAgICAgICAgICAgICB0eXBlOiAnYWRkcmVzcycsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgXSxcclxuICAgICAgICBzdGF0ZU11dGFiaWxpdHk6ICd2aWV3JyxcclxuICAgICAgICB0eXBlOiAnZnVuY3Rpb24nLFxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICBpbnB1dHM6IFtdLFxyXG4gICAgICAgIG5hbWU6ICd0b3RhbFN0YWtlcycsXHJcbiAgICAgICAgb3V0cHV0czogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBpbnRlcm5hbFR5cGU6ICd1aW50MjU2JyxcclxuICAgICAgICAgICAgICAgIG5hbWU6ICcnLFxyXG4gICAgICAgICAgICAgICAgdHlwZTogJ3VpbnQyNTYnLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIF0sXHJcbiAgICAgICAgc3RhdGVNdXRhYmlsaXR5OiAndmlldycsXHJcbiAgICAgICAgdHlwZTogJ2Z1bmN0aW9uJyxcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgaW5wdXRzOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGludGVybmFsVHlwZTogJ2FkZHJlc3MnLFxyXG4gICAgICAgICAgICAgICAgbmFtZTogJycsXHJcbiAgICAgICAgICAgICAgICB0eXBlOiAnYWRkcmVzcycsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgXSxcclxuICAgICAgICBuYW1lOiAndXNlckRhdGEnLFxyXG4gICAgICAgIG91dHB1dHM6IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgaW50ZXJuYWxUeXBlOiAndWludDI1NicsXHJcbiAgICAgICAgICAgICAgICBuYW1lOiAnc3Rha2VzJyxcclxuICAgICAgICAgICAgICAgIHR5cGU6ICd1aW50MjU2JyxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgaW50ZXJuYWxUeXBlOiAndWludDI1NicsXHJcbiAgICAgICAgICAgICAgICBuYW1lOiAnZGVwb3NpdF90aW1lJyxcclxuICAgICAgICAgICAgICAgIHR5cGU6ICd1aW50MjU2JyxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgaW50ZXJuYWxUeXBlOiAndWludDI1NicsXHJcbiAgICAgICAgICAgICAgICBuYW1lOiAnZGVwb3NpdF9wYXlvdXRzJyxcclxuICAgICAgICAgICAgICAgIHR5cGU6ICd1aW50MjU2JyxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICBdLFxyXG4gICAgICAgIHN0YXRlTXV0YWJpbGl0eTogJ3ZpZXcnLFxyXG4gICAgICAgIHR5cGU6ICdmdW5jdGlvbicsXHJcbiAgICB9LFxyXG5dO1xyXG4iXSwic291cmNlUm9vdCI6IiJ9