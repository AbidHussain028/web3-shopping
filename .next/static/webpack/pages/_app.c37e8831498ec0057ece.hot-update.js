webpackHotUpdate_N_E("pages/_app",{

/***/ "./store/cart/saga.js":
/*!****************************!*\
  !*** ./store/cart/saga.js ***!
  \****************************/
/*! exports provided: calculateAmount, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "calculateAmount", function() { return calculateAmount; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return rootSaga; });
/* harmony import */ var D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/regenerator */ "./node_modules/next/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! redux-saga/effects */ "./node_modules/redux-saga/dist/redux-saga-effects-npm-proxy.esm.js");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _action__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./action */ "./store/cart/action.js");



function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var _marked = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(getCartSaga),
    _marked2 = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(addItemSaga),
    _marked3 = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(removeItemSaga),
    _marked4 = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(increaseQtySaga),
    _marked5 = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(decreaseItemQtySaga),
    _marked6 = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(clearCartSaga),
    _marked7 = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(rootSaga);





var modalSuccess = function modalSuccess(type) {
  antd__WEBPACK_IMPORTED_MODULE_3__["notification"][type]({
    message: "Success",
    description: "This product has been added to your cart!",
    duration: 1
  });
};

var modalWarning = function modalWarning(type) {
  antd__WEBPACK_IMPORTED_MODULE_3__["notification"][type]({
    message: "Remove A Item",
    description: "This product has been removed from your cart!",
    duration: 1
  });
};

var calculateAmount = function calculateAmount(obj) {
  return Object.values(obj).reduce(function (acc, _ref) {
    var quantity = _ref.quantity,
        price = _ref.price;
    return acc + quantity * price;
  }, 0).toFixed(2);
};

function getCartSaga() {
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function getCartSaga$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["getCartSuccess"])());

        case 3:
          _context.next = 9;
          break;

        case 5:
          _context.prev = 5;
          _context.t0 = _context["catch"](0);
          _context.next = 9;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["getCartError"])(_context.t0));

        case 9:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, null, [[0, 5]]);
}

function addItemSaga(payload) {
  var product, localCart, currentCart, existItem;
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function addItemSaga$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          product = payload.product;
          localCart = JSON.parse(localStorage.getItem("persist:martfury")).cart;
          currentCart = JSON.parse(localCart);
          existItem = currentCart.cartItems.find(function (item) {
            return item.id === product.id;
          });

          if (existItem) {
            existItem.quantity += product.quantity;
          } else {
            if (!product.quantity) {
              product.quantity = 1;
            }

            currentCart.cartItems = [_objectSpread({}, product)];
          }

          console.log(currentCart.cartItems);
          currentCart.amount = calculateAmount(currentCart.cartItems);
          currentCart.cartTotal++;
          _context2.next = 11;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["updateCartSuccess"])(currentCart));

        case 11:
          modalSuccess("success");
          _context2.next = 18;
          break;

        case 14:
          _context2.prev = 14;
          _context2.t0 = _context2["catch"](0);
          _context2.next = 18;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["getCartError"])(_context2.t0));

        case 18:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2, null, [[0, 14]]);
}

function removeItemSaga(payload) {
  var product, localCart, index;
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function removeItemSaga$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          product = payload.product;
          localCart = JSON.parse(JSON.parse(localStorage.getItem("persist:martfury")).cart);
          index = localCart.cartItems.findIndex(function (item) {
            return item.id === product.id;
          });
          localCart.cartTotal = localCart.cartTotal - product.quantity;
          localCart.cartItems.splice(index, 1);
          localCart.amount = calculateAmount(localCart.cartItems);

          if (localCart.cartItems.length === 0) {
            localCart.cartItems = [];
            localCart.amount = 0;
            localCart.cartTotal = 0;
          }

          _context3.next = 10;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["updateCartSuccess"])(localCart));

        case 10:
          modalWarning("warning");
          _context3.next = 17;
          break;

        case 13:
          _context3.prev = 13;
          _context3.t0 = _context3["catch"](0);
          _context3.next = 17;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["getCartError"])(_context3.t0));

        case 17:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3, null, [[0, 13]]);
}

function increaseQtySaga(payload) {
  var product, localCart, selectedItem;
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function increaseQtySaga$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.prev = 0;
          product = payload.product;
          localCart = JSON.parse(JSON.parse(localStorage.getItem("persist:martfury")).cart);
          selectedItem = localCart.cartItems.find(function (item) {
            return item.id === product.id;
          });

          if (selectedItem) {
            selectedItem.quantity++;
            localCart.cartTotal++;
            localCart.amount = calculateAmount(localCart.cartItems);
          }

          _context4.next = 7;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["updateCartSuccess"])(localCart));

        case 7:
          _context4.next = 13;
          break;

        case 9:
          _context4.prev = 9;
          _context4.t0 = _context4["catch"](0);
          _context4.next = 13;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["getCartError"])(_context4.t0));

        case 13:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4, null, [[0, 9]]);
}

function decreaseItemQtySaga(payload) {
  var product, localCart, selectedItem;
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function decreaseItemQtySaga$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.prev = 0;
          product = payload.product;
          localCart = JSON.parse(JSON.parse(localStorage.getItem("persist:martfury")).cart);
          selectedItem = localCart.cartItems.find(function (item) {
            return item.id === product.id;
          });

          if (selectedItem) {
            selectedItem.quantity--;
            localCart.cartTotal--;
            localCart.amount = calculateAmount(localCart.cartItems);
          }

          _context5.next = 7;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["updateCartSuccess"])(localCart));

        case 7:
          _context5.next = 13;
          break;

        case 9:
          _context5.prev = 9;
          _context5.t0 = _context5["catch"](0);
          _context5.next = 13;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["getCartError"])(_context5.t0));

        case 13:
        case "end":
          return _context5.stop();
      }
    }
  }, _marked5, null, [[0, 9]]);
}

function clearCartSaga() {
  var emptyCart;
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function clearCartSaga$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.prev = 0;
          emptyCart = {
            cartItems: [],
            amount: 0,
            cartTotal: 0
          };
          _context6.next = 4;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["updateCartSuccess"])(emptyCart));

        case 4:
          _context6.next = 10;
          break;

        case 6:
          _context6.prev = 6;
          _context6.t0 = _context6["catch"](0);
          _context6.next = 10;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["updateCartError"])(_context6.t0));

        case 10:
        case "end":
          return _context6.stop();
      }
    }
  }, _marked6, null, [[0, 6]]);
}

function rootSaga() {
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function rootSaga$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          _context7.next = 2;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["all"])([Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["takeEvery"])(_action__WEBPACK_IMPORTED_MODULE_4__["actionTypes"].GET_CART, getCartSaga)]);

        case 2:
          _context7.next = 4;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["all"])([Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["takeEvery"])(_action__WEBPACK_IMPORTED_MODULE_4__["actionTypes"].ADD_ITEM, addItemSaga)]);

        case 4:
          _context7.next = 6;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["all"])([Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["takeEvery"])(_action__WEBPACK_IMPORTED_MODULE_4__["actionTypes"].REMOVE_ITEM, removeItemSaga)]);

        case 6:
          _context7.next = 8;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["all"])([Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["takeEvery"])(_action__WEBPACK_IMPORTED_MODULE_4__["actionTypes"].INCREASE_QTY, increaseQtySaga)]);

        case 8:
          _context7.next = 10;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["all"])([Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["takeEvery"])(_action__WEBPACK_IMPORTED_MODULE_4__["actionTypes"].DECREASE_QTY, decreaseItemQtySaga)]);

        case 10:
        case "end":
          return _context7.stop();
      }
    }
  }, _marked7);
}

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vc3RvcmUvY2FydC9zYWdhLmpzIl0sIm5hbWVzIjpbImdldENhcnRTYWdhIiwiYWRkSXRlbVNhZ2EiLCJyZW1vdmVJdGVtU2FnYSIsImluY3JlYXNlUXR5U2FnYSIsImRlY3JlYXNlSXRlbVF0eVNhZ2EiLCJjbGVhckNhcnRTYWdhIiwicm9vdFNhZ2EiLCJtb2RhbFN1Y2Nlc3MiLCJ0eXBlIiwibm90aWZpY2F0aW9uIiwibWVzc2FnZSIsImRlc2NyaXB0aW9uIiwiZHVyYXRpb24iLCJtb2RhbFdhcm5pbmciLCJjYWxjdWxhdGVBbW91bnQiLCJvYmoiLCJPYmplY3QiLCJ2YWx1ZXMiLCJyZWR1Y2UiLCJhY2MiLCJxdWFudGl0eSIsInByaWNlIiwidG9GaXhlZCIsInB1dCIsImdldENhcnRTdWNjZXNzIiwiZ2V0Q2FydEVycm9yIiwicGF5bG9hZCIsInByb2R1Y3QiLCJsb2NhbENhcnQiLCJKU09OIiwicGFyc2UiLCJsb2NhbFN0b3JhZ2UiLCJnZXRJdGVtIiwiY2FydCIsImN1cnJlbnRDYXJ0IiwiZXhpc3RJdGVtIiwiY2FydEl0ZW1zIiwiZmluZCIsIml0ZW0iLCJpZCIsImNvbnNvbGUiLCJsb2ciLCJhbW91bnQiLCJjYXJ0VG90YWwiLCJ1cGRhdGVDYXJ0U3VjY2VzcyIsImluZGV4IiwiZmluZEluZGV4Iiwic3BsaWNlIiwibGVuZ3RoIiwic2VsZWN0ZWRJdGVtIiwiZW1wdHlDYXJ0IiwidXBkYXRlQ2FydEVycm9yIiwiYWxsIiwidGFrZUV2ZXJ5IiwiYWN0aW9uVHlwZXMiLCJHRVRfQ0FSVCIsIkFERF9JVEVNIiwiUkVNT1ZFX0lURU0iLCJJTkNSRUFTRV9RVFkiLCJERUNSRUFTRV9RVFkiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O3dLQStCVUEsVzt5S0FRQUMsVzt5S0EyQkFDLGM7eUtBc0JBQyxlO3lLQW9CQUMsbUI7eUtBcUJBQyxhO3lLQWFlQyxROztBQTlJekI7QUFDQTtBQUVBOztBQVFBLElBQU1DLFlBQVksR0FBRyxTQUFmQSxZQUFlLENBQUNDLElBQUQsRUFBVTtBQUM3QkMsbURBQVksQ0FBQ0QsSUFBRCxDQUFaLENBQW1CO0FBQ2pCRSxXQUFPLEVBQUUsU0FEUTtBQUVqQkMsZUFBVyxFQUFFLDJDQUZJO0FBR2pCQyxZQUFRLEVBQUU7QUFITyxHQUFuQjtBQUtELENBTkQ7O0FBT0EsSUFBTUMsWUFBWSxHQUFHLFNBQWZBLFlBQWUsQ0FBQ0wsSUFBRCxFQUFVO0FBQzdCQyxtREFBWSxDQUFDRCxJQUFELENBQVosQ0FBbUI7QUFDakJFLFdBQU8sRUFBRSxlQURRO0FBRWpCQyxlQUFXLEVBQUUsK0NBRkk7QUFHakJDLFlBQVEsRUFBRTtBQUhPLEdBQW5CO0FBS0QsQ0FORDs7QUFRTyxJQUFNRSxlQUFlLEdBQUcsU0FBbEJBLGVBQWtCLENBQUNDLEdBQUQ7QUFBQSxTQUM3QkMsTUFBTSxDQUFDQyxNQUFQLENBQWNGLEdBQWQsRUFDR0csTUFESCxDQUNVLFVBQUNDLEdBQUQ7QUFBQSxRQUFRQyxRQUFSLFFBQVFBLFFBQVI7QUFBQSxRQUFrQkMsS0FBbEIsUUFBa0JBLEtBQWxCO0FBQUEsV0FBOEJGLEdBQUcsR0FBR0MsUUFBUSxHQUFHQyxLQUEvQztBQUFBLEdBRFYsRUFDZ0UsQ0FEaEUsRUFFR0MsT0FGSCxDQUVXLENBRlgsQ0FENkI7QUFBQSxDQUF4Qjs7QUFLUCxTQUFVdEIsV0FBVjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVJLGlCQUFNdUIsOERBQUcsQ0FBQ0MsOERBQWMsRUFBZixDQUFUOztBQUZKO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlJLGlCQUFNRCw4REFBRyxDQUFDRSw0REFBWSxhQUFiLENBQVQ7O0FBSko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBUUEsU0FBVXhCLFdBQVYsQ0FBc0J5QixPQUF0QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVZQyxpQkFGWixHQUV3QkQsT0FGeEIsQ0FFWUMsT0FGWjtBQUdVQyxtQkFIVixHQUdzQkMsSUFBSSxDQUFDQyxLQUFMLENBQVdDLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixrQkFBckIsQ0FBWCxFQUFxREMsSUFIM0U7QUFJUUMscUJBSlIsR0FJc0JMLElBQUksQ0FBQ0MsS0FBTCxDQUFXRixTQUFYLENBSnRCO0FBS1FPLG1CQUxSLEdBS29CRCxXQUFXLENBQUNFLFNBQVosQ0FBc0JDLElBQXRCLENBQ2QsVUFBQ0MsSUFBRDtBQUFBLG1CQUFVQSxJQUFJLENBQUNDLEVBQUwsS0FBWVosT0FBTyxDQUFDWSxFQUE5QjtBQUFBLFdBRGMsQ0FMcEI7O0FBUUksY0FBSUosU0FBSixFQUFlO0FBQ2JBLHFCQUFTLENBQUNmLFFBQVYsSUFBc0JPLE9BQU8sQ0FBQ1AsUUFBOUI7QUFDRCxXQUZELE1BRU87QUFDTCxnQkFBSSxDQUFDTyxPQUFPLENBQUNQLFFBQWIsRUFBdUI7QUFDckJPLHFCQUFPLENBQUNQLFFBQVIsR0FBbUIsQ0FBbkI7QUFDRDs7QUFDRGMsdUJBQVcsQ0FBQ0UsU0FBWixHQUF3QixtQkFBTVQsT0FBTixFQUF4QjtBQUNEOztBQUNEYSxpQkFBTyxDQUFDQyxHQUFSLENBQVlQLFdBQVcsQ0FBQ0UsU0FBeEI7QUFFQUYscUJBQVcsQ0FBQ1EsTUFBWixHQUFxQjVCLGVBQWUsQ0FBQ29CLFdBQVcsQ0FBQ0UsU0FBYixDQUFwQztBQUNBRixxQkFBVyxDQUFDUyxTQUFaO0FBbkJKO0FBb0JJLGlCQUFNcEIsOERBQUcsQ0FBQ3FCLGlFQUFpQixDQUFDVixXQUFELENBQWxCLENBQVQ7O0FBcEJKO0FBcUJJM0Isc0JBQVksQ0FBQyxTQUFELENBQVo7QUFyQko7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXVCSSxpQkFBTWdCLDhEQUFHLENBQUNFLDREQUFZLGNBQWIsQ0FBVDs7QUF2Qko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBMkJBLFNBQVV2QixjQUFWLENBQXlCd0IsT0FBekI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFWUMsaUJBRlosR0FFd0JELE9BRnhCLENBRVlDLE9BRlo7QUFHUUMsbUJBSFIsR0FHb0JDLElBQUksQ0FBQ0MsS0FBTCxDQUNkRCxJQUFJLENBQUNDLEtBQUwsQ0FBV0MsWUFBWSxDQUFDQyxPQUFiLENBQXFCLGtCQUFyQixDQUFYLEVBQXFEQyxJQUR2QyxDQUhwQjtBQU1RWSxlQU5SLEdBTWdCakIsU0FBUyxDQUFDUSxTQUFWLENBQW9CVSxTQUFwQixDQUE4QixVQUFDUixJQUFEO0FBQUEsbUJBQVVBLElBQUksQ0FBQ0MsRUFBTCxLQUFZWixPQUFPLENBQUNZLEVBQTlCO0FBQUEsV0FBOUIsQ0FOaEI7QUFPSVgsbUJBQVMsQ0FBQ2UsU0FBVixHQUFzQmYsU0FBUyxDQUFDZSxTQUFWLEdBQXNCaEIsT0FBTyxDQUFDUCxRQUFwRDtBQUNBUSxtQkFBUyxDQUFDUSxTQUFWLENBQW9CVyxNQUFwQixDQUEyQkYsS0FBM0IsRUFBa0MsQ0FBbEM7QUFDQWpCLG1CQUFTLENBQUNjLE1BQVYsR0FBbUI1QixlQUFlLENBQUNjLFNBQVMsQ0FBQ1EsU0FBWCxDQUFsQzs7QUFDQSxjQUFJUixTQUFTLENBQUNRLFNBQVYsQ0FBb0JZLE1BQXBCLEtBQStCLENBQW5DLEVBQXNDO0FBQ3BDcEIscUJBQVMsQ0FBQ1EsU0FBVixHQUFzQixFQUF0QjtBQUNBUixxQkFBUyxDQUFDYyxNQUFWLEdBQW1CLENBQW5CO0FBQ0FkLHFCQUFTLENBQUNlLFNBQVYsR0FBc0IsQ0FBdEI7QUFDRDs7QUFkTDtBQWVJLGlCQUFNcEIsOERBQUcsQ0FBQ3FCLGlFQUFpQixDQUFDaEIsU0FBRCxDQUFsQixDQUFUOztBQWZKO0FBZ0JJZixzQkFBWSxDQUFDLFNBQUQsQ0FBWjtBQWhCSjtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBa0JJLGlCQUFNVSw4REFBRyxDQUFDRSw0REFBWSxjQUFiLENBQVQ7O0FBbEJKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQXNCQSxTQUFVdEIsZUFBVixDQUEwQnVCLE9BQTFCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRVlDLGlCQUZaLEdBRXdCRCxPQUZ4QixDQUVZQyxPQUZaO0FBR1FDLG1CQUhSLEdBR29CQyxJQUFJLENBQUNDLEtBQUwsQ0FDZEQsSUFBSSxDQUFDQyxLQUFMLENBQVdDLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixrQkFBckIsQ0FBWCxFQUFxREMsSUFEdkMsQ0FIcEI7QUFNUWdCLHNCQU5SLEdBTXVCckIsU0FBUyxDQUFDUSxTQUFWLENBQW9CQyxJQUFwQixDQUNqQixVQUFDQyxJQUFEO0FBQUEsbUJBQVVBLElBQUksQ0FBQ0MsRUFBTCxLQUFZWixPQUFPLENBQUNZLEVBQTlCO0FBQUEsV0FEaUIsQ0FOdkI7O0FBU0ksY0FBSVUsWUFBSixFQUFrQjtBQUNoQkEsd0JBQVksQ0FBQzdCLFFBQWI7QUFDQVEscUJBQVMsQ0FBQ2UsU0FBVjtBQUNBZixxQkFBUyxDQUFDYyxNQUFWLEdBQW1CNUIsZUFBZSxDQUFDYyxTQUFTLENBQUNRLFNBQVgsQ0FBbEM7QUFDRDs7QUFiTDtBQWNJLGlCQUFNYiw4REFBRyxDQUFDcUIsaUVBQWlCLENBQUNoQixTQUFELENBQWxCLENBQVQ7O0FBZEo7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZ0JJLGlCQUFNTCw4REFBRyxDQUFDRSw0REFBWSxjQUFiLENBQVQ7O0FBaEJKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQW9CQSxTQUFVckIsbUJBQVYsQ0FBOEJzQixPQUE5QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVZQyxpQkFGWixHQUV3QkQsT0FGeEIsQ0FFWUMsT0FGWjtBQUdVQyxtQkFIVixHQUdzQkMsSUFBSSxDQUFDQyxLQUFMLENBQ2hCRCxJQUFJLENBQUNDLEtBQUwsQ0FBV0MsWUFBWSxDQUFDQyxPQUFiLENBQXFCLGtCQUFyQixDQUFYLEVBQXFEQyxJQURyQyxDQUh0QjtBQU1RZ0Isc0JBTlIsR0FNdUJyQixTQUFTLENBQUNRLFNBQVYsQ0FBb0JDLElBQXBCLENBQ2pCLFVBQUNDLElBQUQ7QUFBQSxtQkFBVUEsSUFBSSxDQUFDQyxFQUFMLEtBQVlaLE9BQU8sQ0FBQ1ksRUFBOUI7QUFBQSxXQURpQixDQU52Qjs7QUFVSSxjQUFJVSxZQUFKLEVBQWtCO0FBQ2hCQSx3QkFBWSxDQUFDN0IsUUFBYjtBQUNBUSxxQkFBUyxDQUFDZSxTQUFWO0FBQ0FmLHFCQUFTLENBQUNjLE1BQVYsR0FBbUI1QixlQUFlLENBQUNjLFNBQVMsQ0FBQ1EsU0FBWCxDQUFsQztBQUNEOztBQWRMO0FBZUksaUJBQU1iLDhEQUFHLENBQUNxQixpRUFBaUIsQ0FBQ2hCLFNBQUQsQ0FBbEIsQ0FBVDs7QUFmSjtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFpQkksaUJBQU1MLDhEQUFHLENBQUNFLDREQUFZLGNBQWIsQ0FBVDs7QUFqQko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBcUJBLFNBQVVwQixhQUFWO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRVU2QyxtQkFGVixHQUVzQjtBQUNoQmQscUJBQVMsRUFBRSxFQURLO0FBRWhCTSxrQkFBTSxFQUFFLENBRlE7QUFHaEJDLHFCQUFTLEVBQUU7QUFISyxXQUZ0QjtBQUFBO0FBT0ksaUJBQU1wQiw4REFBRyxDQUFDcUIsaUVBQWlCLENBQUNNLFNBQUQsQ0FBbEIsQ0FBVDs7QUFQSjtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTSSxpQkFBTTNCLDhEQUFHLENBQUM0QiwrREFBZSxjQUFoQixDQUFUOztBQVRKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQWFlLFNBQVU3QyxRQUFWO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNiLGlCQUFNOEMsOERBQUcsQ0FBQyxDQUFDQyxvRUFBUyxDQUFDQyxtREFBVyxDQUFDQyxRQUFiLEVBQXVCdkQsV0FBdkIsQ0FBVixDQUFELENBQVQ7O0FBRGE7QUFBQTtBQUViLGlCQUFNb0QsOERBQUcsQ0FBQyxDQUFDQyxvRUFBUyxDQUFDQyxtREFBVyxDQUFDRSxRQUFiLEVBQXVCdkQsV0FBdkIsQ0FBVixDQUFELENBQVQ7O0FBRmE7QUFBQTtBQUdiLGlCQUFNbUQsOERBQUcsQ0FBQyxDQUFDQyxvRUFBUyxDQUFDQyxtREFBVyxDQUFDRyxXQUFiLEVBQTBCdkQsY0FBMUIsQ0FBVixDQUFELENBQVQ7O0FBSGE7QUFBQTtBQUliLGlCQUFNa0QsOERBQUcsQ0FBQyxDQUFDQyxvRUFBUyxDQUFDQyxtREFBVyxDQUFDSSxZQUFiLEVBQTJCdkQsZUFBM0IsQ0FBVixDQUFELENBQVQ7O0FBSmE7QUFBQTtBQUtiLGlCQUFNaUQsOERBQUcsQ0FBQyxDQUFDQyxvRUFBUyxDQUFDQyxtREFBVyxDQUFDSyxZQUFiLEVBQTJCdkQsbUJBQTNCLENBQVYsQ0FBRCxDQUFUOztBQUxhO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL19hcHAuYzM3ZTg4MzE0OThlYzAwNTdlY2UuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGFsbCwgcHV0LCB0YWtlRXZlcnkgfSBmcm9tIFwicmVkdXgtc2FnYS9lZmZlY3RzXCI7XHJcbmltcG9ydCB7IG5vdGlmaWNhdGlvbiB9IGZyb20gXCJhbnRkXCI7XHJcblxyXG5pbXBvcnQge1xyXG4gIGFjdGlvblR5cGVzLFxyXG4gIGdldENhcnRFcnJvcixcclxuICBnZXRDYXJ0U3VjY2VzcyxcclxuICB1cGRhdGVDYXJ0U3VjY2VzcyxcclxuICB1cGRhdGVDYXJ0RXJyb3IsXHJcbn0gZnJvbSBcIi4vYWN0aW9uXCI7XHJcblxyXG5jb25zdCBtb2RhbFN1Y2Nlc3MgPSAodHlwZSkgPT4ge1xyXG4gIG5vdGlmaWNhdGlvblt0eXBlXSh7XHJcbiAgICBtZXNzYWdlOiBcIlN1Y2Nlc3NcIixcclxuICAgIGRlc2NyaXB0aW9uOiBcIlRoaXMgcHJvZHVjdCBoYXMgYmVlbiBhZGRlZCB0byB5b3VyIGNhcnQhXCIsXHJcbiAgICBkdXJhdGlvbjogMSxcclxuICB9KTtcclxufTtcclxuY29uc3QgbW9kYWxXYXJuaW5nID0gKHR5cGUpID0+IHtcclxuICBub3RpZmljYXRpb25bdHlwZV0oe1xyXG4gICAgbWVzc2FnZTogXCJSZW1vdmUgQSBJdGVtXCIsXHJcbiAgICBkZXNjcmlwdGlvbjogXCJUaGlzIHByb2R1Y3QgaGFzIGJlZW4gcmVtb3ZlZCBmcm9tIHlvdXIgY2FydCFcIixcclxuICAgIGR1cmF0aW9uOiAxLFxyXG4gIH0pO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGNhbGN1bGF0ZUFtb3VudCA9IChvYmopID0+XHJcbiAgT2JqZWN0LnZhbHVlcyhvYmopXHJcbiAgICAucmVkdWNlKChhY2MsIHsgcXVhbnRpdHksIHByaWNlIH0pID0+IGFjYyArIHF1YW50aXR5ICogcHJpY2UsIDApXHJcbiAgICAudG9GaXhlZCgyKTtcclxuXHJcbmZ1bmN0aW9uKiBnZXRDYXJ0U2FnYSgpIHtcclxuICB0cnkge1xyXG4gICAgeWllbGQgcHV0KGdldENhcnRTdWNjZXNzKCkpO1xyXG4gIH0gY2F0Y2ggKGVycikge1xyXG4gICAgeWllbGQgcHV0KGdldENhcnRFcnJvcihlcnIpKTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uKiBhZGRJdGVtU2FnYShwYXlsb2FkKSB7XHJcbiAgdHJ5IHtcclxuICAgIGNvbnN0IHsgcHJvZHVjdCB9ID0gcGF5bG9hZDtcclxuICAgIGNvbnN0IGxvY2FsQ2FydCA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJwZXJzaXN0Om1hcnRmdXJ5XCIpKS5jYXJ0O1xyXG4gICAgbGV0IGN1cnJlbnRDYXJ0ID0gSlNPTi5wYXJzZShsb2NhbENhcnQpO1xyXG4gICAgbGV0IGV4aXN0SXRlbSA9IGN1cnJlbnRDYXJ0LmNhcnRJdGVtcy5maW5kKFxyXG4gICAgICAoaXRlbSkgPT4gaXRlbS5pZCA9PT0gcHJvZHVjdC5pZFxyXG4gICAgKTtcclxuICAgIGlmIChleGlzdEl0ZW0pIHtcclxuICAgICAgZXhpc3RJdGVtLnF1YW50aXR5ICs9IHByb2R1Y3QucXVhbnRpdHk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBpZiAoIXByb2R1Y3QucXVhbnRpdHkpIHtcclxuICAgICAgICBwcm9kdWN0LnF1YW50aXR5ID0gMTtcclxuICAgICAgfVxyXG4gICAgICBjdXJyZW50Q2FydC5jYXJ0SXRlbXMgPSBbeyAuLi5wcm9kdWN0IH1dO1xyXG4gICAgfVxyXG4gICAgY29uc29sZS5sb2coY3VycmVudENhcnQuY2FydEl0ZW1zKTtcclxuXHJcbiAgICBjdXJyZW50Q2FydC5hbW91bnQgPSBjYWxjdWxhdGVBbW91bnQoY3VycmVudENhcnQuY2FydEl0ZW1zKTtcclxuICAgIGN1cnJlbnRDYXJ0LmNhcnRUb3RhbCsrO1xyXG4gICAgeWllbGQgcHV0KHVwZGF0ZUNhcnRTdWNjZXNzKGN1cnJlbnRDYXJ0KSk7XHJcbiAgICBtb2RhbFN1Y2Nlc3MoXCJzdWNjZXNzXCIpO1xyXG4gIH0gY2F0Y2ggKGVycikge1xyXG4gICAgeWllbGQgcHV0KGdldENhcnRFcnJvcihlcnIpKTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uKiByZW1vdmVJdGVtU2FnYShwYXlsb2FkKSB7XHJcbiAgdHJ5IHtcclxuICAgIGNvbnN0IHsgcHJvZHVjdCB9ID0gcGF5bG9hZDtcclxuICAgIGxldCBsb2NhbENhcnQgPSBKU09OLnBhcnNlKFxyXG4gICAgICBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwicGVyc2lzdDptYXJ0ZnVyeVwiKSkuY2FydFxyXG4gICAgKTtcclxuICAgIGxldCBpbmRleCA9IGxvY2FsQ2FydC5jYXJ0SXRlbXMuZmluZEluZGV4KChpdGVtKSA9PiBpdGVtLmlkID09PSBwcm9kdWN0LmlkKTtcclxuICAgIGxvY2FsQ2FydC5jYXJ0VG90YWwgPSBsb2NhbENhcnQuY2FydFRvdGFsIC0gcHJvZHVjdC5xdWFudGl0eTtcclxuICAgIGxvY2FsQ2FydC5jYXJ0SXRlbXMuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgIGxvY2FsQ2FydC5hbW91bnQgPSBjYWxjdWxhdGVBbW91bnQobG9jYWxDYXJ0LmNhcnRJdGVtcyk7XHJcbiAgICBpZiAobG9jYWxDYXJ0LmNhcnRJdGVtcy5sZW5ndGggPT09IDApIHtcclxuICAgICAgbG9jYWxDYXJ0LmNhcnRJdGVtcyA9IFtdO1xyXG4gICAgICBsb2NhbENhcnQuYW1vdW50ID0gMDtcclxuICAgICAgbG9jYWxDYXJ0LmNhcnRUb3RhbCA9IDA7XHJcbiAgICB9XHJcbiAgICB5aWVsZCBwdXQodXBkYXRlQ2FydFN1Y2Nlc3MobG9jYWxDYXJ0KSk7XHJcbiAgICBtb2RhbFdhcm5pbmcoXCJ3YXJuaW5nXCIpO1xyXG4gIH0gY2F0Y2ggKGVycikge1xyXG4gICAgeWllbGQgcHV0KGdldENhcnRFcnJvcihlcnIpKTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uKiBpbmNyZWFzZVF0eVNhZ2EocGF5bG9hZCkge1xyXG4gIHRyeSB7XHJcbiAgICBjb25zdCB7IHByb2R1Y3QgfSA9IHBheWxvYWQ7XHJcbiAgICBsZXQgbG9jYWxDYXJ0ID0gSlNPTi5wYXJzZShcclxuICAgICAgSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcInBlcnNpc3Q6bWFydGZ1cnlcIikpLmNhcnRcclxuICAgICk7XHJcbiAgICBsZXQgc2VsZWN0ZWRJdGVtID0gbG9jYWxDYXJ0LmNhcnRJdGVtcy5maW5kKFxyXG4gICAgICAoaXRlbSkgPT4gaXRlbS5pZCA9PT0gcHJvZHVjdC5pZFxyXG4gICAgKTtcclxuICAgIGlmIChzZWxlY3RlZEl0ZW0pIHtcclxuICAgICAgc2VsZWN0ZWRJdGVtLnF1YW50aXR5Kys7XHJcbiAgICAgIGxvY2FsQ2FydC5jYXJ0VG90YWwrKztcclxuICAgICAgbG9jYWxDYXJ0LmFtb3VudCA9IGNhbGN1bGF0ZUFtb3VudChsb2NhbENhcnQuY2FydEl0ZW1zKTtcclxuICAgIH1cclxuICAgIHlpZWxkIHB1dCh1cGRhdGVDYXJ0U3VjY2Vzcyhsb2NhbENhcnQpKTtcclxuICB9IGNhdGNoIChlcnIpIHtcclxuICAgIHlpZWxkIHB1dChnZXRDYXJ0RXJyb3IoZXJyKSk7XHJcbiAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiogZGVjcmVhc2VJdGVtUXR5U2FnYShwYXlsb2FkKSB7XHJcbiAgdHJ5IHtcclxuICAgIGNvbnN0IHsgcHJvZHVjdCB9ID0gcGF5bG9hZDtcclxuICAgIGNvbnN0IGxvY2FsQ2FydCA9IEpTT04ucGFyc2UoXHJcbiAgICAgIEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJwZXJzaXN0Om1hcnRmdXJ5XCIpKS5jYXJ0XHJcbiAgICApO1xyXG4gICAgbGV0IHNlbGVjdGVkSXRlbSA9IGxvY2FsQ2FydC5jYXJ0SXRlbXMuZmluZChcclxuICAgICAgKGl0ZW0pID0+IGl0ZW0uaWQgPT09IHByb2R1Y3QuaWRcclxuICAgICk7XHJcblxyXG4gICAgaWYgKHNlbGVjdGVkSXRlbSkge1xyXG4gICAgICBzZWxlY3RlZEl0ZW0ucXVhbnRpdHktLTtcclxuICAgICAgbG9jYWxDYXJ0LmNhcnRUb3RhbC0tO1xyXG4gICAgICBsb2NhbENhcnQuYW1vdW50ID0gY2FsY3VsYXRlQW1vdW50KGxvY2FsQ2FydC5jYXJ0SXRlbXMpO1xyXG4gICAgfVxyXG4gICAgeWllbGQgcHV0KHVwZGF0ZUNhcnRTdWNjZXNzKGxvY2FsQ2FydCkpO1xyXG4gIH0gY2F0Y2ggKGVycikge1xyXG4gICAgeWllbGQgcHV0KGdldENhcnRFcnJvcihlcnIpKTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uKiBjbGVhckNhcnRTYWdhKCkge1xyXG4gIHRyeSB7XHJcbiAgICBjb25zdCBlbXB0eUNhcnQgPSB7XHJcbiAgICAgIGNhcnRJdGVtczogW10sXHJcbiAgICAgIGFtb3VudDogMCxcclxuICAgICAgY2FydFRvdGFsOiAwLFxyXG4gICAgfTtcclxuICAgIHlpZWxkIHB1dCh1cGRhdGVDYXJ0U3VjY2VzcyhlbXB0eUNhcnQpKTtcclxuICB9IGNhdGNoIChlcnIpIHtcclxuICAgIHlpZWxkIHB1dCh1cGRhdGVDYXJ0RXJyb3IoZXJyKSk7XHJcbiAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiogcm9vdFNhZ2EoKSB7XHJcbiAgeWllbGQgYWxsKFt0YWtlRXZlcnkoYWN0aW9uVHlwZXMuR0VUX0NBUlQsIGdldENhcnRTYWdhKV0pO1xyXG4gIHlpZWxkIGFsbChbdGFrZUV2ZXJ5KGFjdGlvblR5cGVzLkFERF9JVEVNLCBhZGRJdGVtU2FnYSldKTtcclxuICB5aWVsZCBhbGwoW3Rha2VFdmVyeShhY3Rpb25UeXBlcy5SRU1PVkVfSVRFTSwgcmVtb3ZlSXRlbVNhZ2EpXSk7XHJcbiAgeWllbGQgYWxsKFt0YWtlRXZlcnkoYWN0aW9uVHlwZXMuSU5DUkVBU0VfUVRZLCBpbmNyZWFzZVF0eVNhZ2EpXSk7XHJcbiAgeWllbGQgYWxsKFt0YWtlRXZlcnkoYWN0aW9uVHlwZXMuREVDUkVBU0VfUVRZLCBkZWNyZWFzZUl0ZW1RdHlTYWdhKV0pO1xyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiIn0=