webpackHotUpdate_N_E("pages/_app",{

/***/ "./store/cart/saga.js":
/*!****************************!*\
  !*** ./store/cart/saga.js ***!
  \****************************/
/*! exports provided: calculateAmount, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "calculateAmount", function() { return calculateAmount; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return rootSaga; });
/* harmony import */ var D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/regenerator */ "./node_modules/next/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-saga/effects */ "./node_modules/redux-saga/dist/redux-saga-effects-npm-proxy.esm.js");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _action__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./action */ "./store/cart/action.js");


var _marked = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(getCartSaga),
    _marked2 = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(addItemSaga),
    _marked3 = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(removeItemSaga),
    _marked4 = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(increaseQtySaga),
    _marked5 = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(decreaseItemQtySaga),
    _marked6 = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(clearCartSaga),
    _marked7 = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(rootSaga);





var modalSuccess = function modalSuccess(type) {
  antd__WEBPACK_IMPORTED_MODULE_2__["notification"][type]({
    message: "Success",
    description: "This product has been added to your cart!",
    duration: 1
  });
};

var modalWarning = function modalWarning(type) {
  antd__WEBPACK_IMPORTED_MODULE_2__["notification"][type]({
    message: "Remove A Item",
    description: "This product has been removed from your cart!",
    duration: 1
  });
};

var calculateAmount = function calculateAmount(obj) {
  return Object.values(obj).reduce(function (acc, _ref) {
    var quantity = _ref.quantity,
        price = _ref.price;
    return acc + quantity * price;
  }, 0).toFixed(2);
};

function getCartSaga() {
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function getCartSaga$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_3__["getCartSuccess"])());

        case 3:
          _context.next = 9;
          break;

        case 5:
          _context.prev = 5;
          _context.t0 = _context["catch"](0);
          _context.next = 9;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_3__["getCartError"])(_context.t0));

        case 9:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, null, [[0, 5]]);
}

function addItemSaga(payload) {
  var product, localCart, currentCart, existItem;
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function addItemSaga$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          product = payload.product;
          localCart = JSON.parse(localStorage.getItem("persist:martfury")).cart;
          currentCart = JSON.parse(localCart);
          existItem = currentCart.cartItems.find(function (item) {
            return item.id === product.id;
          });

          if (existItem) {
            existItem.quantity += product.quantity;
          } else {
            if (!product.quantity) {
              product.quantity = 1;
            }

            currentCart.cartItems.push(product);
          }

          currentCart.amount = calculateAmount(currentCart.cartItems);
          currentCart.cartTotal++;
          _context2.next = 10;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_3__["updateCartSuccess"])(currentCart));

        case 10:
          modalSuccess("success");
          _context2.next = 17;
          break;

        case 13:
          _context2.prev = 13;
          _context2.t0 = _context2["catch"](0);
          _context2.next = 17;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_3__["getCartError"])(_context2.t0));

        case 17:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2, null, [[0, 13]]);
}

function removeItemSaga(payload) {
  var product, localCart, index;
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function removeItemSaga$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          product = payload.product;
          localCart = JSON.parse(JSON.parse(localStorage.getItem("persist:martfury")).cart);
          index = localCart.cartItems.findIndex(function (item) {
            return item.id === product.id;
          });
          localCart.cartTotal = localCart.cartTotal - product.quantity;
          localCart.cartItems.splice(index, 1);
          localCart.amount = calculateAmount(localCart.cartItems);

          if (localCart.cartItems.length === 0) {
            localCart.cartItems = [];
            localCart.amount = 0;
            localCart.cartTotal = 0;
          }

          _context3.next = 10;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_3__["updateCartSuccess"])(localCart));

        case 10:
          modalWarning("warning");
          _context3.next = 17;
          break;

        case 13:
          _context3.prev = 13;
          _context3.t0 = _context3["catch"](0);
          _context3.next = 17;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_3__["getCartError"])(_context3.t0));

        case 17:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3, null, [[0, 13]]);
}

function increaseQtySaga(payload) {
  var product, localCart, selectedItem;
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function increaseQtySaga$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.prev = 0;
          product = payload.product;
          localCart = JSON.parse(JSON.parse(localStorage.getItem("persist:martfury")).cart);
          selectedItem = localCart.cartItems.find(function (item) {
            return item.id === product.id;
          });

          if (selectedItem) {
            selectedItem.quantity++;
            localCart.cartTotal++;
            localCart.amount = calculateAmount(localCart.cartItems);
          }

          _context4.next = 7;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_3__["updateCartSuccess"])(localCart));

        case 7:
          _context4.next = 13;
          break;

        case 9:
          _context4.prev = 9;
          _context4.t0 = _context4["catch"](0);
          _context4.next = 13;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_3__["getCartError"])(_context4.t0));

        case 13:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4, null, [[0, 9]]);
}

function decreaseItemQtySaga(payload) {
  var product, localCart, selectedItem;
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function decreaseItemQtySaga$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.prev = 0;
          product = payload.product;
          localCart = JSON.parse(JSON.parse(localStorage.getItem("persist:martfury")).cart);
          selectedItem = localCart.cartItems.find(function (item) {
            return item.id === product.id;
          });

          if (selectedItem) {
            selectedItem.quantity--;
            localCart.cartTotal--;
            localCart.amount = calculateAmount(localCart.cartItems);
          }

          _context5.next = 7;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_3__["updateCartSuccess"])(localCart));

        case 7:
          _context5.next = 13;
          break;

        case 9:
          _context5.prev = 9;
          _context5.t0 = _context5["catch"](0);
          _context5.next = 13;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_3__["getCartError"])(_context5.t0));

        case 13:
        case "end":
          return _context5.stop();
      }
    }
  }, _marked5, null, [[0, 9]]);
}

function clearCartSaga() {
  var emptyCart;
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function clearCartSaga$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.prev = 0;
          emptyCart = {
            cartItems: [],
            amount: 0,
            cartTotal: 0
          };
          _context6.next = 4;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_3__["updateCartSuccess"])(emptyCart));

        case 4:
          _context6.next = 10;
          break;

        case 6:
          _context6.prev = 6;
          _context6.t0 = _context6["catch"](0);
          _context6.next = 10;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_3__["updateCartError"])(_context6.t0));

        case 10:
        case "end":
          return _context6.stop();
      }
    }
  }, _marked6, null, [[0, 6]]);
}

function rootSaga() {
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function rootSaga$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          _context7.next = 2;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["all"])([Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["takeEvery"])(_action__WEBPACK_IMPORTED_MODULE_3__["actionTypes"].GET_CART, getCartSaga)]);

        case 2:
          _context7.next = 4;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["all"])([Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["takeEvery"])(_action__WEBPACK_IMPORTED_MODULE_3__["actionTypes"].ADD_ITEM, addItemSaga)]);

        case 4:
          _context7.next = 6;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["all"])([Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["takeEvery"])(_action__WEBPACK_IMPORTED_MODULE_3__["actionTypes"].REMOVE_ITEM, removeItemSaga)]);

        case 6:
          _context7.next = 8;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["all"])([Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["takeEvery"])(_action__WEBPACK_IMPORTED_MODULE_3__["actionTypes"].INCREASE_QTY, increaseQtySaga)]);

        case 8:
          _context7.next = 10;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["all"])([Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["takeEvery"])(_action__WEBPACK_IMPORTED_MODULE_3__["actionTypes"].DECREASE_QTY, decreaseItemQtySaga)]);

        case 10:
        case "end":
          return _context7.stop();
      }
    }
  }, _marked7);
}

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vc3RvcmUvY2FydC9zYWdhLmpzIl0sIm5hbWVzIjpbImdldENhcnRTYWdhIiwiYWRkSXRlbVNhZ2EiLCJyZW1vdmVJdGVtU2FnYSIsImluY3JlYXNlUXR5U2FnYSIsImRlY3JlYXNlSXRlbVF0eVNhZ2EiLCJjbGVhckNhcnRTYWdhIiwicm9vdFNhZ2EiLCJtb2RhbFN1Y2Nlc3MiLCJ0eXBlIiwibm90aWZpY2F0aW9uIiwibWVzc2FnZSIsImRlc2NyaXB0aW9uIiwiZHVyYXRpb24iLCJtb2RhbFdhcm5pbmciLCJjYWxjdWxhdGVBbW91bnQiLCJvYmoiLCJPYmplY3QiLCJ2YWx1ZXMiLCJyZWR1Y2UiLCJhY2MiLCJxdWFudGl0eSIsInByaWNlIiwidG9GaXhlZCIsInB1dCIsImdldENhcnRTdWNjZXNzIiwiZ2V0Q2FydEVycm9yIiwicGF5bG9hZCIsInByb2R1Y3QiLCJsb2NhbENhcnQiLCJKU09OIiwicGFyc2UiLCJsb2NhbFN0b3JhZ2UiLCJnZXRJdGVtIiwiY2FydCIsImN1cnJlbnRDYXJ0IiwiZXhpc3RJdGVtIiwiY2FydEl0ZW1zIiwiZmluZCIsIml0ZW0iLCJpZCIsInB1c2giLCJhbW91bnQiLCJjYXJ0VG90YWwiLCJ1cGRhdGVDYXJ0U3VjY2VzcyIsImluZGV4IiwiZmluZEluZGV4Iiwic3BsaWNlIiwibGVuZ3RoIiwic2VsZWN0ZWRJdGVtIiwiZW1wdHlDYXJ0IiwidXBkYXRlQ2FydEVycm9yIiwiYWxsIiwidGFrZUV2ZXJ5IiwiYWN0aW9uVHlwZXMiLCJHRVRfQ0FSVCIsIkFERF9JVEVNIiwiUkVNT1ZFX0lURU0iLCJJTkNSRUFTRV9RVFkiLCJERUNSRUFTRV9RVFkiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O3dLQStCVUEsVzt5S0FRQUMsVzt5S0F5QkFDLGM7eUtBc0JBQyxlO3lLQW9CQUMsbUI7eUtBcUJBQyxhO3lLQWFlQyxROztBQTVJekI7QUFDQTtBQUVBOztBQVFBLElBQU1DLFlBQVksR0FBRyxTQUFmQSxZQUFlLENBQUNDLElBQUQsRUFBVTtBQUM3QkMsbURBQVksQ0FBQ0QsSUFBRCxDQUFaLENBQW1CO0FBQ2pCRSxXQUFPLEVBQUUsU0FEUTtBQUVqQkMsZUFBVyxFQUFFLDJDQUZJO0FBR2pCQyxZQUFRLEVBQUU7QUFITyxHQUFuQjtBQUtELENBTkQ7O0FBT0EsSUFBTUMsWUFBWSxHQUFHLFNBQWZBLFlBQWUsQ0FBQ0wsSUFBRCxFQUFVO0FBQzdCQyxtREFBWSxDQUFDRCxJQUFELENBQVosQ0FBbUI7QUFDakJFLFdBQU8sRUFBRSxlQURRO0FBRWpCQyxlQUFXLEVBQUUsK0NBRkk7QUFHakJDLFlBQVEsRUFBRTtBQUhPLEdBQW5CO0FBS0QsQ0FORDs7QUFRTyxJQUFNRSxlQUFlLEdBQUcsU0FBbEJBLGVBQWtCLENBQUNDLEdBQUQ7QUFBQSxTQUM3QkMsTUFBTSxDQUFDQyxNQUFQLENBQWNGLEdBQWQsRUFDR0csTUFESCxDQUNVLFVBQUNDLEdBQUQ7QUFBQSxRQUFRQyxRQUFSLFFBQVFBLFFBQVI7QUFBQSxRQUFrQkMsS0FBbEIsUUFBa0JBLEtBQWxCO0FBQUEsV0FBOEJGLEdBQUcsR0FBR0MsUUFBUSxHQUFHQyxLQUEvQztBQUFBLEdBRFYsRUFDZ0UsQ0FEaEUsRUFFR0MsT0FGSCxDQUVXLENBRlgsQ0FENkI7QUFBQSxDQUF4Qjs7QUFLUCxTQUFVdEIsV0FBVjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVJLGlCQUFNdUIsOERBQUcsQ0FBQ0MsOERBQWMsRUFBZixDQUFUOztBQUZKO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlJLGlCQUFNRCw4REFBRyxDQUFDRSw0REFBWSxhQUFiLENBQVQ7O0FBSko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBUUEsU0FBVXhCLFdBQVYsQ0FBc0J5QixPQUF0QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVZQyxpQkFGWixHQUV3QkQsT0FGeEIsQ0FFWUMsT0FGWjtBQUdVQyxtQkFIVixHQUdzQkMsSUFBSSxDQUFDQyxLQUFMLENBQVdDLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixrQkFBckIsQ0FBWCxFQUFxREMsSUFIM0U7QUFJUUMscUJBSlIsR0FJc0JMLElBQUksQ0FBQ0MsS0FBTCxDQUFXRixTQUFYLENBSnRCO0FBS1FPLG1CQUxSLEdBS29CRCxXQUFXLENBQUNFLFNBQVosQ0FBc0JDLElBQXRCLENBQ2QsVUFBQ0MsSUFBRDtBQUFBLG1CQUFVQSxJQUFJLENBQUNDLEVBQUwsS0FBWVosT0FBTyxDQUFDWSxFQUE5QjtBQUFBLFdBRGMsQ0FMcEI7O0FBUUksY0FBSUosU0FBSixFQUFlO0FBQ2JBLHFCQUFTLENBQUNmLFFBQVYsSUFBc0JPLE9BQU8sQ0FBQ1AsUUFBOUI7QUFDRCxXQUZELE1BRU87QUFDTCxnQkFBSSxDQUFDTyxPQUFPLENBQUNQLFFBQWIsRUFBdUI7QUFDckJPLHFCQUFPLENBQUNQLFFBQVIsR0FBbUIsQ0FBbkI7QUFDRDs7QUFDRGMsdUJBQVcsQ0FBQ0UsU0FBWixDQUFzQkksSUFBdEIsQ0FBMkJiLE9BQTNCO0FBQ0Q7O0FBQ0RPLHFCQUFXLENBQUNPLE1BQVosR0FBcUIzQixlQUFlLENBQUNvQixXQUFXLENBQUNFLFNBQWIsQ0FBcEM7QUFDQUYscUJBQVcsQ0FBQ1EsU0FBWjtBQWpCSjtBQWtCSSxpQkFBTW5CLDhEQUFHLENBQUNvQixpRUFBaUIsQ0FBQ1QsV0FBRCxDQUFsQixDQUFUOztBQWxCSjtBQW1CSTNCLHNCQUFZLENBQUMsU0FBRCxDQUFaO0FBbkJKO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFxQkksaUJBQU1nQiw4REFBRyxDQUFDRSw0REFBWSxjQUFiLENBQVQ7O0FBckJKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQXlCQSxTQUFVdkIsY0FBVixDQUF5QndCLE9BQXpCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRVlDLGlCQUZaLEdBRXdCRCxPQUZ4QixDQUVZQyxPQUZaO0FBR1FDLG1CQUhSLEdBR29CQyxJQUFJLENBQUNDLEtBQUwsQ0FDZEQsSUFBSSxDQUFDQyxLQUFMLENBQVdDLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixrQkFBckIsQ0FBWCxFQUFxREMsSUFEdkMsQ0FIcEI7QUFNUVcsZUFOUixHQU1nQmhCLFNBQVMsQ0FBQ1EsU0FBVixDQUFvQlMsU0FBcEIsQ0FBOEIsVUFBQ1AsSUFBRDtBQUFBLG1CQUFVQSxJQUFJLENBQUNDLEVBQUwsS0FBWVosT0FBTyxDQUFDWSxFQUE5QjtBQUFBLFdBQTlCLENBTmhCO0FBT0lYLG1CQUFTLENBQUNjLFNBQVYsR0FBc0JkLFNBQVMsQ0FBQ2MsU0FBVixHQUFzQmYsT0FBTyxDQUFDUCxRQUFwRDtBQUNBUSxtQkFBUyxDQUFDUSxTQUFWLENBQW9CVSxNQUFwQixDQUEyQkYsS0FBM0IsRUFBa0MsQ0FBbEM7QUFDQWhCLG1CQUFTLENBQUNhLE1BQVYsR0FBbUIzQixlQUFlLENBQUNjLFNBQVMsQ0FBQ1EsU0FBWCxDQUFsQzs7QUFDQSxjQUFJUixTQUFTLENBQUNRLFNBQVYsQ0FBb0JXLE1BQXBCLEtBQStCLENBQW5DLEVBQXNDO0FBQ3BDbkIscUJBQVMsQ0FBQ1EsU0FBVixHQUFzQixFQUF0QjtBQUNBUixxQkFBUyxDQUFDYSxNQUFWLEdBQW1CLENBQW5CO0FBQ0FiLHFCQUFTLENBQUNjLFNBQVYsR0FBc0IsQ0FBdEI7QUFDRDs7QUFkTDtBQWVJLGlCQUFNbkIsOERBQUcsQ0FBQ29CLGlFQUFpQixDQUFDZixTQUFELENBQWxCLENBQVQ7O0FBZko7QUFnQklmLHNCQUFZLENBQUMsU0FBRCxDQUFaO0FBaEJKO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFrQkksaUJBQU1VLDhEQUFHLENBQUNFLDREQUFZLGNBQWIsQ0FBVDs7QUFsQko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBc0JBLFNBQVV0QixlQUFWLENBQTBCdUIsT0FBMUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFWUMsaUJBRlosR0FFd0JELE9BRnhCLENBRVlDLE9BRlo7QUFHUUMsbUJBSFIsR0FHb0JDLElBQUksQ0FBQ0MsS0FBTCxDQUNkRCxJQUFJLENBQUNDLEtBQUwsQ0FBV0MsWUFBWSxDQUFDQyxPQUFiLENBQXFCLGtCQUFyQixDQUFYLEVBQXFEQyxJQUR2QyxDQUhwQjtBQU1RZSxzQkFOUixHQU11QnBCLFNBQVMsQ0FBQ1EsU0FBVixDQUFvQkMsSUFBcEIsQ0FDakIsVUFBQ0MsSUFBRDtBQUFBLG1CQUFVQSxJQUFJLENBQUNDLEVBQUwsS0FBWVosT0FBTyxDQUFDWSxFQUE5QjtBQUFBLFdBRGlCLENBTnZCOztBQVNJLGNBQUlTLFlBQUosRUFBa0I7QUFDaEJBLHdCQUFZLENBQUM1QixRQUFiO0FBQ0FRLHFCQUFTLENBQUNjLFNBQVY7QUFDQWQscUJBQVMsQ0FBQ2EsTUFBVixHQUFtQjNCLGVBQWUsQ0FBQ2MsU0FBUyxDQUFDUSxTQUFYLENBQWxDO0FBQ0Q7O0FBYkw7QUFjSSxpQkFBTWIsOERBQUcsQ0FBQ29CLGlFQUFpQixDQUFDZixTQUFELENBQWxCLENBQVQ7O0FBZEo7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZ0JJLGlCQUFNTCw4REFBRyxDQUFDRSw0REFBWSxjQUFiLENBQVQ7O0FBaEJKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQW9CQSxTQUFVckIsbUJBQVYsQ0FBOEJzQixPQUE5QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVZQyxpQkFGWixHQUV3QkQsT0FGeEIsQ0FFWUMsT0FGWjtBQUdVQyxtQkFIVixHQUdzQkMsSUFBSSxDQUFDQyxLQUFMLENBQ2hCRCxJQUFJLENBQUNDLEtBQUwsQ0FBV0MsWUFBWSxDQUFDQyxPQUFiLENBQXFCLGtCQUFyQixDQUFYLEVBQXFEQyxJQURyQyxDQUh0QjtBQU1RZSxzQkFOUixHQU11QnBCLFNBQVMsQ0FBQ1EsU0FBVixDQUFvQkMsSUFBcEIsQ0FDakIsVUFBQ0MsSUFBRDtBQUFBLG1CQUFVQSxJQUFJLENBQUNDLEVBQUwsS0FBWVosT0FBTyxDQUFDWSxFQUE5QjtBQUFBLFdBRGlCLENBTnZCOztBQVVJLGNBQUlTLFlBQUosRUFBa0I7QUFDaEJBLHdCQUFZLENBQUM1QixRQUFiO0FBQ0FRLHFCQUFTLENBQUNjLFNBQVY7QUFDQWQscUJBQVMsQ0FBQ2EsTUFBVixHQUFtQjNCLGVBQWUsQ0FBQ2MsU0FBUyxDQUFDUSxTQUFYLENBQWxDO0FBQ0Q7O0FBZEw7QUFlSSxpQkFBTWIsOERBQUcsQ0FBQ29CLGlFQUFpQixDQUFDZixTQUFELENBQWxCLENBQVQ7O0FBZko7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBaUJJLGlCQUFNTCw4REFBRyxDQUFDRSw0REFBWSxjQUFiLENBQVQ7O0FBakJKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQXFCQSxTQUFVcEIsYUFBVjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVVNEMsbUJBRlYsR0FFc0I7QUFDaEJiLHFCQUFTLEVBQUUsRUFESztBQUVoQkssa0JBQU0sRUFBRSxDQUZRO0FBR2hCQyxxQkFBUyxFQUFFO0FBSEssV0FGdEI7QUFBQTtBQU9JLGlCQUFNbkIsOERBQUcsQ0FBQ29CLGlFQUFpQixDQUFDTSxTQUFELENBQWxCLENBQVQ7O0FBUEo7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0ksaUJBQU0xQiw4REFBRyxDQUFDMkIsK0RBQWUsY0FBaEIsQ0FBVDs7QUFUSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFhZSxTQUFVNUMsUUFBVjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDYixpQkFBTTZDLDhEQUFHLENBQUMsQ0FBQ0Msb0VBQVMsQ0FBQ0MsbURBQVcsQ0FBQ0MsUUFBYixFQUF1QnRELFdBQXZCLENBQVYsQ0FBRCxDQUFUOztBQURhO0FBQUE7QUFFYixpQkFBTW1ELDhEQUFHLENBQUMsQ0FBQ0Msb0VBQVMsQ0FBQ0MsbURBQVcsQ0FBQ0UsUUFBYixFQUF1QnRELFdBQXZCLENBQVYsQ0FBRCxDQUFUOztBQUZhO0FBQUE7QUFHYixpQkFBTWtELDhEQUFHLENBQUMsQ0FBQ0Msb0VBQVMsQ0FBQ0MsbURBQVcsQ0FBQ0csV0FBYixFQUEwQnRELGNBQTFCLENBQVYsQ0FBRCxDQUFUOztBQUhhO0FBQUE7QUFJYixpQkFBTWlELDhEQUFHLENBQUMsQ0FBQ0Msb0VBQVMsQ0FBQ0MsbURBQVcsQ0FBQ0ksWUFBYixFQUEyQnRELGVBQTNCLENBQVYsQ0FBRCxDQUFUOztBQUphO0FBQUE7QUFLYixpQkFBTWdELDhEQUFHLENBQUMsQ0FBQ0Msb0VBQVMsQ0FBQ0MsbURBQVcsQ0FBQ0ssWUFBYixFQUEyQnRELG1CQUEzQixDQUFWLENBQUQsQ0FBVDs7QUFMYTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9fYXBwLjlhNTZlZTExNzY3NWJiNGIzMGI4LmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBhbGwsIHB1dCwgdGFrZUV2ZXJ5IH0gZnJvbSBcInJlZHV4LXNhZ2EvZWZmZWN0c1wiO1xyXG5pbXBvcnQgeyBub3RpZmljYXRpb24gfSBmcm9tIFwiYW50ZFwiO1xyXG5cclxuaW1wb3J0IHtcclxuICBhY3Rpb25UeXBlcyxcclxuICBnZXRDYXJ0RXJyb3IsXHJcbiAgZ2V0Q2FydFN1Y2Nlc3MsXHJcbiAgdXBkYXRlQ2FydFN1Y2Nlc3MsXHJcbiAgdXBkYXRlQ2FydEVycm9yLFxyXG59IGZyb20gXCIuL2FjdGlvblwiO1xyXG5cclxuY29uc3QgbW9kYWxTdWNjZXNzID0gKHR5cGUpID0+IHtcclxuICBub3RpZmljYXRpb25bdHlwZV0oe1xyXG4gICAgbWVzc2FnZTogXCJTdWNjZXNzXCIsXHJcbiAgICBkZXNjcmlwdGlvbjogXCJUaGlzIHByb2R1Y3QgaGFzIGJlZW4gYWRkZWQgdG8geW91ciBjYXJ0IVwiLFxyXG4gICAgZHVyYXRpb246IDEsXHJcbiAgfSk7XHJcbn07XHJcbmNvbnN0IG1vZGFsV2FybmluZyA9ICh0eXBlKSA9PiB7XHJcbiAgbm90aWZpY2F0aW9uW3R5cGVdKHtcclxuICAgIG1lc3NhZ2U6IFwiUmVtb3ZlIEEgSXRlbVwiLFxyXG4gICAgZGVzY3JpcHRpb246IFwiVGhpcyBwcm9kdWN0IGhhcyBiZWVuIHJlbW92ZWQgZnJvbSB5b3VyIGNhcnQhXCIsXHJcbiAgICBkdXJhdGlvbjogMSxcclxuICB9KTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBjYWxjdWxhdGVBbW91bnQgPSAob2JqKSA9PlxyXG4gIE9iamVjdC52YWx1ZXMob2JqKVxyXG4gICAgLnJlZHVjZSgoYWNjLCB7IHF1YW50aXR5LCBwcmljZSB9KSA9PiBhY2MgKyBxdWFudGl0eSAqIHByaWNlLCAwKVxyXG4gICAgLnRvRml4ZWQoMik7XHJcblxyXG5mdW5jdGlvbiogZ2V0Q2FydFNhZ2EoKSB7XHJcbiAgdHJ5IHtcclxuICAgIHlpZWxkIHB1dChnZXRDYXJ0U3VjY2VzcygpKTtcclxuICB9IGNhdGNoIChlcnIpIHtcclxuICAgIHlpZWxkIHB1dChnZXRDYXJ0RXJyb3IoZXJyKSk7XHJcbiAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiogYWRkSXRlbVNhZ2EocGF5bG9hZCkge1xyXG4gIHRyeSB7XHJcbiAgICBjb25zdCB7IHByb2R1Y3QgfSA9IHBheWxvYWQ7XHJcbiAgICBjb25zdCBsb2NhbENhcnQgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwicGVyc2lzdDptYXJ0ZnVyeVwiKSkuY2FydDtcclxuICAgIGxldCBjdXJyZW50Q2FydCA9IEpTT04ucGFyc2UobG9jYWxDYXJ0KTtcclxuICAgIGxldCBleGlzdEl0ZW0gPSBjdXJyZW50Q2FydC5jYXJ0SXRlbXMuZmluZChcclxuICAgICAgKGl0ZW0pID0+IGl0ZW0uaWQgPT09IHByb2R1Y3QuaWRcclxuICAgICk7XHJcbiAgICBpZiAoZXhpc3RJdGVtKSB7XHJcbiAgICAgIGV4aXN0SXRlbS5xdWFudGl0eSArPSBwcm9kdWN0LnF1YW50aXR5O1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgaWYgKCFwcm9kdWN0LnF1YW50aXR5KSB7XHJcbiAgICAgICAgcHJvZHVjdC5xdWFudGl0eSA9IDE7XHJcbiAgICAgIH1cclxuICAgICAgY3VycmVudENhcnQuY2FydEl0ZW1zLnB1c2gocHJvZHVjdCk7XHJcbiAgICB9XHJcbiAgICBjdXJyZW50Q2FydC5hbW91bnQgPSBjYWxjdWxhdGVBbW91bnQoY3VycmVudENhcnQuY2FydEl0ZW1zKTtcclxuICAgIGN1cnJlbnRDYXJ0LmNhcnRUb3RhbCsrO1xyXG4gICAgeWllbGQgcHV0KHVwZGF0ZUNhcnRTdWNjZXNzKGN1cnJlbnRDYXJ0KSk7XHJcbiAgICBtb2RhbFN1Y2Nlc3MoXCJzdWNjZXNzXCIpO1xyXG4gIH0gY2F0Y2ggKGVycikge1xyXG4gICAgeWllbGQgcHV0KGdldENhcnRFcnJvcihlcnIpKTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uKiByZW1vdmVJdGVtU2FnYShwYXlsb2FkKSB7XHJcbiAgdHJ5IHtcclxuICAgIGNvbnN0IHsgcHJvZHVjdCB9ID0gcGF5bG9hZDtcclxuICAgIGxldCBsb2NhbENhcnQgPSBKU09OLnBhcnNlKFxyXG4gICAgICBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwicGVyc2lzdDptYXJ0ZnVyeVwiKSkuY2FydFxyXG4gICAgKTtcclxuICAgIGxldCBpbmRleCA9IGxvY2FsQ2FydC5jYXJ0SXRlbXMuZmluZEluZGV4KChpdGVtKSA9PiBpdGVtLmlkID09PSBwcm9kdWN0LmlkKTtcclxuICAgIGxvY2FsQ2FydC5jYXJ0VG90YWwgPSBsb2NhbENhcnQuY2FydFRvdGFsIC0gcHJvZHVjdC5xdWFudGl0eTtcclxuICAgIGxvY2FsQ2FydC5jYXJ0SXRlbXMuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgIGxvY2FsQ2FydC5hbW91bnQgPSBjYWxjdWxhdGVBbW91bnQobG9jYWxDYXJ0LmNhcnRJdGVtcyk7XHJcbiAgICBpZiAobG9jYWxDYXJ0LmNhcnRJdGVtcy5sZW5ndGggPT09IDApIHtcclxuICAgICAgbG9jYWxDYXJ0LmNhcnRJdGVtcyA9IFtdO1xyXG4gICAgICBsb2NhbENhcnQuYW1vdW50ID0gMDtcclxuICAgICAgbG9jYWxDYXJ0LmNhcnRUb3RhbCA9IDA7XHJcbiAgICB9XHJcbiAgICB5aWVsZCBwdXQodXBkYXRlQ2FydFN1Y2Nlc3MobG9jYWxDYXJ0KSk7XHJcbiAgICBtb2RhbFdhcm5pbmcoXCJ3YXJuaW5nXCIpO1xyXG4gIH0gY2F0Y2ggKGVycikge1xyXG4gICAgeWllbGQgcHV0KGdldENhcnRFcnJvcihlcnIpKTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uKiBpbmNyZWFzZVF0eVNhZ2EocGF5bG9hZCkge1xyXG4gIHRyeSB7XHJcbiAgICBjb25zdCB7IHByb2R1Y3QgfSA9IHBheWxvYWQ7XHJcbiAgICBsZXQgbG9jYWxDYXJ0ID0gSlNPTi5wYXJzZShcclxuICAgICAgSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcInBlcnNpc3Q6bWFydGZ1cnlcIikpLmNhcnRcclxuICAgICk7XHJcbiAgICBsZXQgc2VsZWN0ZWRJdGVtID0gbG9jYWxDYXJ0LmNhcnRJdGVtcy5maW5kKFxyXG4gICAgICAoaXRlbSkgPT4gaXRlbS5pZCA9PT0gcHJvZHVjdC5pZFxyXG4gICAgKTtcclxuICAgIGlmIChzZWxlY3RlZEl0ZW0pIHtcclxuICAgICAgc2VsZWN0ZWRJdGVtLnF1YW50aXR5Kys7XHJcbiAgICAgIGxvY2FsQ2FydC5jYXJ0VG90YWwrKztcclxuICAgICAgbG9jYWxDYXJ0LmFtb3VudCA9IGNhbGN1bGF0ZUFtb3VudChsb2NhbENhcnQuY2FydEl0ZW1zKTtcclxuICAgIH1cclxuICAgIHlpZWxkIHB1dCh1cGRhdGVDYXJ0U3VjY2Vzcyhsb2NhbENhcnQpKTtcclxuICB9IGNhdGNoIChlcnIpIHtcclxuICAgIHlpZWxkIHB1dChnZXRDYXJ0RXJyb3IoZXJyKSk7XHJcbiAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiogZGVjcmVhc2VJdGVtUXR5U2FnYShwYXlsb2FkKSB7XHJcbiAgdHJ5IHtcclxuICAgIGNvbnN0IHsgcHJvZHVjdCB9ID0gcGF5bG9hZDtcclxuICAgIGNvbnN0IGxvY2FsQ2FydCA9IEpTT04ucGFyc2UoXHJcbiAgICAgIEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJwZXJzaXN0Om1hcnRmdXJ5XCIpKS5jYXJ0XHJcbiAgICApO1xyXG4gICAgbGV0IHNlbGVjdGVkSXRlbSA9IGxvY2FsQ2FydC5jYXJ0SXRlbXMuZmluZChcclxuICAgICAgKGl0ZW0pID0+IGl0ZW0uaWQgPT09IHByb2R1Y3QuaWRcclxuICAgICk7XHJcblxyXG4gICAgaWYgKHNlbGVjdGVkSXRlbSkge1xyXG4gICAgICBzZWxlY3RlZEl0ZW0ucXVhbnRpdHktLTtcclxuICAgICAgbG9jYWxDYXJ0LmNhcnRUb3RhbC0tO1xyXG4gICAgICBsb2NhbENhcnQuYW1vdW50ID0gY2FsY3VsYXRlQW1vdW50KGxvY2FsQ2FydC5jYXJ0SXRlbXMpO1xyXG4gICAgfVxyXG4gICAgeWllbGQgcHV0KHVwZGF0ZUNhcnRTdWNjZXNzKGxvY2FsQ2FydCkpO1xyXG4gIH0gY2F0Y2ggKGVycikge1xyXG4gICAgeWllbGQgcHV0KGdldENhcnRFcnJvcihlcnIpKTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uKiBjbGVhckNhcnRTYWdhKCkge1xyXG4gIHRyeSB7XHJcbiAgICBjb25zdCBlbXB0eUNhcnQgPSB7XHJcbiAgICAgIGNhcnRJdGVtczogW10sXHJcbiAgICAgIGFtb3VudDogMCxcclxuICAgICAgY2FydFRvdGFsOiAwLFxyXG4gICAgfTtcclxuICAgIHlpZWxkIHB1dCh1cGRhdGVDYXJ0U3VjY2VzcyhlbXB0eUNhcnQpKTtcclxuICB9IGNhdGNoIChlcnIpIHtcclxuICAgIHlpZWxkIHB1dCh1cGRhdGVDYXJ0RXJyb3IoZXJyKSk7XHJcbiAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiogcm9vdFNhZ2EoKSB7XHJcbiAgeWllbGQgYWxsKFt0YWtlRXZlcnkoYWN0aW9uVHlwZXMuR0VUX0NBUlQsIGdldENhcnRTYWdhKV0pO1xyXG4gIHlpZWxkIGFsbChbdGFrZUV2ZXJ5KGFjdGlvblR5cGVzLkFERF9JVEVNLCBhZGRJdGVtU2FnYSldKTtcclxuICB5aWVsZCBhbGwoW3Rha2VFdmVyeShhY3Rpb25UeXBlcy5SRU1PVkVfSVRFTSwgcmVtb3ZlSXRlbVNhZ2EpXSk7XHJcbiAgeWllbGQgYWxsKFt0YWtlRXZlcnkoYWN0aW9uVHlwZXMuSU5DUkVBU0VfUVRZLCBpbmNyZWFzZVF0eVNhZ2EpXSk7XHJcbiAgeWllbGQgYWxsKFt0YWtlRXZlcnkoYWN0aW9uVHlwZXMuREVDUkVBU0VfUVRZLCBkZWNyZWFzZUl0ZW1RdHlTYWdhKV0pO1xyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiIn0=