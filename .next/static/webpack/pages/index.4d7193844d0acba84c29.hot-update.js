webpackHotUpdate_N_E("pages/index",{

/***/ "./components/elements/products/modules/ModuleProductActions.js":
/*!**********************************************************************!*\
  !*** ./components/elements/products/modules/ModuleProductActions.js ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _store_cart_action__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ~/store/cart/action */ "./store/cart/action.js");
/* harmony import */ var _store_compare_action__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ~/store/compare/action */ "./store/compare/action.js");
/* harmony import */ var _store_wishlist_action__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ~/store/wishlist/action */ "./store/wishlist/action.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _components_elements_detail_ProductDetailQuickView__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ~/components/elements/detail/ProductDetailQuickView */ "./components/elements/detail/ProductDetailQuickView.jsx");


var _jsxFileName = "D:\\ReactJS\\projects\\nextjs\\web3-shopping\\components\\elements\\products\\modules\\ModuleProductActions.js",
    _this = undefined,
    _s = $RefreshSig$();









var ModuleProductActions = function ModuleProductActions(_ref) {
  _s();

  var product = _ref.product;
  var dispatch = Object(react_redux__WEBPACK_IMPORTED_MODULE_6__["useDispatch"])();
  var cartItems = Object(react_redux__WEBPACK_IMPORTED_MODULE_6__["useSelector"])(function (state) {
    return state.cart.cartItems;
  });

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      isQuickView = _useState[0],
      setIsQuickView = _useState[1];

  var handleAddItemToCart = function handleAddItemToCart(e) {
    e.preventDefault();
    dispatch(Object(_store_cart_action__WEBPACK_IMPORTED_MODULE_3__["addItem"])(product));
  };

  var handleAddItemToCompare = function handleAddItemToCompare(e) {
    e.preventDefault();
    dispatch(Object(_store_compare_action__WEBPACK_IMPORTED_MODULE_4__["addItemToCompare"])(product));
  };

  var handleAddItemToWishlist = function handleAddItemToWishlist(e) {
    dispatch(Object(_store_wishlist_action__WEBPACK_IMPORTED_MODULE_5__["addItemToWishlist"])(product));
  };

  var handleShowQuickView = function handleShowQuickView(e) {
    e.preventDefault();
    setIsQuickView(true);
  };

  var handleHideQuickView = function handleHideQuickView(e) {
    e.preventDefault();
    setIsQuickView(false);
  };

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
    className: "ps-product__actions",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
        href: "#",
        "data-toggle": "tooltip",
        "data-placement": "top",
        title: "Quick View",
        onClick: handleShowQuickView,
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
          className: "icon-eye"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 57,
          columnNumber: 11
        }, _this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 50,
        columnNumber: 9
      }, _this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 49,
      columnNumber: 7
    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
        href: "#",
        "data-toggle": "tooltip",
        "data-placement": "top",
        title: "Add to wishlist",
        onClick: handleAddItemToWishlist,
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
          className: "icon-heart"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 68,
          columnNumber: 11
        }, _this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 61,
        columnNumber: 9
      }, _this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 60,
      columnNumber: 7
    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
        href: "#",
        "data-toggle": "tooltip",
        "data-placement": "top",
        title: "Compare",
        onClick: handleAddItemToCompare,
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
          className: "icon-chart-bars"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 79,
          columnNumber: 11
        }, _this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 72,
        columnNumber: 9
      }, _this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 71,
      columnNumber: 7
    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Modal"], {
      centered: true,
      footer: null,
      width: 1024,
      onCancel: function onCancel(e) {
        return handleHideQuickView(e);
      },
      visible: isQuickView,
      closeIcon: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
        className: "icon icon-cross2"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 88,
        columnNumber: 20
      }, _this),
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h3", {
        children: "Quickview"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 90,
        columnNumber: 9
      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_elements_detail_ProductDetailQuickView__WEBPACK_IMPORTED_MODULE_7__["default"], {
        product: product
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 91,
        columnNumber: 9
      }, _this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 82,
      columnNumber: 7
    }, _this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 38,
    columnNumber: 5
  }, _this);
};

_s(ModuleProductActions, "l+QyFN/TrP2EwcXvhtJqzL38ilw=", false, function () {
  return [react_redux__WEBPACK_IMPORTED_MODULE_6__["useDispatch"], react_redux__WEBPACK_IMPORTED_MODULE_6__["useSelector"]];
});

_c = ModuleProductActions;
/* harmony default export */ __webpack_exports__["default"] = (ModuleProductActions);

var _c;

$RefreshReg$(_c, "ModuleProductActions");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9lbGVtZW50cy9wcm9kdWN0cy9tb2R1bGVzL01vZHVsZVByb2R1Y3RBY3Rpb25zLmpzIl0sIm5hbWVzIjpbIk1vZHVsZVByb2R1Y3RBY3Rpb25zIiwicHJvZHVjdCIsImRpc3BhdGNoIiwidXNlRGlzcGF0Y2giLCJjYXJ0SXRlbXMiLCJ1c2VTZWxlY3RvciIsInN0YXRlIiwiY2FydCIsInVzZVN0YXRlIiwiaXNRdWlja1ZpZXciLCJzZXRJc1F1aWNrVmlldyIsImhhbmRsZUFkZEl0ZW1Ub0NhcnQiLCJlIiwicHJldmVudERlZmF1bHQiLCJhZGRJdGVtIiwiaGFuZGxlQWRkSXRlbVRvQ29tcGFyZSIsImFkZEl0ZW1Ub0NvbXBhcmUiLCJoYW5kbGVBZGRJdGVtVG9XaXNobGlzdCIsImFkZEl0ZW1Ub1dpc2hsaXN0IiwiaGFuZGxlU2hvd1F1aWNrVmlldyIsImhhbmRsZUhpZGVRdWlja1ZpZXciXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLElBQU1BLG9CQUFvQixHQUFHLFNBQXZCQSxvQkFBdUIsT0FBaUI7QUFBQTs7QUFBQSxNQUFkQyxPQUFjLFFBQWRBLE9BQWM7QUFDNUMsTUFBTUMsUUFBUSxHQUFHQywrREFBVyxFQUE1QjtBQUNBLE1BQU1DLFNBQVMsR0FBR0MsK0RBQVcsQ0FBQyxVQUFDQyxLQUFEO0FBQUEsV0FBV0EsS0FBSyxDQUFDQyxJQUFOLENBQVdILFNBQXRCO0FBQUEsR0FBRCxDQUE3Qjs7QUFGNEMsa0JBR05JLHNEQUFRLENBQUMsS0FBRCxDQUhGO0FBQUEsTUFHckNDLFdBSHFDO0FBQUEsTUFHeEJDLGNBSHdCOztBQUs1QyxNQUFNQyxtQkFBbUIsR0FBRyxTQUF0QkEsbUJBQXNCLENBQUNDLENBQUQsRUFBTztBQUNqQ0EsS0FBQyxDQUFDQyxjQUFGO0FBQ0FYLFlBQVEsQ0FBQ1ksa0VBQU8sQ0FBQ2IsT0FBRCxDQUFSLENBQVI7QUFDRCxHQUhEOztBQUtBLE1BQU1jLHNCQUFzQixHQUFHLFNBQXpCQSxzQkFBeUIsQ0FBQ0gsQ0FBRCxFQUFPO0FBQ3BDQSxLQUFDLENBQUNDLGNBQUY7QUFDQVgsWUFBUSxDQUFDYyw4RUFBZ0IsQ0FBQ2YsT0FBRCxDQUFqQixDQUFSO0FBQ0QsR0FIRDs7QUFLQSxNQUFNZ0IsdUJBQXVCLEdBQUcsU0FBMUJBLHVCQUEwQixDQUFDTCxDQUFELEVBQU87QUFDckNWLFlBQVEsQ0FBQ2dCLGdGQUFpQixDQUFDakIsT0FBRCxDQUFsQixDQUFSO0FBQ0QsR0FGRDs7QUFJQSxNQUFNa0IsbUJBQW1CLEdBQUcsU0FBdEJBLG1CQUFzQixDQUFDUCxDQUFELEVBQU87QUFDakNBLEtBQUMsQ0FBQ0MsY0FBRjtBQUNBSCxrQkFBYyxDQUFDLElBQUQsQ0FBZDtBQUNELEdBSEQ7O0FBS0EsTUFBTVUsbUJBQW1CLEdBQUcsU0FBdEJBLG1CQUFzQixDQUFDUixDQUFELEVBQU87QUFDakNBLEtBQUMsQ0FBQ0MsY0FBRjtBQUNBSCxrQkFBYyxDQUFDLEtBQUQsQ0FBZDtBQUNELEdBSEQ7O0FBSUEsc0JBQ0U7QUFBSSxhQUFTLEVBQUMscUJBQWQ7QUFBQSw0QkFXRTtBQUFBLDZCQUNFO0FBQ0UsWUFBSSxFQUFDLEdBRFA7QUFFRSx1QkFBWSxTQUZkO0FBR0UsMEJBQWUsS0FIakI7QUFJRSxhQUFLLEVBQUMsWUFKUjtBQUtFLGVBQU8sRUFBRVMsbUJBTFg7QUFBQSwrQkFPRTtBQUFHLG1CQUFTLEVBQUM7QUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFYRixlQXNCRTtBQUFBLDZCQUNFO0FBQ0UsWUFBSSxFQUFDLEdBRFA7QUFFRSx1QkFBWSxTQUZkO0FBR0UsMEJBQWUsS0FIakI7QUFJRSxhQUFLLEVBQUMsaUJBSlI7QUFLRSxlQUFPLEVBQUVGLHVCQUxYO0FBQUEsK0JBT0U7QUFBRyxtQkFBUyxFQUFDO0FBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVBGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGFBdEJGLGVBaUNFO0FBQUEsNkJBQ0U7QUFDRSxZQUFJLEVBQUMsR0FEUDtBQUVFLHVCQUFZLFNBRmQ7QUFHRSwwQkFBZSxLQUhqQjtBQUlFLGFBQUssRUFBQyxTQUpSO0FBS0UsZUFBTyxFQUFFRixzQkFMWDtBQUFBLCtCQU9FO0FBQUcsbUJBQVMsRUFBQztBQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFQRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxhQWpDRixlQTRDRSxxRUFBQywwQ0FBRDtBQUNFLGNBQVEsTUFEVjtBQUVFLFlBQU0sRUFBRSxJQUZWO0FBR0UsV0FBSyxFQUFFLElBSFQ7QUFJRSxjQUFRLEVBQUUsa0JBQUNILENBQUQ7QUFBQSxlQUFPUSxtQkFBbUIsQ0FBQ1IsQ0FBRCxDQUExQjtBQUFBLE9BSlo7QUFLRSxhQUFPLEVBQUVILFdBTFg7QUFNRSxlQUFTLGVBQUU7QUFBRyxpQkFBUyxFQUFDO0FBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQU5iO0FBQUEsOEJBUUU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFSRixlQVNFLHFFQUFDLDBGQUFEO0FBQXdCLGVBQU8sRUFBRVI7QUFBakM7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQVRGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxhQTVDRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsV0FERjtBQTBERCxDQXRGRDs7R0FBTUQsb0I7VUFDYUcsdUQsRUFDQ0UsdUQ7OztLQUZkTCxvQjtBQXdGU0EsbUZBQWYiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvaW5kZXguNGQ3MTkzODQ0ZDBhY2JhODRjMjkuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyB1c2VTdGF0ZSB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgeyBNb2RhbCB9IGZyb20gXCJhbnRkXCI7XHJcbmltcG9ydCB7IGFkZEl0ZW0gfSBmcm9tIFwifi9zdG9yZS9jYXJ0L2FjdGlvblwiO1xyXG5pbXBvcnQgeyBhZGRJdGVtVG9Db21wYXJlIH0gZnJvbSBcIn4vc3RvcmUvY29tcGFyZS9hY3Rpb25cIjtcclxuaW1wb3J0IHsgYWRkSXRlbVRvV2lzaGxpc3QgfSBmcm9tIFwifi9zdG9yZS93aXNobGlzdC9hY3Rpb25cIjtcclxuaW1wb3J0IHsgdXNlRGlzcGF0Y2gsIHVzZVNlbGVjdG9yIH0gZnJvbSBcInJlYWN0LXJlZHV4XCI7XHJcbmltcG9ydCBQcm9kdWN0RGV0YWlsUXVpY2tWaWV3IGZyb20gXCJ+L2NvbXBvbmVudHMvZWxlbWVudHMvZGV0YWlsL1Byb2R1Y3REZXRhaWxRdWlja1ZpZXdcIjtcclxuXHJcbmNvbnN0IE1vZHVsZVByb2R1Y3RBY3Rpb25zID0gKHsgcHJvZHVjdCB9KSA9PiB7XHJcbiAgY29uc3QgZGlzcGF0Y2ggPSB1c2VEaXNwYXRjaCgpO1xyXG4gIGNvbnN0IGNhcnRJdGVtcyA9IHVzZVNlbGVjdG9yKChzdGF0ZSkgPT4gc3RhdGUuY2FydC5jYXJ0SXRlbXMpO1xyXG4gIGNvbnN0IFtpc1F1aWNrVmlldywgc2V0SXNRdWlja1ZpZXddID0gdXNlU3RhdGUoZmFsc2UpO1xyXG5cclxuICBjb25zdCBoYW5kbGVBZGRJdGVtVG9DYXJ0ID0gKGUpID0+IHtcclxuICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgIGRpc3BhdGNoKGFkZEl0ZW0ocHJvZHVjdCkpO1xyXG4gIH07XHJcblxyXG4gIGNvbnN0IGhhbmRsZUFkZEl0ZW1Ub0NvbXBhcmUgPSAoZSkgPT4ge1xyXG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgZGlzcGF0Y2goYWRkSXRlbVRvQ29tcGFyZShwcm9kdWN0KSk7XHJcbiAgfTtcclxuXHJcbiAgY29uc3QgaGFuZGxlQWRkSXRlbVRvV2lzaGxpc3QgPSAoZSkgPT4ge1xyXG4gICAgZGlzcGF0Y2goYWRkSXRlbVRvV2lzaGxpc3QocHJvZHVjdCkpO1xyXG4gIH07XHJcblxyXG4gIGNvbnN0IGhhbmRsZVNob3dRdWlja1ZpZXcgPSAoZSkgPT4ge1xyXG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgc2V0SXNRdWlja1ZpZXcodHJ1ZSk7XHJcbiAgfTtcclxuXHJcbiAgY29uc3QgaGFuZGxlSGlkZVF1aWNrVmlldyA9IChlKSA9PiB7XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICBzZXRJc1F1aWNrVmlldyhmYWxzZSk7XHJcbiAgfTtcclxuICByZXR1cm4gKFxyXG4gICAgPHVsIGNsYXNzTmFtZT1cInBzLXByb2R1Y3RfX2FjdGlvbnNcIj5cclxuICAgICAgey8qIHtjYXJ0SXRlbXM/Lmxlbmd0aCA8IDEgJiY8bGk+XHJcbiAgICAgICAgICAgICAgICA8YVxyXG4gICAgICAgICAgICAgICAgICAgIGhyZWY9XCIjXCJcclxuICAgICAgICAgICAgICAgICAgICBkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIlxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGEtcGxhY2VtZW50PVwidG9wXCJcclxuICAgICAgICAgICAgICAgICAgICB0aXRsZT1cIkFkZCBUbyBDYXJ0XCJcclxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXtoYW5kbGVBZGRJdGVtVG9DYXJ0fT5cclxuICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJpY29uLWJhZzJcIj48L2k+XHJcbiAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgIDwvbGk+fSAqL31cclxuICAgICAgPGxpPlxyXG4gICAgICAgIDxhXHJcbiAgICAgICAgICBocmVmPVwiI1wiXHJcbiAgICAgICAgICBkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIlxyXG4gICAgICAgICAgZGF0YS1wbGFjZW1lbnQ9XCJ0b3BcIlxyXG4gICAgICAgICAgdGl0bGU9XCJRdWljayBWaWV3XCJcclxuICAgICAgICAgIG9uQ2xpY2s9e2hhbmRsZVNob3dRdWlja1ZpZXd9XHJcbiAgICAgICAgPlxyXG4gICAgICAgICAgPGkgY2xhc3NOYW1lPVwiaWNvbi1leWVcIj48L2k+XHJcbiAgICAgICAgPC9hPlxyXG4gICAgICA8L2xpPlxyXG4gICAgICA8bGk+XHJcbiAgICAgICAgPGFcclxuICAgICAgICAgIGhyZWY9XCIjXCJcclxuICAgICAgICAgIGRhdGEtdG9nZ2xlPVwidG9vbHRpcFwiXHJcbiAgICAgICAgICBkYXRhLXBsYWNlbWVudD1cInRvcFwiXHJcbiAgICAgICAgICB0aXRsZT1cIkFkZCB0byB3aXNobGlzdFwiXHJcbiAgICAgICAgICBvbkNsaWNrPXtoYW5kbGVBZGRJdGVtVG9XaXNobGlzdH1cclxuICAgICAgICA+XHJcbiAgICAgICAgICA8aSBjbGFzc05hbWU9XCJpY29uLWhlYXJ0XCI+PC9pPlxyXG4gICAgICAgIDwvYT5cclxuICAgICAgPC9saT5cclxuICAgICAgPGxpPlxyXG4gICAgICAgIDxhXHJcbiAgICAgICAgICBocmVmPVwiI1wiXHJcbiAgICAgICAgICBkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIlxyXG4gICAgICAgICAgZGF0YS1wbGFjZW1lbnQ9XCJ0b3BcIlxyXG4gICAgICAgICAgdGl0bGU9XCJDb21wYXJlXCJcclxuICAgICAgICAgIG9uQ2xpY2s9e2hhbmRsZUFkZEl0ZW1Ub0NvbXBhcmV9XHJcbiAgICAgICAgPlxyXG4gICAgICAgICAgPGkgY2xhc3NOYW1lPVwiaWNvbi1jaGFydC1iYXJzXCI+PC9pPlxyXG4gICAgICAgIDwvYT5cclxuICAgICAgPC9saT5cclxuICAgICAgPE1vZGFsXHJcbiAgICAgICAgY2VudGVyZWRcclxuICAgICAgICBmb290ZXI9e251bGx9XHJcbiAgICAgICAgd2lkdGg9ezEwMjR9XHJcbiAgICAgICAgb25DYW5jZWw9eyhlKSA9PiBoYW5kbGVIaWRlUXVpY2tWaWV3KGUpfVxyXG4gICAgICAgIHZpc2libGU9e2lzUXVpY2tWaWV3fVxyXG4gICAgICAgIGNsb3NlSWNvbj17PGkgY2xhc3NOYW1lPVwiaWNvbiBpY29uLWNyb3NzMlwiPjwvaT59XHJcbiAgICAgID5cclxuICAgICAgICA8aDM+UXVpY2t2aWV3PC9oMz5cclxuICAgICAgICA8UHJvZHVjdERldGFpbFF1aWNrVmlldyBwcm9kdWN0PXtwcm9kdWN0fSAvPlxyXG4gICAgICA8L01vZGFsPlxyXG4gICAgPC91bD5cclxuICApO1xyXG59O1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgTW9kdWxlUHJvZHVjdEFjdGlvbnM7XHJcbiJdLCJzb3VyY2VSb290IjoiIn0=