webpackHotUpdate_N_E("pages/product/[pid]",{

/***/ "./components/elements/detail/modules/ModuleDetailShoppingActions.jsx":
/*!****************************************************************************!*\
  !*** ./components/elements/detail/modules/ModuleDetailShoppingActions.jsx ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _store_cart_action__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ~/store/cart/action */ "./store/cart/action.js");
/* harmony import */ var _store_compare_action__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ~/store/compare/action */ "./store/compare/action.js");
/* harmony import */ var _store_wishlist_action__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ~/store/wishlist/action */ "./store/wishlist/action.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_6__);



var _jsxFileName = "D:\\ReactJS\\projects\\nextjs\\web3-shopping\\components\\elements\\detail\\modules\\ModuleDetailShoppingActions.jsx",
    _this = undefined,
    _s = $RefreshSig$();








var ModuleDetailShoppingActions = function ModuleDetailShoppingActions(_ref) {
  _s();

  var product = _ref.product,
      _ref$extended = _ref.extended,
      extended = _ref$extended === void 0 ? false : _ref$extended;
  var dispatch = Object(react_redux__WEBPACK_IMPORTED_MODULE_5__["useDispatch"])();
  var cartItems = Object(react_redux__WEBPACK_IMPORTED_MODULE_5__["useSelector"])(function (state) {
    return state.cart.cartItems;
  });

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(1),
      quantity = _useState[0],
      setQuantity = _useState[1];

  var Router = Object(next_router__WEBPACK_IMPORTED_MODULE_6__["useRouter"])();

  var handleAddItemToCart = function handleAddItemToCart(e) {
    e.preventDefault();
    var tmp = product;
    tmp.quantity = quantity;
    dispatch(Object(_store_cart_action__WEBPACK_IMPORTED_MODULE_2__["addItem"])(tmp));
  };

  var handleBuynow = function handleBuynow(e) {
    e.preventDefault();
    var tmp = product;
    tmp.quantity = quantity;
    dispatch(Object(_store_cart_action__WEBPACK_IMPORTED_MODULE_2__["addItem"])(tmp));
    setTimeout(function () {
      Router.push("/account/checkout");
    }, 1000);
  };

  var handleAddItemToCompare = function handleAddItemToCompare(e) {
    e.preventDefault();
    dispatch(Object(_store_compare_action__WEBPACK_IMPORTED_MODULE_3__["addItemToCompare"])(product));
  };

  var handleAddItemToWishlist = function handleAddItemToWishlist(e) {
    e.preventDefault(); // const { product } = this.props;

    dispatch(Object(_store_wishlist_action__WEBPACK_IMPORTED_MODULE_4__["addItemToWishlist"])(product));
  };

  var handleIncreaseItemQty = function handleIncreaseItemQty(e) {
    e.preventDefault();
    setQuantity(quantity + 1);
  };

  var handleDecreaseItemQty = function handleDecreaseItemQty(e) {
    e.preventDefault();

    if (quantity > 1) {
      setQuantity(quantity - 1);
    }
  };

  if (!extended) {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        className: "ps-product__shopping",
        style: {
          marginBottom: "0px",
          paddingBottom: "0px",
          borderBottom: "0px",
          display: "flex",
          justifyContent: "center"
        },
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
          className: "ps-btn",
          style: {
            fontWeight: "normal"
          },
          href: "#",
          onClick: function onClick(e) {
            return handleBuynow(e);
          },
          children: "Buy Now"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 73,
          columnNumber: 11
        }, _this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 56,
        columnNumber: 9
      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 82,
        columnNumber: 9
      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        className: "ps-product__shopping",
        style: {
          display: "flex",
          justifyContent: "center",
          borderBottom: "0px solid #fff"
        },
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          className: "ps-product__actions",
          style: {
            width: "100%",
            textAlign: "center",
            border: "1px solid #000",
            maxWidth: "100px"
          },
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
            href: "#",
            onClick: function onClick(e) {
              return handleAddItemToWishlist(e);
            },
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
              className: "icon-heart"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 101,
              columnNumber: 15
            }, _this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 100,
            columnNumber: 13
          }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
            href: "#",
            onClick: function onClick(e) {
              return handleAddItemToCompare(e);
            },
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
              className: "icon-chart-bars"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 104,
              columnNumber: 15
            }, _this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 103,
            columnNumber: 13
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 91,
          columnNumber: 11
        }, _this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 83,
        columnNumber: 9
      }, _this)]
    }, void 0, true);
  } else {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      className: "ps-product__shopping extend",
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        className: "ps-product__btn-group",
        style: {
          display: "flex",
          justifyContent: "center"
        },
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
          className: "ps-btn ps-btn--black",
          style: {
            minWidth: "100%",
            marginRight: "20%"
          },
          href: "#",
          onClick: function onClick(e) {
            return handleAddItemToCart(e);
          },
          children: "Add to cart"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 138,
          columnNumber: 11
        }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          className: "ps-product__actions",
          style: {
            border: "1px solid #000",
            textAlign: "center",
            width: "100px"
          },
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
            href: "#",
            onClick: function onClick(e) {
              return handleAddItemToWishlist(e);
            },
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
              className: "icon-heart"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 155,
              columnNumber: 15
            }, _this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 154,
            columnNumber: 13
          }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
            href: "#",
            onClick: function onClick(e) {
              return handleAddItemToCompare(e);
            },
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
              className: "icon-chart-bars"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 158,
              columnNumber: 15
            }, _this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 157,
            columnNumber: 13
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 146,
          columnNumber: 11
        }, _this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 113,
        columnNumber: 9
      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
        className: "ps-btn",
        href: "#",
        onClick: function onClick(e) {
          return handleBuynow(e);
        },
        children: "Buy Now"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 162,
        columnNumber: 9
      }, _this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 112,
      columnNumber: 7
    }, _this);
  }
};

_s(ModuleDetailShoppingActions, "uB62DFDl7e65ktayiBPtCxuxiJI=", false, function () {
  return [react_redux__WEBPACK_IMPORTED_MODULE_5__["useDispatch"], react_redux__WEBPACK_IMPORTED_MODULE_5__["useSelector"], next_router__WEBPACK_IMPORTED_MODULE_6__["useRouter"]];
});

_c = ModuleDetailShoppingActions;
/* harmony default export */ __webpack_exports__["default"] = (ModuleDetailShoppingActions);

var _c;

$RefreshReg$(_c, "ModuleDetailShoppingActions");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9lbGVtZW50cy9kZXRhaWwvbW9kdWxlcy9Nb2R1bGVEZXRhaWxTaG9wcGluZ0FjdGlvbnMuanN4Il0sIm5hbWVzIjpbIk1vZHVsZURldGFpbFNob3BwaW5nQWN0aW9ucyIsInByb2R1Y3QiLCJleHRlbmRlZCIsImRpc3BhdGNoIiwidXNlRGlzcGF0Y2giLCJjYXJ0SXRlbXMiLCJ1c2VTZWxlY3RvciIsInN0YXRlIiwiY2FydCIsInVzZVN0YXRlIiwicXVhbnRpdHkiLCJzZXRRdWFudGl0eSIsIlJvdXRlciIsInVzZVJvdXRlciIsImhhbmRsZUFkZEl0ZW1Ub0NhcnQiLCJlIiwicHJldmVudERlZmF1bHQiLCJ0bXAiLCJhZGRJdGVtIiwiaGFuZGxlQnV5bm93Iiwic2V0VGltZW91dCIsInB1c2giLCJoYW5kbGVBZGRJdGVtVG9Db21wYXJlIiwiYWRkSXRlbVRvQ29tcGFyZSIsImhhbmRsZUFkZEl0ZW1Ub1dpc2hsaXN0IiwiYWRkSXRlbVRvV2lzaGxpc3QiLCJoYW5kbGVJbmNyZWFzZUl0ZW1RdHkiLCJoYW5kbGVEZWNyZWFzZUl0ZW1RdHkiLCJtYXJnaW5Cb3R0b20iLCJwYWRkaW5nQm90dG9tIiwiYm9yZGVyQm90dG9tIiwiZGlzcGxheSIsImp1c3RpZnlDb250ZW50IiwiZm9udFdlaWdodCIsIndpZHRoIiwidGV4dEFsaWduIiwiYm9yZGVyIiwibWF4V2lkdGgiLCJtaW5XaWR0aCIsIm1hcmdpblJpZ2h0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLElBQU1BLDJCQUEyQixHQUFHLFNBQTlCQSwyQkFBOEIsT0FBbUM7QUFBQTs7QUFBQSxNQUFoQ0MsT0FBZ0MsUUFBaENBLE9BQWdDO0FBQUEsMkJBQXZCQyxRQUF1QjtBQUFBLE1BQXZCQSxRQUF1Qiw4QkFBWixLQUFZO0FBQ3JFLE1BQU1DLFFBQVEsR0FBR0MsK0RBQVcsRUFBNUI7QUFDQSxNQUFNQyxTQUFTLEdBQUdDLCtEQUFXLENBQUMsVUFBQ0MsS0FBRDtBQUFBLFdBQVdBLEtBQUssQ0FBQ0MsSUFBTixDQUFXSCxTQUF0QjtBQUFBLEdBQUQsQ0FBN0I7O0FBRnFFLGtCQUdyQ0ksc0RBQVEsQ0FBQyxDQUFELENBSDZCO0FBQUEsTUFHOURDLFFBSDhEO0FBQUEsTUFHcERDLFdBSG9EOztBQUlyRSxNQUFNQyxNQUFNLEdBQUdDLDZEQUFTLEVBQXhCOztBQUVBLE1BQU1DLG1CQUFtQixHQUFHLFNBQXRCQSxtQkFBc0IsQ0FBQ0MsQ0FBRCxFQUFPO0FBQ2pDQSxLQUFDLENBQUNDLGNBQUY7QUFDQSxRQUFJQyxHQUFHLEdBQUdoQixPQUFWO0FBQ0FnQixPQUFHLENBQUNQLFFBQUosR0FBZUEsUUFBZjtBQUNBUCxZQUFRLENBQUNlLGtFQUFPLENBQUNELEdBQUQsQ0FBUixDQUFSO0FBQ0QsR0FMRDs7QUFPQSxNQUFNRSxZQUFZLEdBQUcsU0FBZkEsWUFBZSxDQUFDSixDQUFELEVBQU87QUFDMUJBLEtBQUMsQ0FBQ0MsY0FBRjtBQUNBLFFBQUlDLEdBQUcsR0FBR2hCLE9BQVY7QUFDQWdCLE9BQUcsQ0FBQ1AsUUFBSixHQUFlQSxRQUFmO0FBQ0FQLFlBQVEsQ0FBQ2Usa0VBQU8sQ0FBQ0QsR0FBRCxDQUFSLENBQVI7QUFDQUcsY0FBVSxDQUFDLFlBQVk7QUFDckJSLFlBQU0sQ0FBQ1MsSUFBUCxDQUFZLG1CQUFaO0FBQ0QsS0FGUyxFQUVQLElBRk8sQ0FBVjtBQUdELEdBUkQ7O0FBVUEsTUFBTUMsc0JBQXNCLEdBQUcsU0FBekJBLHNCQUF5QixDQUFDUCxDQUFELEVBQU87QUFDcENBLEtBQUMsQ0FBQ0MsY0FBRjtBQUNBYixZQUFRLENBQUNvQiw4RUFBZ0IsQ0FBQ3RCLE9BQUQsQ0FBakIsQ0FBUjtBQUNELEdBSEQ7O0FBS0EsTUFBTXVCLHVCQUF1QixHQUFHLFNBQTFCQSx1QkFBMEIsQ0FBQ1QsQ0FBRCxFQUFPO0FBQ3JDQSxLQUFDLENBQUNDLGNBQUYsR0FEcUMsQ0FFckM7O0FBQ0FiLFlBQVEsQ0FBQ3NCLGdGQUFpQixDQUFDeEIsT0FBRCxDQUFsQixDQUFSO0FBQ0QsR0FKRDs7QUFNQSxNQUFNeUIscUJBQXFCLEdBQUcsU0FBeEJBLHFCQUF3QixDQUFDWCxDQUFELEVBQU87QUFDbkNBLEtBQUMsQ0FBQ0MsY0FBRjtBQUNBTCxlQUFXLENBQUNELFFBQVEsR0FBRyxDQUFaLENBQVg7QUFDRCxHQUhEOztBQUtBLE1BQU1pQixxQkFBcUIsR0FBRyxTQUF4QkEscUJBQXdCLENBQUNaLENBQUQsRUFBTztBQUNuQ0EsS0FBQyxDQUFDQyxjQUFGOztBQUNBLFFBQUlOLFFBQVEsR0FBRyxDQUFmLEVBQWtCO0FBQ2hCQyxpQkFBVyxDQUFDRCxRQUFRLEdBQUcsQ0FBWixDQUFYO0FBQ0Q7QUFDRixHQUxEOztBQU1BLE1BQUksQ0FBQ1IsUUFBTCxFQUFlO0FBQ2Isd0JBQ0U7QUFBQSw4QkFDRTtBQUNFLGlCQUFTLEVBQUMsc0JBRFo7QUFFRSxhQUFLLEVBQUU7QUFDTDBCLHNCQUFZLEVBQUUsS0FEVDtBQUVMQyx1QkFBYSxFQUFFLEtBRlY7QUFHTEMsc0JBQVksRUFBRSxLQUhUO0FBSUxDLGlCQUFPLEVBQUUsTUFKSjtBQUtMQyx3QkFBYyxFQUFFO0FBTFgsU0FGVDtBQUFBLCtCQWlCRTtBQUNFLG1CQUFTLEVBQUMsUUFEWjtBQUVFLGVBQUssRUFBRTtBQUFFQyxzQkFBVSxFQUFFO0FBQWQsV0FGVDtBQUdFLGNBQUksRUFBQyxHQUhQO0FBSUUsaUJBQU8sRUFBRSxpQkFBQ2xCLENBQUQ7QUFBQSxtQkFBT0ksWUFBWSxDQUFDSixDQUFELENBQW5CO0FBQUEsV0FKWDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWpCRjtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBREYsZUEyQkU7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQTNCRixlQTRCRTtBQUNFLGlCQUFTLEVBQUMsc0JBRFo7QUFFRSxhQUFLLEVBQUU7QUFDTGdCLGlCQUFPLEVBQUUsTUFESjtBQUVMQyx3QkFBYyxFQUFFLFFBRlg7QUFHTEYsc0JBQVksRUFBRTtBQUhULFNBRlQ7QUFBQSwrQkFRRTtBQUNFLG1CQUFTLEVBQUMscUJBRFo7QUFFRSxlQUFLLEVBQUU7QUFDTEksaUJBQUssRUFBRSxNQURGO0FBRUxDLHFCQUFTLEVBQUUsUUFGTjtBQUdMQyxrQkFBTSxFQUFFLGdCQUhIO0FBSUxDLG9CQUFRLEVBQUU7QUFKTCxXQUZUO0FBQUEsa0NBU0U7QUFBRyxnQkFBSSxFQUFDLEdBQVI7QUFBWSxtQkFBTyxFQUFFLGlCQUFDdEIsQ0FBRDtBQUFBLHFCQUFPUyx1QkFBdUIsQ0FBQ1QsQ0FBRCxDQUE5QjtBQUFBLGFBQXJCO0FBQUEsbUNBQ0U7QUFBRyx1QkFBUyxFQUFDO0FBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBVEYsZUFZRTtBQUFHLGdCQUFJLEVBQUMsR0FBUjtBQUFZLG1CQUFPLEVBQUUsaUJBQUNBLENBQUQ7QUFBQSxxQkFBT08sc0JBQXNCLENBQUNQLENBQUQsQ0FBN0I7QUFBQSxhQUFyQjtBQUFBLG1DQUNFO0FBQUcsdUJBQVMsRUFBQztBQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQVpGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVJGO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUE1QkY7QUFBQSxvQkFERjtBQXdERCxHQXpERCxNQXlETztBQUNMLHdCQUNFO0FBQUssZUFBUyxFQUFDLDZCQUFmO0FBQUEsOEJBQ0U7QUFDRSxpQkFBUyxFQUFDLHVCQURaO0FBRUUsYUFBSyxFQUFFO0FBQUVnQixpQkFBTyxFQUFFLE1BQVg7QUFBbUJDLHdCQUFjLEVBQUU7QUFBbkMsU0FGVDtBQUFBLGdDQXlCRTtBQUNFLG1CQUFTLEVBQUMsc0JBRFo7QUFFRSxlQUFLLEVBQUU7QUFBRU0sb0JBQVEsRUFBRSxNQUFaO0FBQW9CQyx1QkFBVyxFQUFFO0FBQWpDLFdBRlQ7QUFHRSxjQUFJLEVBQUMsR0FIUDtBQUlFLGlCQUFPLEVBQUUsaUJBQUN4QixDQUFEO0FBQUEsbUJBQU9ELG1CQUFtQixDQUFDQyxDQUFELENBQTFCO0FBQUEsV0FKWDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkF6QkYsZUFpQ0U7QUFDRSxtQkFBUyxFQUFDLHFCQURaO0FBRUUsZUFBSyxFQUFFO0FBQ0xxQixrQkFBTSxFQUFFLGdCQURIO0FBRUxELHFCQUFTLEVBQUUsUUFGTjtBQUdMRCxpQkFBSyxFQUFFO0FBSEYsV0FGVDtBQUFBLGtDQVFFO0FBQUcsZ0JBQUksRUFBQyxHQUFSO0FBQVksbUJBQU8sRUFBRSxpQkFBQ25CLENBQUQ7QUFBQSxxQkFBT1MsdUJBQXVCLENBQUNULENBQUQsQ0FBOUI7QUFBQSxhQUFyQjtBQUFBLG1DQUNFO0FBQUcsdUJBQVMsRUFBQztBQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQVJGLGVBV0U7QUFBRyxnQkFBSSxFQUFDLEdBQVI7QUFBWSxtQkFBTyxFQUFFLGlCQUFDQSxDQUFEO0FBQUEscUJBQU9PLHNCQUFzQixDQUFDUCxDQUFELENBQTdCO0FBQUEsYUFBckI7QUFBQSxtQ0FDRTtBQUFHLHVCQUFTLEVBQUM7QUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFYRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBakNGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQURGLGVBa0RFO0FBQUcsaUJBQVMsRUFBQyxRQUFiO0FBQXNCLFlBQUksRUFBQyxHQUEzQjtBQUErQixlQUFPLEVBQUUsaUJBQUNBLENBQUQ7QUFBQSxpQkFBT0ksWUFBWSxDQUFDSixDQUFELENBQW5CO0FBQUEsU0FBeEM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFsREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGFBREY7QUF3REQ7QUFDRixDQWhLRDs7R0FBTWYsMkI7VUFDYUksdUQsRUFDQ0UsdUQsRUFFSE8scUQ7OztLQUpYYiwyQjtBQWtLU0EsMEZBQWYiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvcHJvZHVjdC9bcGlkXS4zMWRkZGM3YmNmMjc5YTRkYmNiMS5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IHVzZVN0YXRlIH0gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCB7IGFkZEl0ZW0gfSBmcm9tIFwifi9zdG9yZS9jYXJ0L2FjdGlvblwiO1xyXG5pbXBvcnQgeyBhZGRJdGVtVG9Db21wYXJlIH0gZnJvbSBcIn4vc3RvcmUvY29tcGFyZS9hY3Rpb25cIjtcclxuaW1wb3J0IHsgYWRkSXRlbVRvV2lzaGxpc3QgfSBmcm9tIFwifi9zdG9yZS93aXNobGlzdC9hY3Rpb25cIjtcclxuaW1wb3J0IHsgdXNlRGlzcGF0Y2gsIHVzZVNlbGVjdG9yIH0gZnJvbSBcInJlYWN0LXJlZHV4XCI7XHJcbmltcG9ydCB7IHVzZVJvdXRlciB9IGZyb20gXCJuZXh0L3JvdXRlclwiO1xyXG5cclxuY29uc3QgTW9kdWxlRGV0YWlsU2hvcHBpbmdBY3Rpb25zID0gKHsgcHJvZHVjdCwgZXh0ZW5kZWQgPSBmYWxzZSB9KSA9PiB7XHJcbiAgY29uc3QgZGlzcGF0Y2ggPSB1c2VEaXNwYXRjaCgpO1xyXG4gIGNvbnN0IGNhcnRJdGVtcyA9IHVzZVNlbGVjdG9yKChzdGF0ZSkgPT4gc3RhdGUuY2FydC5jYXJ0SXRlbXMpO1xyXG4gIGNvbnN0IFtxdWFudGl0eSwgc2V0UXVhbnRpdHldID0gdXNlU3RhdGUoMSk7XHJcbiAgY29uc3QgUm91dGVyID0gdXNlUm91dGVyKCk7XHJcblxyXG4gIGNvbnN0IGhhbmRsZUFkZEl0ZW1Ub0NhcnQgPSAoZSkgPT4ge1xyXG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgbGV0IHRtcCA9IHByb2R1Y3Q7XHJcbiAgICB0bXAucXVhbnRpdHkgPSBxdWFudGl0eTtcclxuICAgIGRpc3BhdGNoKGFkZEl0ZW0odG1wKSk7XHJcbiAgfTtcclxuXHJcbiAgY29uc3QgaGFuZGxlQnV5bm93ID0gKGUpID0+IHtcclxuICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgIGxldCB0bXAgPSBwcm9kdWN0O1xyXG4gICAgdG1wLnF1YW50aXR5ID0gcXVhbnRpdHk7XHJcbiAgICBkaXNwYXRjaChhZGRJdGVtKHRtcCkpO1xyXG4gICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgIFJvdXRlci5wdXNoKFwiL2FjY291bnQvY2hlY2tvdXRcIik7XHJcbiAgICB9LCAxMDAwKTtcclxuICB9O1xyXG5cclxuICBjb25zdCBoYW5kbGVBZGRJdGVtVG9Db21wYXJlID0gKGUpID0+IHtcclxuICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgIGRpc3BhdGNoKGFkZEl0ZW1Ub0NvbXBhcmUocHJvZHVjdCkpO1xyXG4gIH07XHJcblxyXG4gIGNvbnN0IGhhbmRsZUFkZEl0ZW1Ub1dpc2hsaXN0ID0gKGUpID0+IHtcclxuICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgIC8vIGNvbnN0IHsgcHJvZHVjdCB9ID0gdGhpcy5wcm9wcztcclxuICAgIGRpc3BhdGNoKGFkZEl0ZW1Ub1dpc2hsaXN0KHByb2R1Y3QpKTtcclxuICB9O1xyXG5cclxuICBjb25zdCBoYW5kbGVJbmNyZWFzZUl0ZW1RdHkgPSAoZSkgPT4ge1xyXG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgc2V0UXVhbnRpdHkocXVhbnRpdHkgKyAxKTtcclxuICB9O1xyXG5cclxuICBjb25zdCBoYW5kbGVEZWNyZWFzZUl0ZW1RdHkgPSAoZSkgPT4ge1xyXG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgaWYgKHF1YW50aXR5ID4gMSkge1xyXG4gICAgICBzZXRRdWFudGl0eShxdWFudGl0eSAtIDEpO1xyXG4gICAgfVxyXG4gIH07XHJcbiAgaWYgKCFleHRlbmRlZCkge1xyXG4gICAgcmV0dXJuIChcclxuICAgICAgPD5cclxuICAgICAgICA8ZGl2XHJcbiAgICAgICAgICBjbGFzc05hbWU9XCJwcy1wcm9kdWN0X19zaG9wcGluZ1wiXHJcbiAgICAgICAgICBzdHlsZT17e1xyXG4gICAgICAgICAgICBtYXJnaW5Cb3R0b206IFwiMHB4XCIsXHJcbiAgICAgICAgICAgIHBhZGRpbmdCb3R0b206IFwiMHB4XCIsXHJcbiAgICAgICAgICAgIGJvcmRlckJvdHRvbTogXCIwcHhcIixcclxuICAgICAgICAgICAgZGlzcGxheTogXCJmbGV4XCIsXHJcbiAgICAgICAgICAgIGp1c3RpZnlDb250ZW50OiBcImNlbnRlclwiLFxyXG4gICAgICAgICAgfX1cclxuICAgICAgICA+XHJcbiAgICAgICAgICB7Lyoge2NhcnRJdGVtcz8ubGVuZ3RoIDwgMSAmJiA8YVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJwcy1idG4gcHMtYnRuLS1ibGFja1wiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhyZWY9XCIjXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGU9e3tmb250V2VpZ2h0Olwibm9ybWFsXCJ9fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoZSkgPT4gaGFuZGxlQWRkSXRlbVRvQ2FydChlKX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIEFkZCB0byBjYXJ0XHJcbiAgICAgICAgICAgICAgICAgICAgPC9hPn0gKi99XHJcbiAgICAgICAgICA8YVxyXG4gICAgICAgICAgICBjbGFzc05hbWU9XCJwcy1idG5cIlxyXG4gICAgICAgICAgICBzdHlsZT17eyBmb250V2VpZ2h0OiBcIm5vcm1hbFwiIH19XHJcbiAgICAgICAgICAgIGhyZWY9XCIjXCJcclxuICAgICAgICAgICAgb25DbGljaz17KGUpID0+IGhhbmRsZUJ1eW5vdyhlKX1cclxuICAgICAgICAgID5cclxuICAgICAgICAgICAgQnV5IE5vd1xyXG4gICAgICAgICAgPC9hPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxicj48L2JyPlxyXG4gICAgICAgIDxkaXZcclxuICAgICAgICAgIGNsYXNzTmFtZT1cInBzLXByb2R1Y3RfX3Nob3BwaW5nXCJcclxuICAgICAgICAgIHN0eWxlPXt7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IFwiZmxleFwiLFxyXG4gICAgICAgICAgICBqdXN0aWZ5Q29udGVudDogXCJjZW50ZXJcIixcclxuICAgICAgICAgICAgYm9yZGVyQm90dG9tOiBcIjBweCBzb2xpZCAjZmZmXCIsXHJcbiAgICAgICAgICB9fVxyXG4gICAgICAgID5cclxuICAgICAgICAgIDxkaXZcclxuICAgICAgICAgICAgY2xhc3NOYW1lPVwicHMtcHJvZHVjdF9fYWN0aW9uc1wiXHJcbiAgICAgICAgICAgIHN0eWxlPXt7XHJcbiAgICAgICAgICAgICAgd2lkdGg6IFwiMTAwJVwiLFxyXG4gICAgICAgICAgICAgIHRleHRBbGlnbjogXCJjZW50ZXJcIixcclxuICAgICAgICAgICAgICBib3JkZXI6IFwiMXB4IHNvbGlkICMwMDBcIixcclxuICAgICAgICAgICAgICBtYXhXaWR0aDogXCIxMDBweFwiLFxyXG4gICAgICAgICAgICB9fVxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICA8YSBocmVmPVwiI1wiIG9uQ2xpY2s9eyhlKSA9PiBoYW5kbGVBZGRJdGVtVG9XaXNobGlzdChlKX0+XHJcbiAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiaWNvbi1oZWFydFwiPjwvaT5cclxuICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICA8YSBocmVmPVwiI1wiIG9uQ2xpY2s9eyhlKSA9PiBoYW5kbGVBZGRJdGVtVG9Db21wYXJlKGUpfT5cclxuICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJpY29uLWNoYXJ0LWJhcnNcIj48L2k+XHJcbiAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICA8Lz5cclxuICAgICk7XHJcbiAgfSBlbHNlIHtcclxuICAgIHJldHVybiAoXHJcbiAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHMtcHJvZHVjdF9fc2hvcHBpbmcgZXh0ZW5kXCI+XHJcbiAgICAgICAgPGRpdlxyXG4gICAgICAgICAgY2xhc3NOYW1lPVwicHMtcHJvZHVjdF9fYnRuLWdyb3VwXCJcclxuICAgICAgICAgIHN0eWxlPXt7IGRpc3BsYXk6IFwiZmxleFwiLCBqdXN0aWZ5Q29udGVudDogXCJjZW50ZXJcIiB9fVxyXG4gICAgICAgID5cclxuICAgICAgICAgIHsvKiA8ZmlndXJlPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZmlnY2FwdGlvbj5RdWFudGl0eTwvZmlnY2FwdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmb3JtLWdyb3VwLS1udW1iZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b25cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJ1cFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KGUpID0+IGhhbmRsZUluY3JlYXNlSXRlbVF0eShlKX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmEgZmEtcGx1c1wiPjwvaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImRvd25cIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eyhlKSA9PiBoYW5kbGVEZWNyZWFzZUl0ZW1RdHkoZSl9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhIGZhLW1pbnVzXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJmb3JtLWNvbnRyb2xcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj17cXVhbnRpdHl9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZmlndXJlPiAqL31cclxuICAgICAgICAgIDxhXHJcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cInBzLWJ0biBwcy1idG4tLWJsYWNrXCJcclxuICAgICAgICAgICAgc3R5bGU9e3sgbWluV2lkdGg6IFwiMTAwJVwiLCBtYXJnaW5SaWdodDogXCIyMCVcIiB9fVxyXG4gICAgICAgICAgICBocmVmPVwiI1wiXHJcbiAgICAgICAgICAgIG9uQ2xpY2s9eyhlKSA9PiBoYW5kbGVBZGRJdGVtVG9DYXJ0KGUpfVxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICBBZGQgdG8gY2FydFxyXG4gICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgPGRpdlxyXG4gICAgICAgICAgICBjbGFzc05hbWU9XCJwcy1wcm9kdWN0X19hY3Rpb25zXCJcclxuICAgICAgICAgICAgc3R5bGU9e3tcclxuICAgICAgICAgICAgICBib3JkZXI6IFwiMXB4IHNvbGlkICMwMDBcIixcclxuICAgICAgICAgICAgICB0ZXh0QWxpZ246IFwiY2VudGVyXCIsXHJcbiAgICAgICAgICAgICAgd2lkdGg6IFwiMTAwcHhcIixcclxuICAgICAgICAgICAgfX1cclxuICAgICAgICAgID5cclxuICAgICAgICAgICAgPGEgaHJlZj1cIiNcIiBvbkNsaWNrPXsoZSkgPT4gaGFuZGxlQWRkSXRlbVRvV2lzaGxpc3QoZSl9PlxyXG4gICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImljb24taGVhcnRcIj48L2k+XHJcbiAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgPGEgaHJlZj1cIiNcIiBvbkNsaWNrPXsoZSkgPT4gaGFuZGxlQWRkSXRlbVRvQ29tcGFyZShlKX0+XHJcbiAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiaWNvbi1jaGFydC1iYXJzXCI+PC9pPlxyXG4gICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8YSBjbGFzc05hbWU9XCJwcy1idG5cIiBocmVmPVwiI1wiIG9uQ2xpY2s9eyhlKSA9PiBoYW5kbGVCdXlub3coZSl9PlxyXG4gICAgICAgICAgQnV5IE5vd1xyXG4gICAgICAgIDwvYT5cclxuICAgICAgPC9kaXY+XHJcbiAgICApO1xyXG4gIH1cclxufTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IE1vZHVsZURldGFpbFNob3BwaW5nQWN0aW9ucztcclxuIl0sInNvdXJjZVJvb3QiOiIifQ==