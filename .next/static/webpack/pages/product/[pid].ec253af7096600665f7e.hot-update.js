webpackHotUpdate_N_E("pages/product/[pid]",{

/***/ "./components/elements/detail/modules/ModuleDetailActionsMobile.jsx":
/*!**************************************************************************!*\
  !*** ./components/elements/detail/modules/ModuleDetailActionsMobile.jsx ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _store_cart_action__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ~/store/cart/action */ "./store/cart/action.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");


var _jsxFileName = "D:\\ReactJS\\projects\\nextjs\\web3-shopping\\components\\elements\\detail\\modules\\ModuleDetailActionsMobile.jsx",
    _this = undefined,
    _s = $RefreshSig$();





var ModuleDetailActionsMobile = function ModuleDetailActionsMobile(_ref) {
  _s();

  var product = _ref.product;
  var dispatch = Object(react_redux__WEBPACK_IMPORTED_MODULE_3__["useDispatch"])();

  var handleAddItemToCart = function handleAddItemToCart(e) {
    e.preventDefault();
    var tmp = product;
    tmp.quantity = 1;
    dispatch(Object(_store_cart_action__WEBPACK_IMPORTED_MODULE_2__["addItem"])(tmp));
  };

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
    className: "ps-product__actions-mobile",
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
      className: "ps-btn",
      href: "#",
      style: {
        fontWeight: "normal"
      },
      onClick: function onClick(e) {
        return handleAddItemToCart(e);
      },
      children: "Buy Now"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 7
    }, _this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 14,
    columnNumber: 5
  }, _this);
};

_s(ModuleDetailActionsMobile, "rgTLoBID190wEKCp9+G8W6F7A5M=", false, function () {
  return [react_redux__WEBPACK_IMPORTED_MODULE_3__["useDispatch"]];
});

_c = ModuleDetailActionsMobile;
/* harmony default export */ __webpack_exports__["default"] = (ModuleDetailActionsMobile);

var _c;

$RefreshReg$(_c, "ModuleDetailActionsMobile");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9lbGVtZW50cy9kZXRhaWwvbW9kdWxlcy9Nb2R1bGVEZXRhaWxBY3Rpb25zTW9iaWxlLmpzeCJdLCJuYW1lcyI6WyJNb2R1bGVEZXRhaWxBY3Rpb25zTW9iaWxlIiwicHJvZHVjdCIsImRpc3BhdGNoIiwidXNlRGlzcGF0Y2giLCJoYW5kbGVBZGRJdGVtVG9DYXJ0IiwiZSIsInByZXZlbnREZWZhdWx0IiwidG1wIiwicXVhbnRpdHkiLCJhZGRJdGVtIiwiZm9udFdlaWdodCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7O0FBRUEsSUFBTUEseUJBQXlCLEdBQUcsU0FBNUJBLHlCQUE0QixPQUFpQjtBQUFBOztBQUFBLE1BQWRDLE9BQWMsUUFBZEEsT0FBYztBQUNqRCxNQUFNQyxRQUFRLEdBQUdDLCtEQUFXLEVBQTVCOztBQUNBLE1BQU1DLG1CQUFtQixHQUFHLFNBQXRCQSxtQkFBc0IsQ0FBQ0MsQ0FBRCxFQUFPO0FBQ2pDQSxLQUFDLENBQUNDLGNBQUY7QUFDQSxRQUFJQyxHQUFHLEdBQUdOLE9BQVY7QUFDQU0sT0FBRyxDQUFDQyxRQUFKLEdBQWUsQ0FBZjtBQUNBTixZQUFRLENBQUNPLGtFQUFPLENBQUNGLEdBQUQsQ0FBUixDQUFSO0FBQ0QsR0FMRDs7QUFNQSxzQkFDRTtBQUFLLGFBQVMsRUFBQyw0QkFBZjtBQUFBLDJCQVFFO0FBQ0UsZUFBUyxFQUFDLFFBRFo7QUFFRSxVQUFJLEVBQUMsR0FGUDtBQUdFLFdBQUssRUFBRTtBQUFFRyxrQkFBVSxFQUFFO0FBQWQsT0FIVDtBQUlFLGFBQU8sRUFBRSxpQkFBQ0wsQ0FBRDtBQUFBLGVBQU9ELG1CQUFtQixDQUFDQyxDQUFELENBQTFCO0FBQUEsT0FKWDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVJGO0FBQUE7QUFBQTtBQUFBO0FBQUEsV0FERjtBQW1CRCxDQTNCRDs7R0FBTUwseUI7VUFDYUcsdUQ7OztLQURiSCx5QjtBQTZCU0Esd0ZBQWYiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvcHJvZHVjdC9bcGlkXS5lYzI1M2FmNzA5NjYwMDY2NWY3ZS5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IHVzZVN0YXRlIH0gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCB7IGFkZEl0ZW0gfSBmcm9tIFwifi9zdG9yZS9jYXJ0L2FjdGlvblwiO1xyXG5pbXBvcnQgeyB1c2VEaXNwYXRjaCB9IGZyb20gXCJyZWFjdC1yZWR1eFwiO1xyXG5cclxuY29uc3QgTW9kdWxlRGV0YWlsQWN0aW9uc01vYmlsZSA9ICh7IHByb2R1Y3QgfSkgPT4ge1xyXG4gIGNvbnN0IGRpc3BhdGNoID0gdXNlRGlzcGF0Y2goKTtcclxuICBjb25zdCBoYW5kbGVBZGRJdGVtVG9DYXJ0ID0gKGUpID0+IHtcclxuICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgIGxldCB0bXAgPSBwcm9kdWN0O1xyXG4gICAgdG1wLnF1YW50aXR5ID0gMTtcclxuICAgIGRpc3BhdGNoKGFkZEl0ZW0odG1wKSk7XHJcbiAgfTtcclxuICByZXR1cm4gKFxyXG4gICAgPGRpdiBjbGFzc05hbWU9XCJwcy1wcm9kdWN0X19hY3Rpb25zLW1vYmlsZVwiPlxyXG4gICAgICB7LyogPGFcclxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInBzLWJ0biBwcy1idG4tLWJsYWNrXCJcclxuICAgICAgICAgICAgICAgIGhyZWY9XCIjXCJcclxuICAgICAgICAgICAgICAgIHN0eWxlPXt7Zm9udFdlaWdodDpcIm5vcm1hbFwifX1cclxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9eyhlKSA9PiBoYW5kbGVBZGRJdGVtVG9DYXJ0KGUpfT5cclxuICAgICAgICAgICAgICAgIEFkZCB0byBjYXJ0XHJcbiAgICAgICAgICAgIDwvYT4gKi99XHJcbiAgICAgIDxhXHJcbiAgICAgICAgY2xhc3NOYW1lPVwicHMtYnRuXCJcclxuICAgICAgICBocmVmPVwiI1wiXHJcbiAgICAgICAgc3R5bGU9e3sgZm9udFdlaWdodDogXCJub3JtYWxcIiB9fVxyXG4gICAgICAgIG9uQ2xpY2s9eyhlKSA9PiBoYW5kbGVBZGRJdGVtVG9DYXJ0KGUpfVxyXG4gICAgICA+XHJcbiAgICAgICAgQnV5IE5vd1xyXG4gICAgICA8L2E+XHJcbiAgICA8L2Rpdj5cclxuICApO1xyXG59O1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgTW9kdWxlRGV0YWlsQWN0aW9uc01vYmlsZTtcclxuIl0sInNvdXJjZVJvb3QiOiIifQ==