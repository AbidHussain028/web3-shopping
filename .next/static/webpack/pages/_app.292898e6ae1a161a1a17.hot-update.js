webpackHotUpdate_N_E("pages/_app",{

/***/ "./store/cart/reducer.js":
/*!*******************************!*\
  !*** ./store/cart/reducer.js ***!
  \*******************************/
/*! exports provided: initCart, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initCart", function() { return initCart; });
/* harmony import */ var D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var _action__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./action */ "./store/cart/action.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }


var initCart = {
  cartItems: [],
  amount: 0,
  cartTotal: 0
};

function reducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initCart;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case _action__WEBPACK_IMPORTED_MODULE_1__["actionTypes"].GET_CART_SUCCESS:
      return _objectSpread({}, state);

    case _action__WEBPACK_IMPORTED_MODULE_1__["actionTypes"].UPDATE_CART_SUCCESS:
      console.log(action.payload);
      return _objectSpread(_objectSpread(_objectSpread(_objectSpread({}, state), {
        cartItems: action.payload.cartItems
      }), {
        amount: action.payload.amount
      }), {
        cartTotal: action.payload.cartTotal
      });

    case _action__WEBPACK_IMPORTED_MODULE_1__["actionTypes"].CLEAR_CART_SUCCESS:
      return _objectSpread(_objectSpread(_objectSpread(_objectSpread({}, state), {
        cartItems: action.payload.cartItems
      }), {
        amount: action.payload.amount
      }), {
        cartTotal: action.payload.cartTotal
      });

    case _action__WEBPACK_IMPORTED_MODULE_1__["actionTypes"].GET_CART_ERROR:
      return _objectSpread(_objectSpread({}, state), {
        error: action.error
      });

    case _action__WEBPACK_IMPORTED_MODULE_1__["actionTypes"].UPDATE_CART_ERROR:
      return _objectSpread(_objectSpread({}, state), {
        error: action.error
      });

    default:
      return state;
  }
}

/* harmony default export */ __webpack_exports__["default"] = (reducer);

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vc3RvcmUvY2FydC9yZWR1Y2VyLmpzIl0sIm5hbWVzIjpbImluaXRDYXJ0IiwiY2FydEl0ZW1zIiwiYW1vdW50IiwiY2FydFRvdGFsIiwicmVkdWNlciIsInN0YXRlIiwiYWN0aW9uIiwidHlwZSIsImFjdGlvblR5cGVzIiwiR0VUX0NBUlRfU1VDQ0VTUyIsIlVQREFURV9DQVJUX1NVQ0NFU1MiLCJjb25zb2xlIiwibG9nIiwicGF5bG9hZCIsIkNMRUFSX0NBUlRfU1VDQ0VTUyIsIkdFVF9DQVJUX0VSUk9SIiwiZXJyb3IiLCJVUERBVEVfQ0FSVF9FUlJPUiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUVPLElBQU1BLFFBQVEsR0FBRztBQUN0QkMsV0FBUyxFQUFFLEVBRFc7QUFFdEJDLFFBQU0sRUFBRSxDQUZjO0FBR3RCQyxXQUFTLEVBQUU7QUFIVyxDQUFqQjs7QUFNUCxTQUFTQyxPQUFULEdBQTJDO0FBQUEsTUFBMUJDLEtBQTBCLHVFQUFsQkwsUUFBa0I7QUFBQSxNQUFSTSxNQUFROztBQUN6QyxVQUFRQSxNQUFNLENBQUNDLElBQWY7QUFDRSxTQUFLQyxtREFBVyxDQUFDQyxnQkFBakI7QUFDRSwrQkFDS0osS0FETDs7QUFHRixTQUFLRyxtREFBVyxDQUFDRSxtQkFBakI7QUFDRUMsYUFBTyxDQUFDQyxHQUFSLENBQVlOLE1BQU0sQ0FBQ08sT0FBbkI7QUFDQSx5RUFDS1IsS0FETCxHQUVLO0FBQUVKLGlCQUFTLEVBQUVLLE1BQU0sQ0FBQ08sT0FBUCxDQUFlWjtBQUE1QixPQUZMLEdBR0s7QUFBRUMsY0FBTSxFQUFFSSxNQUFNLENBQUNPLE9BQVAsQ0FBZVg7QUFBekIsT0FITCxHQUlLO0FBQUVDLGlCQUFTLEVBQUVHLE1BQU0sQ0FBQ08sT0FBUCxDQUFlVjtBQUE1QixPQUpMOztBQU1GLFNBQUtLLG1EQUFXLENBQUNNLGtCQUFqQjtBQUNFLHlFQUNLVCxLQURMLEdBRUs7QUFBRUosaUJBQVMsRUFBRUssTUFBTSxDQUFDTyxPQUFQLENBQWVaO0FBQTVCLE9BRkwsR0FHSztBQUFFQyxjQUFNLEVBQUVJLE1BQU0sQ0FBQ08sT0FBUCxDQUFlWDtBQUF6QixPQUhMLEdBSUs7QUFBRUMsaUJBQVMsRUFBRUcsTUFBTSxDQUFDTyxPQUFQLENBQWVWO0FBQTVCLE9BSkw7O0FBTUYsU0FBS0ssbURBQVcsQ0FBQ08sY0FBakI7QUFDRSw2Q0FDS1YsS0FETCxHQUVLO0FBQUVXLGFBQUssRUFBRVYsTUFBTSxDQUFDVTtBQUFoQixPQUZMOztBQUlGLFNBQUtSLG1EQUFXLENBQUNTLGlCQUFqQjtBQUNFLDZDQUNLWixLQURMLEdBRUs7QUFBRVcsYUFBSyxFQUFFVixNQUFNLENBQUNVO0FBQWhCLE9BRkw7O0FBSUY7QUFDRSxhQUFPWCxLQUFQO0FBL0JKO0FBaUNEOztBQUVjRCxzRUFBZiIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9fYXBwLjI5Mjg5OGU2YWUxYTE2MWExYTE3LmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBhY3Rpb25UeXBlcyB9IGZyb20gXCIuL2FjdGlvblwiO1xyXG5cclxuZXhwb3J0IGNvbnN0IGluaXRDYXJ0ID0ge1xyXG4gIGNhcnRJdGVtczogW10sXHJcbiAgYW1vdW50OiAwLFxyXG4gIGNhcnRUb3RhbDogMCxcclxufTtcclxuXHJcbmZ1bmN0aW9uIHJlZHVjZXIoc3RhdGUgPSBpbml0Q2FydCwgYWN0aW9uKSB7XHJcbiAgc3dpdGNoIChhY3Rpb24udHlwZSkge1xyXG4gICAgY2FzZSBhY3Rpb25UeXBlcy5HRVRfQ0FSVF9TVUNDRVNTOlxyXG4gICAgICByZXR1cm4ge1xyXG4gICAgICAgIC4uLnN0YXRlLFxyXG4gICAgICB9O1xyXG4gICAgY2FzZSBhY3Rpb25UeXBlcy5VUERBVEVfQ0FSVF9TVUNDRVNTOlxyXG4gICAgICBjb25zb2xlLmxvZyhhY3Rpb24ucGF5bG9hZCk7XHJcbiAgICAgIHJldHVybiB7XHJcbiAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgLi4ueyBjYXJ0SXRlbXM6IGFjdGlvbi5wYXlsb2FkLmNhcnRJdGVtcyB9LFxyXG4gICAgICAgIC4uLnsgYW1vdW50OiBhY3Rpb24ucGF5bG9hZC5hbW91bnQgfSxcclxuICAgICAgICAuLi57IGNhcnRUb3RhbDogYWN0aW9uLnBheWxvYWQuY2FydFRvdGFsIH0sXHJcbiAgICAgIH07XHJcbiAgICBjYXNlIGFjdGlvblR5cGVzLkNMRUFSX0NBUlRfU1VDQ0VTUzpcclxuICAgICAgcmV0dXJuIHtcclxuICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICAuLi57IGNhcnRJdGVtczogYWN0aW9uLnBheWxvYWQuY2FydEl0ZW1zIH0sXHJcbiAgICAgICAgLi4ueyBhbW91bnQ6IGFjdGlvbi5wYXlsb2FkLmFtb3VudCB9LFxyXG4gICAgICAgIC4uLnsgY2FydFRvdGFsOiBhY3Rpb24ucGF5bG9hZC5jYXJ0VG90YWwgfSxcclxuICAgICAgfTtcclxuICAgIGNhc2UgYWN0aW9uVHlwZXMuR0VUX0NBUlRfRVJST1I6XHJcbiAgICAgIHJldHVybiB7XHJcbiAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgLi4ueyBlcnJvcjogYWN0aW9uLmVycm9yIH0sXHJcbiAgICAgIH07XHJcbiAgICBjYXNlIGFjdGlvblR5cGVzLlVQREFURV9DQVJUX0VSUk9SOlxyXG4gICAgICByZXR1cm4ge1xyXG4gICAgICAgIC4uLnN0YXRlLFxyXG4gICAgICAgIC4uLnsgZXJyb3I6IGFjdGlvbi5lcnJvciB9LFxyXG4gICAgICB9O1xyXG4gICAgZGVmYXVsdDpcclxuICAgICAgcmV0dXJuIHN0YXRlO1xyXG4gIH1cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgcmVkdWNlcjtcclxuIl0sInNvdXJjZVJvb3QiOiIifQ==