webpackHotUpdate_N_E("pages/_app",{

/***/ "./store/cart/saga.js":
/*!****************************!*\
  !*** ./store/cart/saga.js ***!
  \****************************/
/*! exports provided: calculateAmount, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "calculateAmount", function() { return calculateAmount; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return rootSaga; });
/* harmony import */ var D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/regenerator */ "./node_modules/next/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! redux-saga/effects */ "./node_modules/redux-saga/dist/redux-saga-effects-npm-proxy.esm.js");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _action__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./action */ "./store/cart/action.js");



function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var _marked = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(getCartSaga),
    _marked2 = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(addItemSaga),
    _marked3 = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(removeItemSaga),
    _marked4 = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(increaseQtySaga),
    _marked5 = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(decreaseItemQtySaga),
    _marked6 = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(clearCartSaga),
    _marked7 = /*#__PURE__*/D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(rootSaga);





var modalSuccess = function modalSuccess(type) {
  antd__WEBPACK_IMPORTED_MODULE_3__["notification"][type]({
    message: "Success",
    description: "This product has been added to your cart!",
    duration: 1
  });
};

var modalWarning = function modalWarning(type) {
  antd__WEBPACK_IMPORTED_MODULE_3__["notification"][type]({
    message: "Remove A Item",
    description: "This product has been removed from your cart!",
    duration: 1
  });
};

var calculateAmount = function calculateAmount(obj) {
  return Object.values(obj).reduce(function (acc, _ref) {
    var quantity = _ref.quantity,
        price = _ref.price;
    return acc + quantity * price;
  }, 0).toFixed(2);
};

function getCartSaga() {
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function getCartSaga$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["getCartSuccess"])());

        case 3:
          _context.next = 9;
          break;

        case 5:
          _context.prev = 5;
          _context.t0 = _context["catch"](0);
          _context.next = 9;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["getCartError"])(_context.t0));

        case 9:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, null, [[0, 5]]);
}

function addItemSaga(payload) {
  var product, localCart, currentCart, existItem;
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function addItemSaga$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          product = payload.product;
          localCart = JSON.parse(localStorage.getItem("persist:martfury")).cart;
          currentCart = JSON.parse(localCart);
          existItem = currentCart.cartItems.find(function (item) {
            return item.id === product.id;
          });

          if (existItem) {
            existItem.quantity += product.quantity;
          } else {
            if (!product.quantity) {
              product.quantity = 1;
            }

            currentCart.cartItems = [_objectSpread({}, product)];
          }

          currentCart.amount = calculateAmount(currentCart.cartItems);
          currentCart.cartTotal++;
          _context2.next = 10;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["updateCartSuccess"])(currentCart));

        case 10:
          modalSuccess("success");
          _context2.next = 17;
          break;

        case 13:
          _context2.prev = 13;
          _context2.t0 = _context2["catch"](0);
          _context2.next = 17;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["getCartError"])(_context2.t0));

        case 17:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2, null, [[0, 13]]);
}

function removeItemSaga(payload) {
  var product, localCart, index;
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function removeItemSaga$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          product = payload.product;
          localCart = JSON.parse(JSON.parse(localStorage.getItem("persist:martfury")).cart);
          index = localCart.cartItems.findIndex(function (item) {
            return item.id === product.id;
          });
          localCart.cartTotal = localCart.cartTotal - product.quantity;
          localCart.cartItems.splice(index, 1);
          localCart.amount = calculateAmount(localCart.cartItems);

          if (localCart.cartItems.length === 0) {
            localCart.cartItems = [];
            localCart.amount = 0;
            localCart.cartTotal = 0;
          }

          _context3.next = 10;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["updateCartSuccess"])(localCart));

        case 10:
          modalWarning("warning");
          _context3.next = 17;
          break;

        case 13:
          _context3.prev = 13;
          _context3.t0 = _context3["catch"](0);
          _context3.next = 17;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["getCartError"])(_context3.t0));

        case 17:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3, null, [[0, 13]]);
}

function increaseQtySaga(payload) {
  var product, localCart, selectedItem;
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function increaseQtySaga$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.prev = 0;
          product = payload.product;
          localCart = JSON.parse(JSON.parse(localStorage.getItem("persist:martfury")).cart);
          selectedItem = localCart.cartItems.find(function (item) {
            return item.id === product.id;
          });

          if (selectedItem) {
            selectedItem.quantity++;
            localCart.cartTotal++;
            localCart.amount = calculateAmount(localCart.cartItems);
          }

          _context4.next = 7;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["updateCartSuccess"])(localCart));

        case 7:
          _context4.next = 13;
          break;

        case 9:
          _context4.prev = 9;
          _context4.t0 = _context4["catch"](0);
          _context4.next = 13;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["getCartError"])(_context4.t0));

        case 13:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4, null, [[0, 9]]);
}

function decreaseItemQtySaga(payload) {
  var product, localCart, selectedItem;
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function decreaseItemQtySaga$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.prev = 0;
          product = payload.product;
          localCart = JSON.parse(JSON.parse(localStorage.getItem("persist:martfury")).cart);
          selectedItem = localCart.cartItems.find(function (item) {
            return item.id === product.id;
          });

          if (selectedItem) {
            selectedItem.quantity--;
            localCart.cartTotal--;
            localCart.amount = calculateAmount(localCart.cartItems);
          }

          _context5.next = 7;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["updateCartSuccess"])(localCart));

        case 7:
          _context5.next = 13;
          break;

        case 9:
          _context5.prev = 9;
          _context5.t0 = _context5["catch"](0);
          _context5.next = 13;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["getCartError"])(_context5.t0));

        case 13:
        case "end":
          return _context5.stop();
      }
    }
  }, _marked5, null, [[0, 9]]);
}

function clearCartSaga() {
  var emptyCart;
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function clearCartSaga$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.prev = 0;
          emptyCart = {
            cartItems: [],
            amount: 0,
            cartTotal: 0
          };
          _context6.next = 4;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["updateCartSuccess"])(emptyCart));

        case 4:
          _context6.next = 10;
          break;

        case 6:
          _context6.prev = 6;
          _context6.t0 = _context6["catch"](0);
          _context6.next = 10;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])(Object(_action__WEBPACK_IMPORTED_MODULE_4__["updateCartError"])(_context6.t0));

        case 10:
        case "end":
          return _context6.stop();
      }
    }
  }, _marked6, null, [[0, 6]]);
}

function rootSaga() {
  return D_ReactJS_projects_nextjs_web3_shopping_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function rootSaga$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          _context7.next = 2;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["all"])([Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["takeEvery"])(_action__WEBPACK_IMPORTED_MODULE_4__["actionTypes"].GET_CART, getCartSaga)]);

        case 2:
          _context7.next = 4;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["all"])([Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["takeEvery"])(_action__WEBPACK_IMPORTED_MODULE_4__["actionTypes"].ADD_ITEM, addItemSaga)]);

        case 4:
          _context7.next = 6;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["all"])([Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["takeEvery"])(_action__WEBPACK_IMPORTED_MODULE_4__["actionTypes"].REMOVE_ITEM, removeItemSaga)]);

        case 6:
          _context7.next = 8;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["all"])([Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["takeEvery"])(_action__WEBPACK_IMPORTED_MODULE_4__["actionTypes"].INCREASE_QTY, increaseQtySaga)]);

        case 8:
          _context7.next = 10;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["all"])([Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["takeEvery"])(_action__WEBPACK_IMPORTED_MODULE_4__["actionTypes"].DECREASE_QTY, decreaseItemQtySaga)]);

        case 10:
        case "end":
          return _context7.stop();
      }
    }
  }, _marked7);
}

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vc3RvcmUvY2FydC9zYWdhLmpzIl0sIm5hbWVzIjpbImdldENhcnRTYWdhIiwiYWRkSXRlbVNhZ2EiLCJyZW1vdmVJdGVtU2FnYSIsImluY3JlYXNlUXR5U2FnYSIsImRlY3JlYXNlSXRlbVF0eVNhZ2EiLCJjbGVhckNhcnRTYWdhIiwicm9vdFNhZ2EiLCJtb2RhbFN1Y2Nlc3MiLCJ0eXBlIiwibm90aWZpY2F0aW9uIiwibWVzc2FnZSIsImRlc2NyaXB0aW9uIiwiZHVyYXRpb24iLCJtb2RhbFdhcm5pbmciLCJjYWxjdWxhdGVBbW91bnQiLCJvYmoiLCJPYmplY3QiLCJ2YWx1ZXMiLCJyZWR1Y2UiLCJhY2MiLCJxdWFudGl0eSIsInByaWNlIiwidG9GaXhlZCIsInB1dCIsImdldENhcnRTdWNjZXNzIiwiZ2V0Q2FydEVycm9yIiwicGF5bG9hZCIsInByb2R1Y3QiLCJsb2NhbENhcnQiLCJKU09OIiwicGFyc2UiLCJsb2NhbFN0b3JhZ2UiLCJnZXRJdGVtIiwiY2FydCIsImN1cnJlbnRDYXJ0IiwiZXhpc3RJdGVtIiwiY2FydEl0ZW1zIiwiZmluZCIsIml0ZW0iLCJpZCIsImFtb3VudCIsImNhcnRUb3RhbCIsInVwZGF0ZUNhcnRTdWNjZXNzIiwiaW5kZXgiLCJmaW5kSW5kZXgiLCJzcGxpY2UiLCJsZW5ndGgiLCJzZWxlY3RlZEl0ZW0iLCJlbXB0eUNhcnQiLCJ1cGRhdGVDYXJ0RXJyb3IiLCJhbGwiLCJ0YWtlRXZlcnkiLCJhY3Rpb25UeXBlcyIsIkdFVF9DQVJUIiwiQUREX0lURU0iLCJSRU1PVkVfSVRFTSIsIklOQ1JFQVNFX1FUWSIsIkRFQ1JFQVNFX1FUWSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7d0tBK0JVQSxXO3lLQVFBQyxXO3lLQXlCQUMsYzt5S0FzQkFDLGU7eUtBb0JBQyxtQjt5S0FxQkFDLGE7eUtBYWVDLFE7O0FBNUl6QjtBQUNBO0FBRUE7O0FBUUEsSUFBTUMsWUFBWSxHQUFHLFNBQWZBLFlBQWUsQ0FBQ0MsSUFBRCxFQUFVO0FBQzdCQyxtREFBWSxDQUFDRCxJQUFELENBQVosQ0FBbUI7QUFDakJFLFdBQU8sRUFBRSxTQURRO0FBRWpCQyxlQUFXLEVBQUUsMkNBRkk7QUFHakJDLFlBQVEsRUFBRTtBQUhPLEdBQW5CO0FBS0QsQ0FORDs7QUFPQSxJQUFNQyxZQUFZLEdBQUcsU0FBZkEsWUFBZSxDQUFDTCxJQUFELEVBQVU7QUFDN0JDLG1EQUFZLENBQUNELElBQUQsQ0FBWixDQUFtQjtBQUNqQkUsV0FBTyxFQUFFLGVBRFE7QUFFakJDLGVBQVcsRUFBRSwrQ0FGSTtBQUdqQkMsWUFBUSxFQUFFO0FBSE8sR0FBbkI7QUFLRCxDQU5EOztBQVFPLElBQU1FLGVBQWUsR0FBRyxTQUFsQkEsZUFBa0IsQ0FBQ0MsR0FBRDtBQUFBLFNBQzdCQyxNQUFNLENBQUNDLE1BQVAsQ0FBY0YsR0FBZCxFQUNHRyxNQURILENBQ1UsVUFBQ0MsR0FBRDtBQUFBLFFBQVFDLFFBQVIsUUFBUUEsUUFBUjtBQUFBLFFBQWtCQyxLQUFsQixRQUFrQkEsS0FBbEI7QUFBQSxXQUE4QkYsR0FBRyxHQUFHQyxRQUFRLEdBQUdDLEtBQS9DO0FBQUEsR0FEVixFQUNnRSxDQURoRSxFQUVHQyxPQUZILENBRVcsQ0FGWCxDQUQ2QjtBQUFBLENBQXhCOztBQUtQLFNBQVV0QixXQUFWO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUksaUJBQU11Qiw4REFBRyxDQUFDQyw4REFBYyxFQUFmLENBQVQ7O0FBRko7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUksaUJBQU1ELDhEQUFHLENBQUNFLDREQUFZLGFBQWIsQ0FBVDs7QUFKSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFRQSxTQUFVeEIsV0FBVixDQUFzQnlCLE9BQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRVlDLGlCQUZaLEdBRXdCRCxPQUZ4QixDQUVZQyxPQUZaO0FBR1VDLG1CQUhWLEdBR3NCQyxJQUFJLENBQUNDLEtBQUwsQ0FBV0MsWUFBWSxDQUFDQyxPQUFiLENBQXFCLGtCQUFyQixDQUFYLEVBQXFEQyxJQUgzRTtBQUlRQyxxQkFKUixHQUlzQkwsSUFBSSxDQUFDQyxLQUFMLENBQVdGLFNBQVgsQ0FKdEI7QUFLUU8sbUJBTFIsR0FLb0JELFdBQVcsQ0FBQ0UsU0FBWixDQUFzQkMsSUFBdEIsQ0FDZCxVQUFDQyxJQUFEO0FBQUEsbUJBQVVBLElBQUksQ0FBQ0MsRUFBTCxLQUFZWixPQUFPLENBQUNZLEVBQTlCO0FBQUEsV0FEYyxDQUxwQjs7QUFRSSxjQUFJSixTQUFKLEVBQWU7QUFDYkEscUJBQVMsQ0FBQ2YsUUFBVixJQUFzQk8sT0FBTyxDQUFDUCxRQUE5QjtBQUNELFdBRkQsTUFFTztBQUNMLGdCQUFJLENBQUNPLE9BQU8sQ0FBQ1AsUUFBYixFQUF1QjtBQUNyQk8scUJBQU8sQ0FBQ1AsUUFBUixHQUFtQixDQUFuQjtBQUNEOztBQUNEYyx1QkFBVyxDQUFDRSxTQUFaLEdBQXdCLG1CQUFNVCxPQUFOLEVBQXhCO0FBQ0Q7O0FBQ0RPLHFCQUFXLENBQUNNLE1BQVosR0FBcUIxQixlQUFlLENBQUNvQixXQUFXLENBQUNFLFNBQWIsQ0FBcEM7QUFDQUYscUJBQVcsQ0FBQ08sU0FBWjtBQWpCSjtBQWtCSSxpQkFBTWxCLDhEQUFHLENBQUNtQixpRUFBaUIsQ0FBQ1IsV0FBRCxDQUFsQixDQUFUOztBQWxCSjtBQW1CSTNCLHNCQUFZLENBQUMsU0FBRCxDQUFaO0FBbkJKO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFxQkksaUJBQU1nQiw4REFBRyxDQUFDRSw0REFBWSxjQUFiLENBQVQ7O0FBckJKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQXlCQSxTQUFVdkIsY0FBVixDQUF5QndCLE9BQXpCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRVlDLGlCQUZaLEdBRXdCRCxPQUZ4QixDQUVZQyxPQUZaO0FBR1FDLG1CQUhSLEdBR29CQyxJQUFJLENBQUNDLEtBQUwsQ0FDZEQsSUFBSSxDQUFDQyxLQUFMLENBQVdDLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixrQkFBckIsQ0FBWCxFQUFxREMsSUFEdkMsQ0FIcEI7QUFNUVUsZUFOUixHQU1nQmYsU0FBUyxDQUFDUSxTQUFWLENBQW9CUSxTQUFwQixDQUE4QixVQUFDTixJQUFEO0FBQUEsbUJBQVVBLElBQUksQ0FBQ0MsRUFBTCxLQUFZWixPQUFPLENBQUNZLEVBQTlCO0FBQUEsV0FBOUIsQ0FOaEI7QUFPSVgsbUJBQVMsQ0FBQ2EsU0FBVixHQUFzQmIsU0FBUyxDQUFDYSxTQUFWLEdBQXNCZCxPQUFPLENBQUNQLFFBQXBEO0FBQ0FRLG1CQUFTLENBQUNRLFNBQVYsQ0FBb0JTLE1BQXBCLENBQTJCRixLQUEzQixFQUFrQyxDQUFsQztBQUNBZixtQkFBUyxDQUFDWSxNQUFWLEdBQW1CMUIsZUFBZSxDQUFDYyxTQUFTLENBQUNRLFNBQVgsQ0FBbEM7O0FBQ0EsY0FBSVIsU0FBUyxDQUFDUSxTQUFWLENBQW9CVSxNQUFwQixLQUErQixDQUFuQyxFQUFzQztBQUNwQ2xCLHFCQUFTLENBQUNRLFNBQVYsR0FBc0IsRUFBdEI7QUFDQVIscUJBQVMsQ0FBQ1ksTUFBVixHQUFtQixDQUFuQjtBQUNBWixxQkFBUyxDQUFDYSxTQUFWLEdBQXNCLENBQXRCO0FBQ0Q7O0FBZEw7QUFlSSxpQkFBTWxCLDhEQUFHLENBQUNtQixpRUFBaUIsQ0FBQ2QsU0FBRCxDQUFsQixDQUFUOztBQWZKO0FBZ0JJZixzQkFBWSxDQUFDLFNBQUQsQ0FBWjtBQWhCSjtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBa0JJLGlCQUFNVSw4REFBRyxDQUFDRSw0REFBWSxjQUFiLENBQVQ7O0FBbEJKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQXNCQSxTQUFVdEIsZUFBVixDQUEwQnVCLE9BQTFCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRVlDLGlCQUZaLEdBRXdCRCxPQUZ4QixDQUVZQyxPQUZaO0FBR1FDLG1CQUhSLEdBR29CQyxJQUFJLENBQUNDLEtBQUwsQ0FDZEQsSUFBSSxDQUFDQyxLQUFMLENBQVdDLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixrQkFBckIsQ0FBWCxFQUFxREMsSUFEdkMsQ0FIcEI7QUFNUWMsc0JBTlIsR0FNdUJuQixTQUFTLENBQUNRLFNBQVYsQ0FBb0JDLElBQXBCLENBQ2pCLFVBQUNDLElBQUQ7QUFBQSxtQkFBVUEsSUFBSSxDQUFDQyxFQUFMLEtBQVlaLE9BQU8sQ0FBQ1ksRUFBOUI7QUFBQSxXQURpQixDQU52Qjs7QUFTSSxjQUFJUSxZQUFKLEVBQWtCO0FBQ2hCQSx3QkFBWSxDQUFDM0IsUUFBYjtBQUNBUSxxQkFBUyxDQUFDYSxTQUFWO0FBQ0FiLHFCQUFTLENBQUNZLE1BQVYsR0FBbUIxQixlQUFlLENBQUNjLFNBQVMsQ0FBQ1EsU0FBWCxDQUFsQztBQUNEOztBQWJMO0FBY0ksaUJBQU1iLDhEQUFHLENBQUNtQixpRUFBaUIsQ0FBQ2QsU0FBRCxDQUFsQixDQUFUOztBQWRKO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWdCSSxpQkFBTUwsOERBQUcsQ0FBQ0UsNERBQVksY0FBYixDQUFUOztBQWhCSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFvQkEsU0FBVXJCLG1CQUFWLENBQThCc0IsT0FBOUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFWUMsaUJBRlosR0FFd0JELE9BRnhCLENBRVlDLE9BRlo7QUFHVUMsbUJBSFYsR0FHc0JDLElBQUksQ0FBQ0MsS0FBTCxDQUNoQkQsSUFBSSxDQUFDQyxLQUFMLENBQVdDLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixrQkFBckIsQ0FBWCxFQUFxREMsSUFEckMsQ0FIdEI7QUFNUWMsc0JBTlIsR0FNdUJuQixTQUFTLENBQUNRLFNBQVYsQ0FBb0JDLElBQXBCLENBQ2pCLFVBQUNDLElBQUQ7QUFBQSxtQkFBVUEsSUFBSSxDQUFDQyxFQUFMLEtBQVlaLE9BQU8sQ0FBQ1ksRUFBOUI7QUFBQSxXQURpQixDQU52Qjs7QUFVSSxjQUFJUSxZQUFKLEVBQWtCO0FBQ2hCQSx3QkFBWSxDQUFDM0IsUUFBYjtBQUNBUSxxQkFBUyxDQUFDYSxTQUFWO0FBQ0FiLHFCQUFTLENBQUNZLE1BQVYsR0FBbUIxQixlQUFlLENBQUNjLFNBQVMsQ0FBQ1EsU0FBWCxDQUFsQztBQUNEOztBQWRMO0FBZUksaUJBQU1iLDhEQUFHLENBQUNtQixpRUFBaUIsQ0FBQ2QsU0FBRCxDQUFsQixDQUFUOztBQWZKO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWlCSSxpQkFBTUwsOERBQUcsQ0FBQ0UsNERBQVksY0FBYixDQUFUOztBQWpCSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFxQkEsU0FBVXBCLGFBQVY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFVTJDLG1CQUZWLEdBRXNCO0FBQ2hCWixxQkFBUyxFQUFFLEVBREs7QUFFaEJJLGtCQUFNLEVBQUUsQ0FGUTtBQUdoQkMscUJBQVMsRUFBRTtBQUhLLFdBRnRCO0FBQUE7QUFPSSxpQkFBTWxCLDhEQUFHLENBQUNtQixpRUFBaUIsQ0FBQ00sU0FBRCxDQUFsQixDQUFUOztBQVBKO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNJLGlCQUFNekIsOERBQUcsQ0FBQzBCLCtEQUFlLGNBQWhCLENBQVQ7O0FBVEo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBYWUsU0FBVTNDLFFBQVY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ2IsaUJBQU00Qyw4REFBRyxDQUFDLENBQUNDLG9FQUFTLENBQUNDLG1EQUFXLENBQUNDLFFBQWIsRUFBdUJyRCxXQUF2QixDQUFWLENBQUQsQ0FBVDs7QUFEYTtBQUFBO0FBRWIsaUJBQU1rRCw4REFBRyxDQUFDLENBQUNDLG9FQUFTLENBQUNDLG1EQUFXLENBQUNFLFFBQWIsRUFBdUJyRCxXQUF2QixDQUFWLENBQUQsQ0FBVDs7QUFGYTtBQUFBO0FBR2IsaUJBQU1pRCw4REFBRyxDQUFDLENBQUNDLG9FQUFTLENBQUNDLG1EQUFXLENBQUNHLFdBQWIsRUFBMEJyRCxjQUExQixDQUFWLENBQUQsQ0FBVDs7QUFIYTtBQUFBO0FBSWIsaUJBQU1nRCw4REFBRyxDQUFDLENBQUNDLG9FQUFTLENBQUNDLG1EQUFXLENBQUNJLFlBQWIsRUFBMkJyRCxlQUEzQixDQUFWLENBQUQsQ0FBVDs7QUFKYTtBQUFBO0FBS2IsaUJBQU0rQyw4REFBRyxDQUFDLENBQUNDLG9FQUFTLENBQUNDLG1EQUFXLENBQUNLLFlBQWIsRUFBMkJyRCxtQkFBM0IsQ0FBVixDQUFELENBQVQ7O0FBTGE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvX2FwcC45OGZiZTc4ZmI0MzYyNTlkMjY3YS5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgYWxsLCBwdXQsIHRha2VFdmVyeSB9IGZyb20gXCJyZWR1eC1zYWdhL2VmZmVjdHNcIjtcclxuaW1wb3J0IHsgbm90aWZpY2F0aW9uIH0gZnJvbSBcImFudGRcIjtcclxuXHJcbmltcG9ydCB7XHJcbiAgYWN0aW9uVHlwZXMsXHJcbiAgZ2V0Q2FydEVycm9yLFxyXG4gIGdldENhcnRTdWNjZXNzLFxyXG4gIHVwZGF0ZUNhcnRTdWNjZXNzLFxyXG4gIHVwZGF0ZUNhcnRFcnJvcixcclxufSBmcm9tIFwiLi9hY3Rpb25cIjtcclxuXHJcbmNvbnN0IG1vZGFsU3VjY2VzcyA9ICh0eXBlKSA9PiB7XHJcbiAgbm90aWZpY2F0aW9uW3R5cGVdKHtcclxuICAgIG1lc3NhZ2U6IFwiU3VjY2Vzc1wiLFxyXG4gICAgZGVzY3JpcHRpb246IFwiVGhpcyBwcm9kdWN0IGhhcyBiZWVuIGFkZGVkIHRvIHlvdXIgY2FydCFcIixcclxuICAgIGR1cmF0aW9uOiAxLFxyXG4gIH0pO1xyXG59O1xyXG5jb25zdCBtb2RhbFdhcm5pbmcgPSAodHlwZSkgPT4ge1xyXG4gIG5vdGlmaWNhdGlvblt0eXBlXSh7XHJcbiAgICBtZXNzYWdlOiBcIlJlbW92ZSBBIEl0ZW1cIixcclxuICAgIGRlc2NyaXB0aW9uOiBcIlRoaXMgcHJvZHVjdCBoYXMgYmVlbiByZW1vdmVkIGZyb20geW91ciBjYXJ0IVwiLFxyXG4gICAgZHVyYXRpb246IDEsXHJcbiAgfSk7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgY2FsY3VsYXRlQW1vdW50ID0gKG9iaikgPT5cclxuICBPYmplY3QudmFsdWVzKG9iailcclxuICAgIC5yZWR1Y2UoKGFjYywgeyBxdWFudGl0eSwgcHJpY2UgfSkgPT4gYWNjICsgcXVhbnRpdHkgKiBwcmljZSwgMClcclxuICAgIC50b0ZpeGVkKDIpO1xyXG5cclxuZnVuY3Rpb24qIGdldENhcnRTYWdhKCkge1xyXG4gIHRyeSB7XHJcbiAgICB5aWVsZCBwdXQoZ2V0Q2FydFN1Y2Nlc3MoKSk7XHJcbiAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICB5aWVsZCBwdXQoZ2V0Q2FydEVycm9yKGVycikpO1xyXG4gIH1cclxufVxyXG5cclxuZnVuY3Rpb24qIGFkZEl0ZW1TYWdhKHBheWxvYWQpIHtcclxuICB0cnkge1xyXG4gICAgY29uc3QgeyBwcm9kdWN0IH0gPSBwYXlsb2FkO1xyXG4gICAgY29uc3QgbG9jYWxDYXJ0ID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcInBlcnNpc3Q6bWFydGZ1cnlcIikpLmNhcnQ7XHJcbiAgICBsZXQgY3VycmVudENhcnQgPSBKU09OLnBhcnNlKGxvY2FsQ2FydCk7XHJcbiAgICBsZXQgZXhpc3RJdGVtID0gY3VycmVudENhcnQuY2FydEl0ZW1zLmZpbmQoXHJcbiAgICAgIChpdGVtKSA9PiBpdGVtLmlkID09PSBwcm9kdWN0LmlkXHJcbiAgICApO1xyXG4gICAgaWYgKGV4aXN0SXRlbSkge1xyXG4gICAgICBleGlzdEl0ZW0ucXVhbnRpdHkgKz0gcHJvZHVjdC5xdWFudGl0eTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGlmICghcHJvZHVjdC5xdWFudGl0eSkge1xyXG4gICAgICAgIHByb2R1Y3QucXVhbnRpdHkgPSAxO1xyXG4gICAgICB9XHJcbiAgICAgIGN1cnJlbnRDYXJ0LmNhcnRJdGVtcyA9IFt7IC4uLnByb2R1Y3QgfV07XHJcbiAgICB9XHJcbiAgICBjdXJyZW50Q2FydC5hbW91bnQgPSBjYWxjdWxhdGVBbW91bnQoY3VycmVudENhcnQuY2FydEl0ZW1zKTtcclxuICAgIGN1cnJlbnRDYXJ0LmNhcnRUb3RhbCsrO1xyXG4gICAgeWllbGQgcHV0KHVwZGF0ZUNhcnRTdWNjZXNzKGN1cnJlbnRDYXJ0KSk7XHJcbiAgICBtb2RhbFN1Y2Nlc3MoXCJzdWNjZXNzXCIpO1xyXG4gIH0gY2F0Y2ggKGVycikge1xyXG4gICAgeWllbGQgcHV0KGdldENhcnRFcnJvcihlcnIpKTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uKiByZW1vdmVJdGVtU2FnYShwYXlsb2FkKSB7XHJcbiAgdHJ5IHtcclxuICAgIGNvbnN0IHsgcHJvZHVjdCB9ID0gcGF5bG9hZDtcclxuICAgIGxldCBsb2NhbENhcnQgPSBKU09OLnBhcnNlKFxyXG4gICAgICBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwicGVyc2lzdDptYXJ0ZnVyeVwiKSkuY2FydFxyXG4gICAgKTtcclxuICAgIGxldCBpbmRleCA9IGxvY2FsQ2FydC5jYXJ0SXRlbXMuZmluZEluZGV4KChpdGVtKSA9PiBpdGVtLmlkID09PSBwcm9kdWN0LmlkKTtcclxuICAgIGxvY2FsQ2FydC5jYXJ0VG90YWwgPSBsb2NhbENhcnQuY2FydFRvdGFsIC0gcHJvZHVjdC5xdWFudGl0eTtcclxuICAgIGxvY2FsQ2FydC5jYXJ0SXRlbXMuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgIGxvY2FsQ2FydC5hbW91bnQgPSBjYWxjdWxhdGVBbW91bnQobG9jYWxDYXJ0LmNhcnRJdGVtcyk7XHJcbiAgICBpZiAobG9jYWxDYXJ0LmNhcnRJdGVtcy5sZW5ndGggPT09IDApIHtcclxuICAgICAgbG9jYWxDYXJ0LmNhcnRJdGVtcyA9IFtdO1xyXG4gICAgICBsb2NhbENhcnQuYW1vdW50ID0gMDtcclxuICAgICAgbG9jYWxDYXJ0LmNhcnRUb3RhbCA9IDA7XHJcbiAgICB9XHJcbiAgICB5aWVsZCBwdXQodXBkYXRlQ2FydFN1Y2Nlc3MobG9jYWxDYXJ0KSk7XHJcbiAgICBtb2RhbFdhcm5pbmcoXCJ3YXJuaW5nXCIpO1xyXG4gIH0gY2F0Y2ggKGVycikge1xyXG4gICAgeWllbGQgcHV0KGdldENhcnRFcnJvcihlcnIpKTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uKiBpbmNyZWFzZVF0eVNhZ2EocGF5bG9hZCkge1xyXG4gIHRyeSB7XHJcbiAgICBjb25zdCB7IHByb2R1Y3QgfSA9IHBheWxvYWQ7XHJcbiAgICBsZXQgbG9jYWxDYXJ0ID0gSlNPTi5wYXJzZShcclxuICAgICAgSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcInBlcnNpc3Q6bWFydGZ1cnlcIikpLmNhcnRcclxuICAgICk7XHJcbiAgICBsZXQgc2VsZWN0ZWRJdGVtID0gbG9jYWxDYXJ0LmNhcnRJdGVtcy5maW5kKFxyXG4gICAgICAoaXRlbSkgPT4gaXRlbS5pZCA9PT0gcHJvZHVjdC5pZFxyXG4gICAgKTtcclxuICAgIGlmIChzZWxlY3RlZEl0ZW0pIHtcclxuICAgICAgc2VsZWN0ZWRJdGVtLnF1YW50aXR5Kys7XHJcbiAgICAgIGxvY2FsQ2FydC5jYXJ0VG90YWwrKztcclxuICAgICAgbG9jYWxDYXJ0LmFtb3VudCA9IGNhbGN1bGF0ZUFtb3VudChsb2NhbENhcnQuY2FydEl0ZW1zKTtcclxuICAgIH1cclxuICAgIHlpZWxkIHB1dCh1cGRhdGVDYXJ0U3VjY2Vzcyhsb2NhbENhcnQpKTtcclxuICB9IGNhdGNoIChlcnIpIHtcclxuICAgIHlpZWxkIHB1dChnZXRDYXJ0RXJyb3IoZXJyKSk7XHJcbiAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiogZGVjcmVhc2VJdGVtUXR5U2FnYShwYXlsb2FkKSB7XHJcbiAgdHJ5IHtcclxuICAgIGNvbnN0IHsgcHJvZHVjdCB9ID0gcGF5bG9hZDtcclxuICAgIGNvbnN0IGxvY2FsQ2FydCA9IEpTT04ucGFyc2UoXHJcbiAgICAgIEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJwZXJzaXN0Om1hcnRmdXJ5XCIpKS5jYXJ0XHJcbiAgICApO1xyXG4gICAgbGV0IHNlbGVjdGVkSXRlbSA9IGxvY2FsQ2FydC5jYXJ0SXRlbXMuZmluZChcclxuICAgICAgKGl0ZW0pID0+IGl0ZW0uaWQgPT09IHByb2R1Y3QuaWRcclxuICAgICk7XHJcblxyXG4gICAgaWYgKHNlbGVjdGVkSXRlbSkge1xyXG4gICAgICBzZWxlY3RlZEl0ZW0ucXVhbnRpdHktLTtcclxuICAgICAgbG9jYWxDYXJ0LmNhcnRUb3RhbC0tO1xyXG4gICAgICBsb2NhbENhcnQuYW1vdW50ID0gY2FsY3VsYXRlQW1vdW50KGxvY2FsQ2FydC5jYXJ0SXRlbXMpO1xyXG4gICAgfVxyXG4gICAgeWllbGQgcHV0KHVwZGF0ZUNhcnRTdWNjZXNzKGxvY2FsQ2FydCkpO1xyXG4gIH0gY2F0Y2ggKGVycikge1xyXG4gICAgeWllbGQgcHV0KGdldENhcnRFcnJvcihlcnIpKTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uKiBjbGVhckNhcnRTYWdhKCkge1xyXG4gIHRyeSB7XHJcbiAgICBjb25zdCBlbXB0eUNhcnQgPSB7XHJcbiAgICAgIGNhcnRJdGVtczogW10sXHJcbiAgICAgIGFtb3VudDogMCxcclxuICAgICAgY2FydFRvdGFsOiAwLFxyXG4gICAgfTtcclxuICAgIHlpZWxkIHB1dCh1cGRhdGVDYXJ0U3VjY2VzcyhlbXB0eUNhcnQpKTtcclxuICB9IGNhdGNoIChlcnIpIHtcclxuICAgIHlpZWxkIHB1dCh1cGRhdGVDYXJ0RXJyb3IoZXJyKSk7XHJcbiAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiogcm9vdFNhZ2EoKSB7XHJcbiAgeWllbGQgYWxsKFt0YWtlRXZlcnkoYWN0aW9uVHlwZXMuR0VUX0NBUlQsIGdldENhcnRTYWdhKV0pO1xyXG4gIHlpZWxkIGFsbChbdGFrZUV2ZXJ5KGFjdGlvblR5cGVzLkFERF9JVEVNLCBhZGRJdGVtU2FnYSldKTtcclxuICB5aWVsZCBhbGwoW3Rha2VFdmVyeShhY3Rpb25UeXBlcy5SRU1PVkVfSVRFTSwgcmVtb3ZlSXRlbVNhZ2EpXSk7XHJcbiAgeWllbGQgYWxsKFt0YWtlRXZlcnkoYWN0aW9uVHlwZXMuSU5DUkVBU0VfUVRZLCBpbmNyZWFzZVF0eVNhZ2EpXSk7XHJcbiAgeWllbGQgYWxsKFt0YWtlRXZlcnkoYWN0aW9uVHlwZXMuREVDUkVBU0VfUVRZLCBkZWNyZWFzZUl0ZW1RdHlTYWdhKV0pO1xyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiIn0=