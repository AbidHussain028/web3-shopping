import React from 'react';

const PartialDescription = ({ product }) => (
    <div className="ps-document" style={{textAlign:"justify"}}>
        <p>{product.description}</p>
    </div>
);

export default PartialDescription;
