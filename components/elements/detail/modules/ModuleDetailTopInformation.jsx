import React from 'react';
import Link from 'next/link';
import Rating from '~/components/elements/Rating';

const ModuleDetailTopInformation = ({ product }) => {
    // Views
    let priceView;

    if (product.is_sale) {
        priceView = (
            <h4 className="ps-product__price sale">
                <del className="mr-2">&{product.sale_price}</del>
                <img
                    src="/static/img/eth.png"
                    style={{ maxHeight: '25px' }}
                    alt="Luxury Ledger Market"
                />{' '}
                {product.price}
            </h4>
        );
    } else {
        priceView = (
            <h4 className="ps-product__price">
                <img
                    src="/static/img/eth.png"
                    style={{ maxHeight: '25px' }}
                    alt="Luxury Ledger Market"
                />
                {product.price}
            </h4>
        );
    }
    return (
        <header style={{ textAlign: 'center' }}>
            <h1>{product.title}</h1>
            {priceView}
        </header>
    );
};

export default ModuleDetailTopInformation;
