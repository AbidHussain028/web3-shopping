import React, { useState } from "react";
import { addItem } from "~/store/cart/action";
import { addItemToCompare } from "~/store/compare/action";
import { addItemToWishlist } from "~/store/wishlist/action";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";

const ModuleDetailShoppingActions = ({ product, extended = false }) => {
  const dispatch = useDispatch();
  const cartItems = useSelector((state) => state.cart.cartItems);
  const [quantity, setQuantity] = useState(1);
  const Router = useRouter();

  const handleAddItemToCart = (e) => {
    e.preventDefault();
    let tmp = product;
    tmp.quantity = quantity;
    dispatch(addItem(tmp));
  };

  const handleBuynow = (e) => {
    e.preventDefault();
    let tmp = product;
    tmp.quantity = quantity;
    dispatch(addItem(tmp));
    setTimeout(function () {
      Router.push("/account/checkout");
    }, 1000);
  };

  const handleAddItemToCompare = (e) => {
    e.preventDefault();
    dispatch(addItemToCompare(product));
  };

  const handleAddItemToWishlist = (e) => {
    e.preventDefault();
    // const { product } = this.props;
    dispatch(addItemToWishlist(product));
  };

  const handleIncreaseItemQty = (e) => {
    e.preventDefault();
    setQuantity(quantity + 1);
  };

  const handleDecreaseItemQty = (e) => {
    e.preventDefault();
    if (quantity > 1) {
      setQuantity(quantity - 1);
    }
  };
  if (!extended) {
    return (
      <>
        <div
          className="ps-product__shopping"
          style={{
            marginBottom: "0px",
            paddingBottom: "0px",
            borderBottom: "0px",
            display: "flex",
            justifyContent: "center",
          }}
        >
          {/* {cartItems?.length < 1 && <a
                        className="ps-btn ps-btn--black"
                        href="#"
                        style={{fontWeight:"normal"}}
                        onClick={(e) => handleAddItemToCart(e)}>
                        Add to cart
                    </a>} */}
          <a
            className="ps-btn"
            style={{ fontWeight: "normal" }}
            href="#"
            onClick={(e) => handleBuynow(e)}
          >
            Buy Now
          </a>
        </div>
        <br></br>
        <div
          className="ps-product__shopping"
          style={{
            display: "flex",
            justifyContent: "center",
            borderBottom: "0px solid #fff",
          }}
        >
          <div
            className="ps-product__actions"
            style={{
              width: "100%",
              textAlign: "center",
              border: "1px solid #000",
              maxWidth: "100px",
            }}
          >
            <a href="#" onClick={(e) => handleAddItemToWishlist(e)}>
              <i className="icon-heart"></i>
            </a>
            <a href="#" onClick={(e) => handleAddItemToCompare(e)}>
              <i className="icon-chart-bars"></i>
            </a>
          </div>
        </div>
      </>
    );
  } else {
    return (
      <div className="ps-product__shopping extend">
        <div
          className="ps-product__btn-group"
          style={{ display: "flex", justifyContent: "center" }}
        >
          {/* <figure>
                        <figcaption>Quantity</figcaption>
                        <div className="form-group--number">
                            <button
                                className="up"
                                onClick={(e) => handleIncreaseItemQty(e)}>
                                <i className="fa fa-plus"></i>
                            </button>
                            <button
                                className="down"
                                onClick={(e) => handleDecreaseItemQty(e)}>
                                <i className="fa fa-minus"></i>
                            </button>
                            <input
                                className="form-control"
                                type="text"
                                placeholder={quantity}
                                disabled
                            />
                        </div>
                    </figure> */}
          {/* <a
            className="ps-btn ps-btn--black"
            style={{ minWidth: "100%", marginRight: "20%" }}
            href="#"
            onClick={(e) => handleAddItemToCart(e)}
          >
            Add to cart
          </a> */}
          <div
            className="ps-product__actions"
            style={{
              border: "1px solid #000",
              textAlign: "center",
              width: "100px",
            }}
          >
            <a href="#" onClick={(e) => handleAddItemToWishlist(e)}>
              <i className="icon-heart"></i>
            </a>
            <a href="#" onClick={(e) => handleAddItemToCompare(e)}>
              <i className="icon-chart-bars"></i>
            </a>
          </div>
        </div>
        <a className="ps-btn" href="#" onClick={(e) => handleBuynow(e)}>
          Buy Now
        </a>
      </div>
    );
  }
};

export default ModuleDetailShoppingActions;
