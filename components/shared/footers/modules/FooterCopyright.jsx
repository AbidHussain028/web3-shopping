import React from 'react';

const FooterCopyright = () => (
    <div className="ps-footer__copyright" style={{padding:"15px 0px"}}>
        <p>&copy; 2021 Luxury Ledger Market. All Rights Reserved</p>
  
    </div>
);

export default FooterCopyright;
