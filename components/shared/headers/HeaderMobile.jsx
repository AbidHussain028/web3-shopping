import React, { Component } from 'react';
import CurrencyDropdown from './modules/CurrencyDropdown';
import Link from 'next/link';
import LanguageSwicher from './modules/LanguageSwicher';
import MobileHeaderActions from './modules/MobileHeaderActions';

class HeaderMobile extends Component {
    constructor({ props }) {
        super(props);
    }

    render() {
        return (
            <header className="header header--mobile">
                <div className="header__top">
                    <div className="header__left">
                        <p>Welcome to Luxury Ledger Market !</p>
                    </div>
                    <div className="header__right">
                        <ul className="navigation__extra">
                            {/* <li>
                                <Link href="/vendor/become-a-vendor">
                                    <a>Sell on Martfury</a>
                                </Link>
                            </li>
                            <li>
                                <Link href="/account/order-tracking">
                                    <a>Tract your order</a>
                                </Link>
                            </li> */}
                            <li>
                                <CurrencyDropdown />
                            </li>
                            <li>
                                <LanguageSwicher />
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="navigation--mobile">
                    <div className="navigation__left">
                        <Link href="/">
                            <a className="ps-logo" href="https://luxuryledger.io/" target="_blank">
                                <img
                                    src="/static/img/logo_light_icon.png"
                                    style={{ maxHeight: '50px' }}
                                    alt="Luxury Ledger Market"
                                />
                            </a>
                        </Link>
                    </div>
                    
                    <MobileHeaderActions />
                </div>
                
                <div className="ps-search--mobile">
                    <form
                        className="ps-form--search-mobile"
                        action="/"
                        style={{maxWidth:"70%"}}
                        method="get">
                        <div className="form-group--nest">
                            <input
                                className="form-control"
                                type="text"
                                placeholder="NFT's I'm shopping for..."
                                style={{padding:"5px"}}
                            />
                            <button>
                                <i className="icon-magnifier"></i>
                            </button>
                        </div>
                       
                    </form>
                    <div className="navigation__left" style={{minWidth:"80px", cursor:"pointer", display:"flex",alignItems:"center",maxWidth:"20%"}}>
                        <Link href="/presale">
                            Buy Presale
                        </Link>
                    </div>
                  
                </div>
            </header>
        );
    }
}

export default HeaderMobile;
