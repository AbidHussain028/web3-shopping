import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import SkeletonProduct from '~/components/elements/skeletons/SkeletonProduct';
import { getProductsByCollectionHelper } from '~/utilities/strapi-fetch-data-helpers';
import { generateTempArray } from '~/utilities/common-helpers';
import { ProductGroupWithCarousel } from '~/components/partials/product/ProductGroupWithCarousel';
import Product   from "../../../elements/products/Product";
const HomeDefaultProductListing = ({ collectionSlug, title }) => {
    const [productItems, setProductItems] = useState(null);
    const [currentCollection, setCurrentCollection] = useState('products');
    const [loading, setLoading] = useState(true);

    const sectionLinks = [
       
    ];

    async function getProducts(slug) {
        setLoading(true);
        const responseData = await getProductsByCollectionHelper(slug);
        console.log("Data",responseData)
        if (responseData) {
            setProductItems(responseData);
            setTimeout(
                function () {
                    setLoading(false);
                }.bind(this),
                250
            );
        }
    }

    function handleChangeTab(e, tab) {
        e.preventDefault();
        setCurrentCollection(tab.name);
        getProducts(tab.slug);
    }

    useEffect(() => {
        getProducts(collectionSlug);
    }, [collectionSlug]);

    const sectionLinksView = sectionLinks.map((link) => (
        <li
            className={currentCollection === link.name ? 'active' : ''}
            key={link.name}>
            <a href="#" onClick={(e) => handleChangeTab(e, link)}>
                {link.title}
            </a>
        </li>
    ));

    // views
    let productItemsView;
    if (!loading) {
        if (productItems && productItems.length > 0) {
            productItemsView = (
                <ProductGroupWithCarousel
                    products={productItems}
                    type="fullwidth"
                />
            );
        } else {
            productItemsView = <p>No product(s) found.</p>;
        }
    } else {
        const skeletons = generateTempArray(6).map((item) => (
            <div className="col-xl-2 col-lg-3 col-sm-3 col-6" key={item}>
                <SkeletonProduct />
            </div>
        ));
        productItemsView = <div className="row">{skeletons}</div>;
    }

    return (
        <div className="ps-product-list">
            <div className="ps-container">
                <div className="ps-section__header" style={{border:"0px",marginTop:"20px",background:"#fff",textAlign:"center",justifyContent:"center"}}>
                    <h3 style={{fontSize:"25px",textAlign:"center"}}>{"Luxury  Ledger's Top 10 Most Influential People in Crypto"}</h3>
                    {/* <ul className="ps-section__links">
                        {sectionLinksView}
                        <li>
                            <Link href={`/shop`}>
                                <a>View All</a>
                            </Link>
                        </li>
                    </ul> */}
                </div>
                <div className="ps-section__content" style={{paddingTop:"0px"}}>
                <div className="ps-block--shop-features">
           
            <div className="row">
            {productItems && productItems.length > 0&&
            productItems.map((item) => (
            <div className="col-xl-4 col-lg-4 col-sm-4 col-6" >
{console.log(item)}
                <Product product={item} key={item.id} />
                </div>
            ))
            
            }
            </div></div></div>
            </div>
        </div>
    );
};

export default HomeDefaultProductListing;
