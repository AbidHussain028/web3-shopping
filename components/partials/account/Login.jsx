import React, { Component } from 'react';
import Link from 'next/link';
import Router from 'next/router';
import { login } from '../../../store/auth/action';
import Web3 from 'web3';
import LazyLoad from 'react-lazyload';

import { Form, Input, notification } from 'antd';
import { connect } from 'react-redux';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    static getDerivedStateFromProps(props) {
        if (props.isLoggedIn === true) {
            Router.push('/');
        }
        return false;
    }

    handleFeatureWillUpdate(e) {
        e.preventDefault();
        notification.open({
            message: 'Opp! Something went wrong.',
            description: 'This feature has been updated later!',
            duration: 500,
        });
    }

    handleLoginSubmit = (e) => {
        console.log('test');
        this.loadWeb3();
        // this.props.dispatch(login());
        // Router.push('/');
    };
    loadWeb3 = async () => {
        let isConnected = false;
        try {
            if (window.ethereum) {
                window.web3 = new Web3(window.ethereum);
                await window.ethereum.enable();
                isConnected = true;
            } else if (window.web3) {
                window.web3 = new Web3(window.web3.currentProvider);
                isConnected = true;
            } else {
                isConnected = false;
                notification.open({
                    message: 'Metamask is not installed',
                    description:
                        ' please install it on your browser to connect.',
                    duration: 500,
                });
                // "Metamask is not installed, please install it on your browser to connect.",
            }
            if (isConnected === true) {
                const web3 = window.web3;
                let accounts = await web3.eth.getAccounts();
                this.props.dispatch(login(accounts[0]));
                Router.push('/');

                window.ethereum.on('accountsChanged', function (accounts) {
                    this.props.dispatch(login(accounts[0]));
                });
            }
        } catch (error) {
            console.log(error);
        }
    };
    render() {
        return (
            <div className="ps-my-account">
                <div className="container">
                    <Form
                        className="ps-form--account"
                        onFinish={this.handleLoginSubmit.bind(this)}>
                        <div className="ps-tab active" id="sign-in">
                            <div className="ps-form__content">
                                <h5>Connect to your wallet</h5>

                                <div className="form-group submit">
                                    <button
                                        type="submit"
                                        className="ps-btn ps-btn--fullwidth"
                                        style={{ padding: '15px 0px' }}>
                                        <LazyLoad>
                                            <img
                                                src="/static/img/meta-mask.png"
                                                alt="Luxury Ledger Market"
                                                style={{ paddingRight: '15px' }}
                                            />
                                        </LazyLoad>
                                        Connect to Metamask
                                    </button>
                                </div>
                            </div>
                            <div className="ps-form__footer"></div>
                        </div>
                    </Form>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return state.auth;
};
export default connect(mapStateToProps)(Login);
