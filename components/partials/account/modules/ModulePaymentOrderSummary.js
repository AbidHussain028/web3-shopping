import React from 'react';
import Link from 'next/link';
import { connect } from 'react-redux';
import LazyLoad from 'react-lazyload';

const ModulePaymentOrderSummary = ({
    shipping,
    amount,
    cartItems,
    handlePurchase,
}) => {
    let listItemsView, shippingView, totalView;
    if (cartItems && cartItems.length > 0) {
        listItemsView = cartItems.map((product) => (
            <Link href="/" key={product.id}>
                <a>
                    <strong>
                        {product.title}
                        <span>x{product.quantity}</span>
                    </strong>
                    <small>{product.quantity * product.price}ETH</small>
                </a>
            </Link>
        ));
    } else {
        listItemsView = <p>No Product.</p>;
    }
    if (shipping === true) {
        shippingView = (
            <figure>
                <figcaption>
                    {/* <strong>Shipping Fee</strong>
                    <small>$20.00</small> */}
                </figcaption>
            </figure>
        );
        totalView = (
            <figure className="ps-block__total">
                <h3>
                    Total
                    <strong>
                        {' '}
                        <img
                            src="/static/img/eth.png"
                            style={{ maxHeight: '25px' }}
                            alt="Luxury Ledger Market"
                        />
                        {amount}ETH
                    </strong>
                </h3>
                <div className="ps-block__content">
                    <div className="ps-block__tab">
                        <a className="ps-btn" onClick={this.handlePurchase}>
                            <LazyLoad>
                                <img
                                    src="/static/img/meta-mask.png"
                                    alt="Luxury Ledger Market"
                                    style={{
                                        paddingRight: '15px',
                                    }}
                                />
                            </LazyLoad>
                            Pay With Crypto
                        </a>
                    </div>
                </div>
            </figure>
        );
    } else {
        totalView = (
            <><figure className="ps-block__total">
                <h3>
                    Total
                    <strong>
                        {' '}
                        <img
                            src="/static/img/eth.png"
                            style={{ maxHeight: '25px' }}
                            alt="Luxury Ledger Market"
                        />
                        {amount}ETH
                    </strong>
                </h3>
               
            </figure>
            <div className="ps-block__content d-flex justify-content-center" style={{border:"0px" ,padding:"0px"}}>
                    <div className="ps-block__tab">
                        <a className="ps-btn" onClick={handlePurchase}>
                            <LazyLoad>
                                <img
                                    src="/static/img/meta-mask.png"
                                    alt="Luxury Ledger Market"
                                    style={{
                                        paddingRight: '15px',
                                    }}
                                />
                            </LazyLoad>
                            Pay With Crypto
                        </a>
                    </div>
                </div>
            </>
        );
    }
    return (
        <div className="ps-block--checkout-order">
            <div className="ps-block__content">
                <figure>
                    <figcaption>
                        <strong>Product</strong>
                        <strong>total</strong>
                    </figcaption>
                </figure>
                <figure className="ps-block__items">{listItemsView}</figure>
                <figure>
                    <figcaption>
                        <strong>Subtotal</strong>
                        <small>
                            {' '}
                            <img
                                src="/static/img/eth.png"
                                style={{ maxHeight: '25px' }}
                                alt="Luxury Ledger Market"
                            />
                            {amount}ETH
                        </small>
                    </figcaption>
                </figure>
                {shippingView}
                {totalView}
            </div>
        </div>
    );
};
export default connect((state) => state.cart)(ModulePaymentOrderSummary);
