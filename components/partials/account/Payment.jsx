import React, { Component } from 'react';
import { connect } from 'react-redux';
import Link from 'next/link';
import { Radio, Select } from 'antd';
import ModulePaymentOrderSummary from '~/components/partials/account/modules/ModulePaymentOrderSummary';
import { contractAbi, contractAddress } from '../../../utilities/data';
import Web3 from 'web3';
import LazyLoad from 'react-lazyload';

const { Option } = Select;

class Payment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            method: 2,
        };
    }
    componentDidMount() {
        this.loadWeb3();
    }
    loadWeb3 = async () => {
        let isConnected = false;
        try {
            if (window.ethereum) {
                window.web3 = new Web3(window.ethereum);
                await window.ethereum.enable();
                isConnected = true;
            } else if (window.web3) {
                window.web3 = new Web3(window.web3.currentProvider);
                isConnected = true;
            } else {
                isConnected = false;
                notification.open({
                    message: 'Metamask is not installed',
                    description:
                        ' please install it on your browser to connect.',
                    duration: 500,
                });
                // "Metamask is not installed, please install it on your browser to connect.",
            }
        } catch (error) {
            console.log(error);
        }
    };
    handlePurchase = () => {
        const web3 = window.web3;

        let _amount = this.props?.amount.toString();
        let contract = new web3.eth.Contract(contractAbi, contractAddress);
        let account = this.props?.account;
        console.log('web=======', web3.utils.toWei(_amount, 'ether'));
        try {
            contract.methods
                .stakeEth()
                .send({
                    from: account,
                    value: web3.utils.toWei(_amount, 'ether'),
                })
                .on('transactionHash', async (hash) => {
                    // message: 'Your transaction is pending',
                })
                .on('receipt', async (receipt) => {
                    // message: 'Your transaction is confirmed',
                })
                .on('error', async (error) => {
                    console.log('error', error);
                });
        } catch (e) {
            console.log('error rejection', e);
        }
    };
    handleChangePaymentMethod = (e) => {
        this.setState({ method: e.target.value });
    };

    render() {
        let month = [],
            year = [];
        for (let i = 1; i <= 12; i++) {
            month.push(i);
        }
        for (let i = 2019; i <= 2050; i++) {
            year.push(i);
        }
        return (
            <div className="ps-checkout ps-section--shopping" style={{padding:"0px ",margin:"0px"}}>
                <div className="container">
                    <div className="ps-section__header"
                    style={{padding:"30px 0px",margin:"0px"}}
                    >
                        <h1>Purchase your NFT</h1>
                    </div>
                    <div className="ps-section__content " style={{padding:"0px ",margin:"0px"}}>
                        <div className="row d-flex justify-content-center">
                         
                            <div className="col-xl-4 col-lg-4 col-md-12 col-sm-12 ">
                                <div className="ps-form__orders">
                                    <ModulePaymentOrderSummary
                                        handlePurchase={this.handlePurchase}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    console.log('state', state);
    return { amount: state?.cart?.amount, account: state?.auth?.account };
};
export default connect(mapStateToProps)(Payment);
