import React, { useEffect, useState } from 'react';
import ContainerShop from '~/components/layouts/ContainerShop';
import BreadCrumb from '~/components/elements/BreadCrumb';
import WidgetShopCategories from '~/components/shared/widgets/WidgetShopCategories';
import WidgetShopBrands from '~/components/shared/widgets/WidgetShopBrands';
import WidgetShopFilterByPriceRange from '~/components/shared/widgets/WidgetShopFilterByPriceRange';
import ProductRepository from '~/repositories/ProductRepository';
import { useRouter } from 'next/router';
import ProductItems from '~/components/partials/product/ProductItems';
import LazyLoad from 'react-lazyload';
import { Row, Col } from 'antd';

const ProductByBrandScreen = () => {
    const Router = useRouter();
    const [brand, setCategory] = useState(null);
    const [loading, setLoading] = useState(false);

    const breadCrumb = [
        {
            text: 'Home',
            url: '/',
        },
        {
            text: 'presale',
            url: '/presale',
        },
    ];

    //Views
    let productItemsViews;
    if (!loading) {
        if (brand && brand.products.length > 0) {
            productItemsViews = (
                <ProductItems columns={4} products={brand.products} />
            );
        } else {
            productItemsViews = <p>Tokens are comming soon</p>;
        }
    } else {
        productItemsViews = <p>Loading...</p>;
    }

    return (
        <ContainerShop title={brand ? brand.name : 'Brand'} boxed={true}>
            <div className="ps-page--shop">
                <BreadCrumb breacrumb={breadCrumb} />
                <div
                    className="container"
                    style={{
                        // padding: '0px',
                        // margin: '0px',
                        // minWidth: '100vw',
                    }}>
                    <div>
                        <div
                            className="ps-section__header"
                            style={{
                                border: '0px',
                                marginTop: '30px',
                                background: '#fff',
                                textAlign: 'center',
                                justifyContent: 'center',
                            }}>
                            <h3
                                style={{
                                    fontSize: '25px',
                                    textAlign: 'center',
                                    fontWeight:"normal"
                                }}>
                                {'Luxury Ledger (LXRY) Presale is Live'}
                            </h3>
                        </div>
                        <div className="d-flex justify-content-center align-self-center" >
                        <div className="row" style={{maxWidth:"600px"}} >
                        <div className="mt-4 d-flex justify-content-center col-sm-12 col-md-6 col-lg-6 col-xs-12 col-12">
                                <div
                                    style={{
                                        marginRight: '20px',
                                        minWidth: '220px',
                                        maxWidth: '220px',
                                    }}>
                                    <button
                                        type="submit"
                                        className="ps-btn ps-btn--fullwidth"
                                        style={{ padding: '15px 0px' }}>
                                        <LazyLoad>
                                            <img
                                                src="/static/img/meta-mask.png"
                                                alt="Luxury Ledger Market"
                                                style={{
                                                    padding: '0px 15px',
                                                    height: '35px',
                                                }}
                                            />
                                        </LazyLoad>
                                        Buy LXRY
                                        <LazyLoad>
                                            <img
                                                src="/static/img/eth.png"
                                                alt="Luxury Ledger Market"
                                                style={{
                                                    padding: '0px 15px',
                                                    height: '35px',
                                                }}
                                            />
                                        </LazyLoad>
                                    </button>
                                </div>
                            </div>
                     
                            <div className=" mt-4 d-flex justify-content-center col-sm-12 col-md-6 col-lg-6 col-xs-12 col-12">
                                <div
                                    style={{
                                        marginRight: '20px',
                                        minWidth: '220px',
                                        maxWidth: '220px',
                                    }}>
                                    <button
                                        type="submit"
                                        className="ps-btn ps-btn--fullwidth"
                                        style={{ padding: '15px 0px' }}>
                                        <LazyLoad>
                                            <img
                                                src="/static/img/btn11.png"
                                                alt="Luxury Ledger Market"
                                                style={{
                                                    padding: '0px 15px',
                                                    height: '35px',
                                                }}
                                            />
                                        </LazyLoad>
                                        Buy LXRY
                                        <LazyLoad>
                                            <img
                                                src="/static/img/btn12.png"
                                                alt="Luxury Ledger Market"
                                                style={{
                                                    padding: '0px 15px',
                                                    height: '35px',
                                                }}
                                            />
                                        </LazyLoad>
                                    </button>
                                </div>
                            </div>
                        </div>
                   </div>
                    </div>
                    <div
                            className="ps-layout__right"
                            style={{
                                maxWidth: '100%',
                               overflow:"auto"
                            }}>
                            <img

                            style={{
                                width: "100%",
                                height: "auto",
                                minWidth:"500px",
                                overflowX:"auto"
                              }}
                                src="/static/img/info.jpeg"
                                alt="Luxury Ledger Market"
                            />
                        </div>
 
                    <div
                        className="ps-layout--shop ps-shop--category"
                        style={{
                            display: 'flex',
                            justifyContent: 'center',
                            paddingTop: '30px',
                            paddingBottom: '30px',
                        }}>
                       
                        <div
                            className="ps-layout__right"
                            style={{
                                maxWidth: '100%',
                               overflow:"auto"
                            }}>
                            <img

                            style={{
                                width: "100%",
                                height: "auto",
                                minWidth:"500px",
                                overflowX:"auto"
                              }}
                                src="/static/img/LXRY-One-Pager.jpeg"
                                alt="Luxury Ledger Market"
                            />
                        </div>
                    </div>
                </div>
            </div>
        </ContainerShop>
    );
};
export default ProductByBrandScreen;
